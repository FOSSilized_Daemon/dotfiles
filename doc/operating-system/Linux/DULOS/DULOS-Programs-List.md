# Introduction
In this document you will find a concrete list of all the programs I use along with helpful links and resources.

## Core Operating System
### Kernel
| Name                | Description                            | Configuration File | Documentation                       | Fork Homepage | Project Homepage                 |
| :-------------------| :--------------------------------------| :------------------| :-----------------------------------| :-------------| :--------------------------------|
| linux               | A free and open-source modular kernel. |                    | [Site](https://www.kernel.org/doc/) | N/A           | [linux](https://www.kernel.org/) |

### Init System
| Name                | Description                                                                                                      | Configuration File | Documentation                                      | Fork Homepage | Project Homepage                                                 |
| :-------------------| :----------------------------------------------------------------------------------------------------------------| :------------------| :--------------------------------------------------| :-------------| :----------------------------------------------------------------|
| sinit               | A suckless init, initially based on Rich Felker's minimal init.                                                  |                    | [Site](https://core.suckless.org/sinit/)           | N/A           | [sinit](https://core.suckless.org/sinit/)                        |
| daemon-tools encore | A backwards compatible, enhanced version of Daniel J. Bernstein's daemontools package, written by Bruce Guenter. |                    | [Site](https://untroubled.org/daemontools-encore/) | N/A           | [daemontools-encore](https://untroubled.org/daemontools-encore/) |

### Bootloader
| Name     | Description                                   | Configuration File | Documentation                                                               | Fork Homepage | Project Homepage                                                                |
| :--------| :---------------------------------------------| :------------------| :---------------------------------------------------------------------------| :-------------| :-------------------------------------------------------------------------------|
| syslinux | A boot loader for the Linux operating system. |                    | [Site](https://wiki.syslinux.org/wiki/index.php?title=The_Syslinux_Project) | N/A           | [syslinux](https://wiki.syslinux.org/wiki/index.php?title=The_Syslinux_Project) |

### Package Manager
| Name | Description                                  | Configuration File | Documentation                         | Fork Homepage | Project Homepage                  |
| :----| :--------------------------------------------| :------------------| :-------------------------------------| :-------------| :---------------------------------|
| guix | A functional cross-platform package manager. |                    | [Site](https://guix.gnu.org/en/help/) | N/A           | [GNU Guix](https://guix.gnu.org/) |

### Audio Server & Utilities
| Name       | Description                                                        | Configuration File | Documentation                                 | Fork Homepage | Project Homepage                                                    |
| :----------| :------------------------------------------------------------------| :------------------| :---------------------------------------------| :-------------| :-------------------------------------------------------------------|
| pipewire   | A server for handling audio, video streams, and hardware on Linux. |                    | [Site](https://docs.pipewire.org/)            | N/A           | [pipewire](https://pipewire.org/)                                   |
| pactl      | Control a running PulseAudio sound server.                         | N/A                | [Man Page](https://linux.die.net/man/1/pactl) | N/A           | [Pulseaudio](https://www.freedesktop.org/wiki/Software/PulseAudio/) |

## Graphical User Interface
### Desktop Environment
| Name               | Description                             | Configuration File                                                                        | Documentation                                                                | Fork Homepage                                       | Project Homepage                                       |
| :------------------| :---------------------------------------| :-----------------------------------------------------------------------------------------| :----------------------------------------------------------------------------| :---------------------------------------------------| :------------------------------------------------------|
| xorg-server        | X11R7 X server.                         |                                                                                           | [Man Page](https://linux.die.net/man/1/xorg)                                 | N/A                                                 | [x.org](https://www.x.org/wiki/)                       |
| xinit              | Initialize an X session.                | [.xinitrc](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/main/src/home/.xinitrc)   | [Man Page](https://linux.die.net/man/1/startx)                               | N/A                                                 | [xinit](https://gitlab.freedesktop.org/xorg/app/xinit) |
| dwm                | A dynamic window manager for X.         | [config.def.h](https://gitlab.com/FOSSilized_Daemon/dwm/-/blob/master/config.def.h)       | [README](https://gitlab.com/FOSSilized_Daemon/dwm/README.md)                 | [dwm](https://gitlab.com/FOSSilized_Daemon/dwm)     | [dwm](https://dwm.suckless.org/)                       |
| st                 | A simple terminal implementation for X. | [config.def.h](https://gitlab.com/FOSSilized_Daemon/st/-/blob/master/src/config.def.h)    | [README](https://gitlab.com/FOSSilized_Daemon/st/README.md)                  | [st](https://gitlab.com/FOSSilized_Daemon/st)       | [st](https://st.suckless.org/)                         |
| slock              | Simple X display locker.                | [config.def.h](https://gitlab.com/FOSSilized_Daemon/slock/-/blob/master/src/config.def.h) | [README](https://gitlab.com/FOSSilized_Daemon/slock/README.md)               | [slock](https://gitlab.com/FOSSilized_Daemon/slock) | [slock](https://tools.suckless.org/slock/)             |
| dmenu              | A dynamic menu for X.                   | [config.def.h](https://gitlab.com/FOSSilized_Daemon/dmenu/-/blob/master/src/config.def.h) | [README](https://gitlab.com/FOSSilized_Daemon/dmenu/-/blob/master/README.md) | [dmenu](https://gitlab.com/FOSSilized_Daemon/dmenu) | [dmenu](https://tools.suckless.org/dmenu/)             |
| xmenu              | A menu utility for X.                   |                                                                                           | [README](https://gitlab.com/FOSSilized_Daemon/xmenu-/blob/master/README.md)  | [xmenu](https://github.com/phillbush/xmenu)         | [xmenu](https://github.com/phillbush/xmenu)            |

### Graphical Environment Utilities
| Name               | Description                                                                                | Configuration File                                                                                          | Documentation                                                                                  | Fork Homepage | Project Homepage                                                  |
| :------------------| :------------------------------------------------------------------------------------------| :-----------------------------------------------------------------------------------------------------------| :----------------------------------------------------------------------------------------------| :-------------| :-----------------------------------------------------------------|
| sxhkd              | A X daemon that reacts to input events by executing commands.                              | [sxhkdrc](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/main/src/home/.config/sxhkd/sxhkdrc)         | [README](https://github.com/baskerville/sxhkd/blob/master/README.md)                           | N/A           | [sxhkd](https://github.com/baskerville/sxhkd)                     |
| xset               | User preference utility for X.                                                             | N/A                                                                                                         | [Man Page](https://linux.die.net/man/1/xset)                                                   | N/A           | [x.org](https://www.x.org/wiki/)                                  |
| setxbkmap          | Set the keyboard using the X Keyboard Extension.                                           | N/A                                                                                                         | [Man Page](https://linux.die.net/man/1/setxkbmap)                                              | N/A           | [setxkbmap](https://gitlab.freedesktop.org/xorg/app/setxkbmap)    |
| xcape              | Use a modifier key as another key.                                                         | N/A                                                                                                         | [Man Page](http://manpages.ubuntu.com/manpages/xenial/man1/xcape.1.html)                       | N/A           | [xcape](https://github.com/alols/xcape)                           |
| unclutter-xfixes   | Rewrite of unclutter using the X11-Xfixes extension.                                       | N/A                                                                                                         | [Man Page](https://github.com/Airblader/unclutter-xfixes/blob/master/man/unclutter-xfixes.man) | N/A           | [unclutter-xfixes](https://github.com/Airblader/unclutter-xfixes) |
| xsel               | Manipulate the X selection.                                                                | N/A                                                                                                         | [Man Page](https://linux.die.net/man/1/xsel)                                                   | N/A           | [xsel](https://github.com/kfish/xsel)                             |
| xclip              | Command line interface to X selections (clipboard).                                        | N/A                                                                                                         | [Man Page](https://linux.die.net/man/1/xclip)                                                  | N/A           | [xclip](https://github.com/astrand/xclip)                         |
| dunst              | A highly configurable and lightweight notification daemon.                                 | [dunstrc](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/main/src/home/.config/dunst/dunstrc)         | [README](https://github.com/dunst-project/dunst/blob/master/README.md)                         | N/A           | [dunst](https://github.com/dunst-project/dunst)                   |
| dunstctl           | Command line control utility for dunst.                                                    | N/A                                                                                                         | [Man Page](https://man.archlinux.org/man/dunstctl.1.en)                                        | N/A           | [dunstctl](https://github.com/dunst-project/dunst)                |
| polybar-dwm-module | A fast and easy-to-use tool for creating status bars.                                      | [config](OOOO)                                                                                              | [README](https://github.com/mihirlad55/polybar-dwm-module/README.md)                           | N/A           | [polybar](https://github.com/mihirlad55/polybar-dwm-module)       |
| xwallpaper         | A utility that allows you to set image files as your X wallpaper.                          | N/A                                                                                                         | [README](https://github.com/stoeckmann/xwallpaper/blob/master/README.md)                       | N/A           | [xwallpaper](https://github.com/stoeckmann/xwallpaper)            |
| xautolock          | Monitors console activity under the X window system and runs a specified program.          | N/A                                                                                                         | [README](https://github.com/l0b0/xautolock/blob/master/Readme)                                 | N/A           | [xautolock](https://github.com/l0b0/xautolock)                    |
| picom              | A standalone compositor for Xorg.                                                          | [picom.conf](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/tree/main/src/home/.config/picom/picom.conf)   | [README](https://github.com/yshui/picom/blob/master/README.md)                                 | N/A           | [picom](https://github.com/yshui/picom/)                          |
| slop               | An application that queries for a selection from the user and prints the region to stdout. | [slop](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/tree/main/src/home/.config/slop)                     | [README](https://github.com/naelstrof/slop/blob/master/README.md)                              | N/A           | [slop](https://github.com/naelstrof/slop)                         |
| brigthnessctl      | Read and control device brightness.                                                        | N/A                                                                                                         | [Man Page](https://man.archlinux.org/man/community/brightnessctl/brightnessctl.1.en)           | N/A           | [Brightnessctl](https://github.com/Hummer12007/brightnessctl)     |
| gal                | An application launcher for the graphical environment.                                     | N/A                                                                                                         | [README](https://gitlab.com/FOSSilized_Daemon/gal/-/blob/master/README.md)                     | N/A           | [gal](https://gitlab.com/FOSSilized_Daemon/gal)                   |
| shqu               | A simple help utility for the graphical user interface.                                    | N/A                                                                                                         | [README](https://gitlab.com/FOSSilized_Daemon/shqu/-/blob/master/README.md)                    | N/A           | [shqu](https://gitlab.com/FOSSilized_Daemon/shqu)                 |
| gsm                | A simple utility for managing the system state.                                            | N/A                                                                                                         | [README](https://gitlab.com/FOSSilized_Daemon/gsm/-/blob/master/README.md)                     | N/A           | [gsm](https://gitlab.com/FOSSilized_Daemon/gsm)                   |

## Multimedia Utilities
### Multimedia Viewing Utilities
| Name                | Description                                                      | Configuration File                                                                                    | Documentation                                                                     | Fork Homepage | Project Homepage                                                   |
| :-------------------| :----------------------------------------------------------------| :-----------------------------------------------------------------------------------------------------| :---------------------------------------------------------------------------------| :-------------| :------------------------------------------------------------------|
| sxiv                | Simple X Image Viewer.                                           |                                                                                                       | [README](https://github.com/muennich/sxiv/blob/master/README.md)                  | N/A           | [sxiv](https://github.com/muennich/sxiv)                           |
| zathura             | A document viewer.                                               |                                                                                                       | [README](https://github.com/pwmt/zathura/blob/develop/README.md)                  | N/A           | [zathura](https://github.com/pwmt/zathura)                         |
| zathura-cb          | Comic book support for zathura.                                  | N/A                                                                                                   | [README](https://github.com/pwmt/zathura-cb/blob/develop/README.md)               | N/A           | [zathura-cb](https://github.com/pwmt/zathura-cb)                   |
| zathura-djvu        | DjVU support for zathura.                                        | N/A                                                                                                   | N/A                                                                               | N/A           | [zathura-djvu](https://github.com/pwmt/zathura-djvu)               |
| zarhura-pdf-poppler | PDF support for zathura (poppler).                               | N/A                                                                                                   | [README](https://github.com/pwmt/zathura-pdf-poppler/blob/master/README)          | N/A           | [zathura-pdf-poppler](https://github.com/pwmt/zathura-pdf-poppler) |
| zathura-ps          | PostScript support to zathura.                                   | N/A                                                                                                   | [Site](https://pwmt.org/projects/zathura-ps/)                                     | N/A           | [zathura-ps](https://pwmt.org/projects/zathura-ps/)                |
| mpd                 | A flexible, powerful, server-side application for playing music. | [mpd.conf](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/main/src/home/.config/mpd/mpd.conf)   | [Site](https://www.musicpd.org/doc/html/user.html)                                | N/A           | [mpd](https://www.musicpd.org/)                                    |
| mpc                 | A minimalist command line interface to MPD.                      | N/A                                                                                                   | [Man Page](https://linux.die.net/man/1/mpc)                                       | N/A           | [mpc](https://www.musicpd.org/clients/mpc/)                        |
| mpv                 | A free (as in freedom) media player for the command line.        | [mpv](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/main/src/home/.config/mpv)                 | [Site](https://mpv.io/manual/)                                                    | N/A           | [mpv](https://mpv.io/)                                             |
| steam               | A client for the Steam platform.                                 |                                                                                                       | [Man Page](https://man.archlinux.org/man/multilib/steam/steam.6.en)               | N/A           | [steam](https://store.steampowered.com/about/?)                    |
| itch.io             | A client for the itch.io platform.                               |                                                                                                       | [Site](https://itch.io/docs/itch/)                                                | N/A           | [itch.io](https://itch.io)                                         |
| lutris              | A video game manager.                                            | [lutris](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/main/src/home/.config/lutris)           | [Site](https://github.com/lutris/docs)                                            | N/A           | [lutris](https://lutris.net/)                                      |
| battle.net          | A client for the Blizzard platform.                              |                                                                                                       | [Site](https://develop.battle.net/documentation)                                  | N/A           | [battle.net](https://us.shop.battle.net/en-us)                     |
| epic games store    | A client for the Epic Games platform.                            |                                                                                                       | [Site](https://dev.epicgames.com/docs/services/en-US/index.html)                  | N/A           | [epic games store](https://store.epicgames.com/en-US/)             |
| gog galaxy          | A client for the GOG platform.                                   |                                                                                                       | [Site](https://docs.gog.com/galaxyapi/)                                           | N/A           | [gog galaxy](https://www.gog.com/galaxy)                           |
| indiegala           | A client for the Indiegala platform.                             |                                                                                                       | [Site](https://docs.indiegala.com/)                                               | N/A           | [indiegala](https://www.indiegala.com/)                            |
| uplay               | A client for the Ubisoft platform.                               |                                                                                                       | [Site](https://www.ubisoft.com/en-us/help/gameplay/article/game-manual/000075586) | N/A           | [uplay](https://ubisoftconnect.com/en-US/)                         |

### Multimedia Editing Utilities
| Name            | Description                                       | Configuration File | Documentation                                                             | Fork Homepage | Project Homepage                                                            |
| :---------------| :-------------------------------------------------| :------------------| :-------------------------------------------------------------------------| :-------------| :---------------------------------------------------------------------------|
| blender         | A 3D modelling and rendering package.             |                    | [Site](https://docs.blender.org/)                                         | N/A           | [blender](https://www.blender.org/)                                         |
| davinci resolve | A video editor.                                   |                    | [Site](https://www.blackmagicdesign.com/products/davinciresolve/training) | N/A           | [davinci resolve](https://www.blackmagicdesign.com/products/davinciresolve) |
| audacity        | A digital audio editor and recording application. |                    | [Site](https://manual.audacityteam.org/)                                  | N/A           | [audacity](https://www.audacityteam.org/)                                   |
| gimp            | A cross-platform image editor.                    |                    | [Site](https://www.gimp.org/docs/)                                        | N/A           | [gimp](https://www.gimp.org/)                                               |
| inkscape        | Professional quality vector graphics software.    |                    | [Site](https://inkscape.org/learn/books/)                                 | N/A           | [inkscape](https://inkscape.org/)                                           |
| sc              | Spreadsheet calculator.                           |                    | [Man Page](https://www.unix.com/man-page/linux/1/sc/)                     | N/A           | [sc](https://github.com/n-t-roff/sc)                                        |
| eyed3           | A Python tool for working with audio files.       |                    | [Site](https://eyed3.readthedocs.io/en/latest/)                           | N/A           | [eyed3](https://github.com/nicfit/eyeD3)                                    |










































## UNIX Utilities
### Archive Utilities
| Name   | Description                                                                                  | Configuration File | Documentation                                                | Fork Homepage | Project Homepage                |
| :------| :--------------------------------------------------------------------------------------------| :------------------| :------------------------------------------------------------| :-------------| :-------------------------------|
| cpio   | A general file archiver utility and its associated file format.                              | N/A                | [Man Page](https://linux.die.net/man/1/cpio)                 | N/A           | [BusyBox](https://busybox.net/) |
| gzip   | Reduces the size of the named files using Lempel-Ziv coding (LZ77).                          | N/A                | [Man Page](https://linux.die.net/man/1/gunzip)               | N/A           | [BusyBox](https://busybox.net/) |
| tar    | Provides the ability to create tar archives, as well as various other kinds of manipulation. | N/A                | [Man Page](https://man7.org/linux/man-pages/man1/tar.1.html) | N/A           | [BusyBox](https://busybox.net/) |
| unzip  | List, test, or extract files from a ZIP archive, commonly found on MS-DOS systems.           | N/A                | [Man Page](https://linux.die.net/man/1/unzip)                | N/A           | [BusyBox](https://busybox.net/) |
| xz     | A general-purpose data compression tool.                                                     | N/A                | [Man Page](https://linux.die.net/man/1/xz)                   | N/A           | [BusyBox](https://busybox.net/) |
| zip    | Package and compress (archive) files.                                                        | N/A                | [Man Page](https://linux.die.net/man/1/zip)                  | N/A           | [BusyBox](https://busybox.net/) |

### Disk Usage Utilities
| Name   | Description                          | Configuration File | Documentation                              | Fork Homepage | Project Homepage |
| :------| :------------------------------------| :------------------| :------------------------------------------| :-------------| :----------------|
| df     | Report file system disk space usage. | N/A                | [Man Page](https://linux.die.net/man/1/df) | N/A           | [BusyBox](https://busybox.net/) |
| du     | Estimate file space usage.           | N/A                | [Man Page](https://linux.die.net/man/1/du) | N/A           | [BusyBox](https://busybox.net/) |

### Documentation Utilities
| Name   | Description                                        | Configuration File | Documentation                                                               | Fork Homepage | Project Homepage                                                |
| :------| :--------------------------------------------------| :------------------| :---------------------------------------------------------------------------| :-------------| :---------------------------------------------------------------|
| man    | Format and display the on-line manual pages.       | N/A                | [Man Page](https://linux.die.net/man/1/man)                                 | N/A           | [BusyBox](https://busybox.net/)                                 |
| info   | Read Info documents.                               | N/A                | [Man Page](https://linux.die.net/man/1/info)                                | N/A           | [BusyBox](https://busybox.net/)                                 |
| shqu   | A simple utility for the graphical user interface. | N/A                | [README](https://gitlab.com/FOSSilized_Daemon/shqu/-/blob/master/README.md) | N/A           | [shqu](https://gitlab.com/FOSSilized_Daemon/shqu/-/blob/master) |

### Editor Utilities
| Name   | Description                              | Configuration File                                                                        | Documentation                              | Fork Homepage | Project Homepage                |
| :------| :----------------------------------------| :-----------------------------------------------------------------------------------------| :------------------------------------------| :-------------| :-------------------------------|
| ed     | Text editor.                             | N/A                                                                                       | [Man Page](https://linux.die.net/man/1/ed) | N/A           | [BusyBox](https://busybox.net/) |
| neovim | A hyperextensible Vim-based text editor. | [nvim](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/home/.config/nvim) | [Site](https://neovim.io/doc/)             | N/A           | [neovim](https://neovim.io/)    |

### Encoding and Decoding Utilities
| Name     | Description                                             | Configuration File | Documentation                                                   | Fork Homepage | Project Homepage                |
| :--------| :-------------------------------------------------------| :------------------| :---------------------------------------------------------------| :-------------| :-------------------------------|
| base32   | base32 encode/decode data and print to standard output. | N/A                | [Man Page](https://man7.org/linux/man-pages/man1/base32.1.html) | N/A           | [BusyBox](https://busybox.net/) |
| base64   | base64 encode/decode data and print to standard output. | N/A                | [Man Page](https://linux.die.net/man/1/base64)                  | N/A           | [BusyBox](https://busybox.net/) |
| uudecode | Decode a binary files representation.                   | N/A                | [Man Page](https://linux.die.net/man/1/uudecode)                | N/A           | [BusyBox](https://busybox.net/) |
| uuencode | Encode a binary files representation.                   | N/A                | [Man Page](https://linux.die.net/man/1/uudecode)                | N/A           | [BusyBox](https://busybox.net/) |

### File Formatting Utilities
| Name     | Description                                 | Configuration File | Documentation                                    | Fork Homepage | Project Homepage                |
| :--------| :-------------------------------------------| :------------------| :------------------------------------------------| :-------------| :-------------------------------|
| dos2unix | DOS/MAC to UNIX text file format converter. | N/A                | [Man Page](https://linux.die.net/man/1/dos2unix) | N/A           | [BusyBox](https://busybox.net/) |
| unix2dos | UNIX to DOS text file format converter.     | N/A                | [Man Page](https://linux.die.net/man/1/unix2dos) | N/A           | [BusyBox](https://busybox.net/) |

### File Management Utilities
| Name   | Description                                                       | Configuration File | Documentation                                                              | Fork Homepage | Project Homepage                                            |
| :------| :-----------------------------------------------------------------| :------------------| :--------------------------------------------------------------------------| :-------------| :-----------------------------------------------------------|
| cp     | Copy files and directories.                                       | N/A                | [Man Page](https://linux.die.net/man/1/cp)                                 | N/A           | [BusyBox](https://busybox.net/)                             |
| ln     | Make links between file.                                          | N/A                | [Man Page](https://linux.die.net/man/1/ln)                                 | N/A           | [BusyBox](https://busybox.net/)                             |
| mkdir  | Make directories.                                                 | N/A                | [Man Page](https://linux.die.net/man/1/mkdir)                              | N/A           | [BusyBox](https://busybox.net/)                             |
| mkfifo | Make FIFOs (named pipes).                                         | N/A                | [Man Page](https://linux.die.net/man/1/mkfifo)                             | N/A           | [BusyBox](https://busybox.net/)                             |
| mknod  | Make block or character special files.                            | N/A                | [Man Page](https://linux.die.net/man/1/mknod)                              | N/A           | [BusyBox](https://busybox.net/)                             |
| mktemp | Create a temporary file or directory.                             | N/A                | [Man Page](https://linux.die.net/man/1/mktemp)                             | N/A           | [BusyBox](https://busybox.net/)                             |
| mv     | Move (rename) files.                                              | N/A                | [Man Page](https://linux.die.net/man/1/mv)                                 | N/A           | [BusyBox](https://busybox.net/)                             |
| rm     | Remove files or directories.                                      | N/A                | [Man Page](https://linux.die.net/man/1/rm)                                 | N/A           | [BusyBox](https://busybox.net/)                             |
| srm    | Secure remove (secure_delection toolkit).                         | N/A                | [Man Page](https://github.com/BlackArch/secure-delete/blob/master/srm.1)   | N/A           | [secure-delete](https://github.com/BlackArch/secure-delete) |
| smem   | Secure memory wiper (secure_deletion toolkit).                    | N/A                | [Man Page](https://github.com/BlackArch/secure-delete/blob/master/smem.1)  | N/A           | [secure-delete](https://github.com/BlackArch/secure-delete) |
| sfill  | Secure free disk and inode space wiper (secure_deletion toolkit). | N/A                | [Man Page](https://github.com/BlackArch/secure-delete/blob/master/sfill.1) | N/A           | [secure-delete](https://github.com/BlackArch/secure-delete) |
| sswap  | Secure swap wiper (secure_deletion toolkit).                      | N/A                | [Man Page](https://github.com/BlackArch/secure-delete/blob/master/sswap.1) | N/A           | [secure-delete](https://github.com/BlackArch/secure-delete) |

### File Searching Utilities
| Name | Description                                                                  | Configuration File | Documentation                                | Fork Homepage | Project Homepage                |
| :----| :----------------------------------------------------------------------------| :------------------| :--------------------------------------------| :-------------| :-------------------------------|
| exa  | A modern replacement for the venerable file-listing command-line program ls. | N/A                | [Site](https://the.exa.website/introduction) | N/A           | [exa](https://the.exa.website/) |
| find | Search for files in a directory hierarchy.                                   | N/A                | [Man Page](https://linux.die.net/man/1/find) | N/A           | [BusyBox](https://busybox.net/) |

### File System Utilities
| Name        | Description                                                         | Configuration File | Documentation                                                           | Fork Homepage | Project Homepage                |
| :-----------| :-------------------------------------------------------------------| :------------------| :-----------------------------------------------------------------------| :-------------| :-------------------------------|
| fsck        | Check and repair a Linux file system.                               | N/A                | [Man Page](https://linux.die.net/man/8/fsck)                            | N/A           | [BusyBox](https://busybox.net/) |
| fsck.minix  | Check consistency of Minix filesystem.                              | N/A                | [Man Page](https://www.man7.org/linux/man-pages/man8/fsck.minix.8.html) | N/A           | [BusyBox](https://busybox.net/) |
| tune2fs     | Adjust tunable filesystem parameters on ext2/ext3/ext4 filesystems. | N/A                | [Man Page](https://linux.die.net/man/8/tune2fs)                         | N/A           | [BusyBox](https://busybox.net/) |
| mkfs.ext2   | Create an ext2 filesystem.                                          | N/A                | [Man Page](https://linux.die.net/man/8/mkfs.ext2)                       | N/A           | [BusyBox](https://busybox.net/) |
| mkfs.ext3   | Create an ext3 filesystem.                                          | N/A                | [Man Page](https://linux.die.net/man/8/mkfs.ext3)                       | N/A           | [BusyBox](https://busybox.net/) |
| mkfs.ext4   | Create an ext4 filesystem.                                          | N/A                | [Man Page](https://linux.die.net/man/8/mkfs.ext4)                       | N/A           | [BusyBox](https://busybox.net/) |
| mkfs.ntfs   | Create a ntfs filesystem.                                           | N/A                | [Man Page](https://linux.die.net/man/8/mkfs.ntfs)                       | N/A           | [BusyBox](https://busybox.net/) |
| mkfs.minix  | Create a minix filesystem.                                          | N/A                | [Man Page](https://linux.die.net/man/8/mkfs.minix)                      | N/A           | [BusyBox](https://busybox.net/) |
| mkfs.vfat   | Create a vfat filesystem.                                           | N/A                | [Man Page](https://linux.die.net/man/8/mkfs.vfat)                       | N/A           | [BusyBox](https://busybox.net/) |
| mkfs_reiser | Create a reiser filesystem.                                         | N/A                | [Man Page](https://linux.die.net/man/8/mkfs_reiser)                     | N/A           | [BusyBox](https://busybox.net/) |
| mkdosfs     | Create a dos filesystem.                                            | N/A                | [Man Page](https://linux.die.net/man/8/mkdosfs)                         | N/A           | [BusyBox](https://busybox.net/) |
| mkswap      | Create a swap filesystem.                                           | N/A                | [Man Page](https://linux.die.net/man/8/mkswap)                          | N/A           | [BusyBox](https://busybox.net/) |

### Process Management Utilities
| Name    | Description                                                | Configuration File | Documentation                                   | Fork Homepage | Project Homepage                |
| :-------| :----------------------------------------------------------| :------------------| :-----------------------------------------------| :-------------| :-------------------------------|
| nice    | Run a program with modified scheduling priority.           | N/A                | [Man Page](https://linux.die.net/man/1/nice)    | N/A           | [BusyBox](https://busybox.net/) |
| nohup   | Run a command immune to hangups, with output to a non-tty. | N/A                | [Man Page](https://linux.die.net/man/1/nohup)   | N/A           | [BusyBox](https://busybox.net/) |
| kill    | Terminate a process.                                       | N/A                | [Man Page](https://linux.die.net/man/1/kill)    | N/A           | [BusyBox](https://busybox.net/) |
| killall | Kill processes by name.                                    | N/A                | [Man Page](https://linux.die.net/man/1/killall) | N/A           | [BusyBox](https://busybox.net/) |

### Text Formatting Utilities
| Name     | Description             | Configuration File | Documentation                                    | Fork Homepage | Project Homepage                |
| :--------| :-----------------------| :------------------| :------------------------------------------------| :-------------| :-------------------------------|
| expand   | Convert tabs to spaces. | N/A                | [Man Page](https://linux.die.net/man/1/expand)   | N/A           | [BusyBox](https://busybox.net/) |
| unexpand | Convert spaces to tabs. | N/A                | [Man Page](https://linux.die.net/man/1/unexpand) | N/A           | [BusyBox](https://busybox.net/) |

### User and Group Utilities
| Name     | Description                                               | Configuration File | Documentation                                    | Fork Homepage | Project Homepage                |
| :--------| :---------------------------------------------------------| :------------------| :------------------------------------------------| :-------------| :-------------------------------|
| addgroup | Create a new group.                                       | N/A                | [Man Page](https://linux.die.net/man/8/groupadd) | N/A           | [BusyBox](https://busybox.net/) |
| adduser  | Create a new user or update default new user information. | N/A                | [Man Page](https://linux.die.net/man/8/adduser)  | N/A           | [BusyBox](https://busybox.net/) |
| chgrp    | Change group ownership.                                   | N/A                | [Man Page](https://linux.die.net/man/1/chgrp)    | N/A           | [BusyBox](https://busybox.net/) |
| chown    | Change file owner and group.                              | N/A                | [Man Page](https://linux.die.net/man/1/chown)    | N/A           | [BusyBox](https://busybox.net/) |
| deluser  | Remove a user from the system.                            | N/A                | [Man Page](http://man.he.net/man8/deluser)       | N/A           | [BusyBox](https://busybox.net/) |
| delgroup | Remove a group from the system.                           | N/A                | [Man Page](http://man.he.net/man8/delgroup)      | N/A           | [BusyBox](https://busybox.net/) |
| id       | Print real and effective user and group IDs.              | N/A                | [Man Page](https://linux.die.net/man/1/id)       | N/A           | [BusyBox](https://busybox.net/) |

### Admin Utilities
| Name | Description                                    | Configuration File                                                                                 | Documentation                            | Fork Homepage | Project Homepage                         |
| :----| :----------------------------------------------| :--------------------------------------------------------------------------------------------------| :----------------------------------------| :-------------| :----------------------------------------|
| doas | A program to execute commands as another user. | [doas.conf](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/config/doas/doas.conf) | [Man Page](https://man.openbsd.org/doas) | N/A           | [doas](https://github.com/Duncaen/OpenDoas) |

### Hard Drive Utilities
| Name     | Description                             | Configuration File | Documentation                                                           | Fork Homepage | Project Homepage                                   |
| :--------| :---------------------------------------| :------------------| :-----------------------------------------------------------------------| :-------------| :--------------------------------------------------|
| nvme-cli | NVMe management command line interface. | N/A                | [Man Page](https://manpages.debian.org/testing/nvme-cli/nvme.1.en.html) | N/A           | [nvme-cli](https://github.com/linux-nvme/nvme-cli) |

### Network Utilities
| Name           | Description                                                                      | Configuration File                                                                                                               | Documentation                                                                  | Fork Homepage | Project Homepage                                            |
| :--------------| :--------------------------------------------------------------------------------| :--------------------------------------------------------------------------------------------------------------------------------| :------------------------------------------------------------------------------| :-------------| :-----------------------------------------------------------|
| arp            | Manipulate the system ARP cache.                                                 | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/arp)                                    | N/A           | [BusyBox](https://busybox.net/)                             |
| ether-wake     | A tool to send a Wake-On-LAN "Magic Packet".                                     | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/ether-wake)                             | N/A           | [BusyBox](https://busybox.net/)                             |
| hostname       | Show or set the system's host name.                                              | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/1/hostname)                               | N/A           | [BusyBox](https://busybox.net/)                             |
| ip             | Show/manipulate routing, devices, policy routing and tunnels.                    | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/ip)                                     | N/A           | [BusyBox](https://busybox.net/)                             |
| ntpd           | Network Time Protocol (NTP) daemon.                                              | [ntp.conf](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/config/ntpd/ntp.conf)                                 | [Man Page](https://linux.die.net/man/8/ntpd)                                   | N/A           | [BusyBox](https://busybox.net/)                             |
| ping           | Send ICMP ECHO_REQUEST to network hosts.                                         | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/ping)                                   | N/A           | [BusyBox](https://busybox.net/)                             |
| ping6          | Send ICMP ECHO_REQUEST to network hosts.                                         | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/ping6)                                  | N/A           | [BusyBox](https://busybox.net/)                             |
| route          | Show/manipulate the IP routing table.                                            | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/route)                                  | N/A           | [BusyBox](https://busybox.net/)                             |
| traceroute     | Print the route packets trace to network host.                                   | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/traceroute)                             | N/A           | [BusyBox](https://busybox.net/)                             |
| traceroute6    | Print the route packets trace to network host.                                   | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/traceroute6)                            | N/A           | [BusyBox](https://busybox.net/)                             |
| nslookup       | Query Internet name servers interactively.                                       | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/1/nslookup)                               | N/A           | [BusyBox](https://busybox.net/)                             |
| whois          | Client for the whois service.                                                    | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/1/whois)                                  | N/A           | [BusyBox](https://busybox.net/)                             |
| dhclient       | Dynamic Host Configuration Protocol Client.                                      | [dhclient.conf](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/config/dhclient/dhclient.conf)                   | [Man Page](https://linux.die.net/man/8/dhclient)                               | N/A           | [BusyBox](https://busybox.net/)                             |
| wpa_supplicant | Wi-Fi Protected Access client and IEEE 802.1X supplicant.                        | [wpa_supplicant.conf](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/config/wpa_supplicant/wpa_supplicant.conf) | [Man Page](https://linux.die.net/man/8/wpa_supplicant)                         | N/A           | [BusyBox](https://busybox.net/)                             |
| macchanger     | MAC Changer.                                                                     | N/A                                                                                                                              | [Man Page](https://manpages.ubuntu.com/manpages/xenial/man1/macchanger.1.html) | N/A           | [BusyBox](https://busybox.net/)                             |
| ss             | Another utility to investigate sockets.                                          | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/ss)                                     | N/A           | [BusyBox](https://busybox.net/)                             |
| dig            | DNS lookup utility.                                                              | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/1/dig)                                    | N/A           | [BusyBox](https://busybox.net/)                             |
| iwconfig       | Configure a wireless network interface.                                          | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/iwconfig)                               | N/A           | [BusyBox](https://busybox.net/)                             |
| iw             | Show/manipulate wireless devices and their configuration.                        | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/iw)                                     | N/A           | [BusyBox](https://busybox.net/)                             |
| mtr            | A network diagnostic tool.                                                       | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/mtr)                                    | N/A           | [BusyBox](https://busybox.net/)                             |
| iftop          | Display bandwidth usage on an interface by host.                                 | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/iftop)                                  | N/A           | [BusyBox](https://busybox.net/)                             |
| tcpdump        | Dump TCP traffic on a network.                                                   | N/A                                                                                                                              | [Man Page](https://linux.die.net/man/8/tcpdump)                                | N/A           | [BusyBox](https://busybox.net/)                             |
| udpdump        | Dump UDP traffic on a network.                                                   | N/A                                                                                                                              | [Man Page](https://man.archlinux.org/man/community/wireshark-cli/udpdump.1.en) | N/A           | [BusyBox](https://busybox.net/)                             |
| unbound        | Unbound DNS validating resolver 1.4.19.                                          | [unbound.conf](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/main/src/config/unbound/unbound.conf)                        | [Man Page](https://linux.die.net/man/8/unbound)                                | N/A           | [Unbound](https://www.nlnetlabs.nl/projects/unbound/about/) |
| i2pd           | Full-featured C++ implementation of I2P client.                                  |                                                                                                                                  | [Man Page](https://manpages.debian.org/testing/i2pd/i2pd.1.en.html)            | N/A           | [I2PD](https://github.com/PurpleI2P/i2pd)                   |
| tor            | The second-generation onion router.                                              |                                                                                                                                  | [Man Page](https://linux.die.net/man/1/tor)                                    | N/A           | [TOR](https://www.torproject.org/)                          |
| freenet        | A peer-to-peer platform for publishing and communication.                        |                                                                                                                                  | [Site](https://freenetproject.org/pages/documentation.html)                    | N/A           | [Freenet](https://freenetproject.org/)                      |
| gnunet         | A network protocol stack.                                                        |                                                                                                                                  | [Site](https://docs.gnunet.org/)                                               | N/A           | [GNU Net](https://www.gnunet.org/en/)                       |
| btpd           | BitTorrent Protocol Daemon.                                                      |                                                                                                                                  | [README](https://github.com/btpd/btpd/blob/master/README.md)                   | N/A           | [BTPD](https://github.com/btpd/btpd)                        |
| proxychains    | Forces any TCP connection made by any given application to follow through proxy. |                                                                                                                                  | [Man Page](https://man.cx/proxychains)                                         | N/A           | [Proxychains](https://github.com/haad/proxychains)          |
| hyperspace     | A Lightweight VPN Built on top of IPFS + Libp2p for Truly Distributed Networks.  | N/A                                                                                                                              | [README](https://github.com/hyprspace/hyprspace/blob/main/README.md)           | N/A           | [Hyperspace](https://github.com/hyprspace/hyprspace)        |

### Uncategorized Utilities
| Name        | Description                                                                                                                    | Configuration File                                                                                       | Documentation                                                   | Fork Homepage | Project Homepage                |
| :-----------| :------------------------------------------------------------------------------------------------------------------------------| :--------------------------------------------------------------------------------------------------------| :---------------------------------------------------------------| :-------------| :-------------------------------|
| basename    | Strip directory and suffix from filenames.                                                                                     | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/basename)                | N/A           | [BusyBox](https://busybox.net/) |
| cat         | Concatenate files and print on the standard output.                                                                            | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/cat)                     | N/A           | [BusyBox](https://busybox.net/) |
| chmod       | Change file mode bits.                                                                                                         | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/chmod)                   | N/A           | [BusyBox](https://busybox.net/) |
| chroot      | Run command or interactive shell with special root directory.                                                                  | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/chroot)                  | N/A           | [BusyBox](https://busybox.net/) |
| comm        | Compare two sorted files line by line.                                                                                         | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/comm)                    | N/A           | [BusyBox](https://busybox.net/) |
| cut         | Remove sections from each line of files.                                                                                       | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/cut)                     | N/A           | [BusyBox](https://busybox.net/) |
| date        | Print or set the system date and time.                                                                                         | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/date)                    | N/A           | [BusyBox](https://busybox.net/) |
| dd          | Convert and copy a file.                                                                                                       | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/dd)                      | N/A           | [BusyBox](https://busybox.net/) |
| dirname     | Strip non-directory suffix from file name.                                                                                     | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/dirname)                 | N/A           | [BusyBox](https://busybox.net/) |
| direnv      | Augments existing shells with a new feature that can load and unload environment variables depending on the current directory. | [direnvrc](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/home/.config/direnv/direnvrc) | [Site](https://direnv.net/)                                     | N/A           | [direnv](https://direnv.net/)   |
| echo        | Display a line of text.                                                                                                        | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/echo)                    | N/A           | [BusyBox](https://busybox.net/) |
| env         | Run a program in a modified environment.                                                                                       | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/env)                     | N/A           | [BusyBox](https://busybox.net/) |
| false       | Do nothing, unsuccessfully.                                                                                                    | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/false)                   | N/A           | [BusyBox](https://busybox.net/) |
| head        | Output the first part of files.                                                                                                | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/head)                    | N/A           | [BusyBox](https://busybox.net/) |
| install     | Copy files and set attributes.                                                                                                 | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/install)                 | N/A           | [BusyBox](https://busybox.net/) |
| sha256sum   | Compute and check SHA256 message digest.                                                                                       | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/sha256sum)               | N/A           | [BusyBox](https://busybox.net/) |
| sha512sum   | Compute and check SHA512 message digest.                                                                                       | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/sha512sum)               | N/A           | [BusyBox](https://busybox.net/) |
| od          | Dump files in octal and other formats.                                                                                         | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/od)                      | N/A           | [BusyBox](https://busybox.net/) |
| printf      | Format and print data.                                                                                                         | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/printf)                  | N/A           | [BusyBox](https://busybox.net/) |
| pwd         | Print name of current/working directory.                                                                                       | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/pwd)                     | N/A           | [BusyBox](https://busybox.net/) |
| sleep       | Delay for a specified amount of time.                                                                                          | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/sleep)                   | N/A           | [BusyBox](https://busybox.net/) |
| awk         | Pattern scanning and processing language.                                                                                      | N/A                                                                                                      | [Man Page](https://man7.org/linux/man-pages/man1/awk.1p.html)   | N/A           | [BusyBox](https://busybox.net/) |
| sed         | Stream editor for filtering and transforming text.                                                                             | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/sed)                     | N/A           | [BusyBox](https://busybox.net/) |
| cmp         | Compare two files byte by byte.                                                                                                | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/cmp)                     | N/A           | [BusyBox](https://busybox.net/) |
| diff        | Compare files line by line.                                                                                                    | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/diff)                    | N/A           | [BusyBox](https://busybox.net/) |
| patch       | Apply a diff file to an original.                                                                                              | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/patch)                   | N/A           | [BusyBox](https://busybox.net/) |
| sort        | Sort lines of text files.                                                                                                      | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/sort)                    | N/A           | [BusyBox](https://busybox.net/) |
| split       | Split a file into pieces.                                                                                                      | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/split)                   | N/A           | [BusyBox](https://busybox.net/) |
| stty        | Change and print terminal line settings.                                                                                       | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/stty)                    | N/A           | [BusyBox](https://busybox.net/) |
| tail        | Output the last part of files.                                                                                                 | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/tail)                    | N/A           | [BusyBox](https://busybox.net/) |
| tee         | Read from standard input and write to standard output and files.                                                               | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/tee)                     | N/A           | [BusyBox](https://busybox.net/) |
| test        | Check file types and compare values.                                                                                           | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/test)                    | N/A           | [BusyBox](https://busybox.net/) |
| touch       | Change file timestamps.                                                                                                        | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/touch)                   | N/A           | [BusyBox](https://busybox.net/) |
| tr          | Translate or delete characters.                                                                                                | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/tr)                      | N/A           | [BusyBox](https://busybox.net/) |
| true        | Do nothing, successfully.                                                                                                      | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/true)                    | N/A           | [BusyBox](https://busybox.net/) |
| truncate    | Shrink or extend the size of a file to the specified size.                                                                     | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/truncate)                | N/A           | [BusyBox](https://busybox.net/) |
| tty         | Print the file name of the terminal connected to standard input.                                                               | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/tty)                     | N/A           | [BusyBox](https://busybox.net/) |
| uname       | Print system information.                                                                                                      | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/uname)                   | N/A           | [BusyBox](https://busybox.net/) |
| uniq        | Report or omit repeated lines.                                                                                                 | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/uniq)                    | N/A           | [BusyBox](https://busybox.net/) |
| wc          | Print newline, word, and byte counts for each file.                                                                            | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/wc)                      | N/A           | [BusyBox](https://busybox.net/) |
| who         | Show who is logged on.                                                                                                         | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/who)                     | N/A           | [BusyBox](https://busybox.net/) |
| yes         | Output a string repeatedly until killed.                                                                                       | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/yes)                     | N/A           | [BusyBox](https://busybox.net/) |
| grep        | Print lines matching a pattern.                                                                                                | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/grep)                    | N/A           | [BusyBox](https://busybox.net/) |
| egrep       | Print lines matching a pattern.                                                                                                | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/egrep)                   | N/A           | [BusyBox](https://busybox.net/) |
| frgep       | Print lines matching a pattern.                                                                                                | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/fgrep)                   | N/A           | [BusyBox](https://busybox.net/) |
| xargs       | Build and execute command lines from standard input.                                                                           | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/xargs)                   | N/A           | [BusyBox](https://busybox.net/) |
| clear       | Clear the terminal screen.                                                                                                     | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/clear)                   | N/A           | [BusyBox](https://busybox.net/) |
| deallocvt   | Deallocate unused virtual consoles.                                                                                            | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/deallocvt)               | N/A           | [BusyBox](https://busybox.net/) |
| dumpkmap    | Print a binary keyboard translation table to standard output.                                                                  | N/A                                                                                                      | [Site](https://boxmatrix.info/wiki/Property:dumpkmap)           | N/A           | [BusyBox](https://busybox.net/) |
| fgconsole   | Print the number of the active VT.                                                                                             | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/fgconsole)               | N/A           | [BusyBox](https://busybox.net/) |
| kbd_mode    | Report or set the keyboard mode.                                                                                               | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/kbd_mode)                | N/A           | [BusyBox](https://busybox.net/) |
| loadfont    |                                                                                                                                |
| setfont     |                                                                                                                                |
| loadkmap    |                                                                                                                                |
| openvt      |                                                                                                                                |
| reset       | Terminal initialization.                                                                                                       | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/reset)                   | N/A           |
| resize      | Set TERMCAP and terminal settings to current xterm window size.                                                                | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/resize)                  | N/A           |
| setconsole  |                                                                                                                                |
| setkeycodes | Load kernel scancode-to-keycode mapping table entries.                                                                         | N/A                                                                                                      | [Man Page](https://linux.die.net/man/8/setkeycodes)             | N/A           |
| setlogcons  |                                                                                                                                |
| showkey     | Examine the codes sent by the keyboard.                                                                                        | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/showkey)                 | N/A           |
| getty       | Sets terminal mode, speed, and line discipline.                                                                                | N/A                                                                                                      | [Man Page](http://www.skrenta.com/rt/man/getty.1.html)          | N/A           |
| login       | Sign on.                                                                                                                       | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/login)                   | N/A           |
| nologin     |                                                                                                                                |
| passwd      | Update user's authentication.                                                                                                  | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/passwd)                  | N/A           |
| vlock       | Virtual Console lock program.                                                                                                  | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/vlock)                   | N/A           |
| chattr      | Change file attributes on a Linux file system.                                                                                 | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/chattr)                  | N/A           |
| lsattr      | List file attributes on a Linux second extended file system.                                                                   | N/A                                                                                                      | [Man Page](https://linux.die.net/man/1/lsattr)                  | N/A           |
| fdisk       | Partition table manipulator for Linux.                                                                                         | N/A                                                                                                      | [Man Page](https://linux.die.net/man/8/fdisk)                   | N/A           |
| gdisk       |
| cgdisk      |
| sgdisk      |
| lvm2        |
| depmod      |
| insmod      |
| lsmod       |
| modinfo     |
| modprobe    |
| rmmod       |
| acpid       |
| blkdiscard  |
| blkid       |
| blockdev    |
| chrt        |
| dmesg       |
| eject       |
| falloctate  |
| fatarrtr    |
| fbset       |
| hexdump     |
| xxd         |
| hwclock     |
| wget        |

## Version Control Utilities
| Name | Description                                                                                                                                         | Configuration File                                                                      | Documentation                                                            | Fork Homepage                                                       | Project Homepage                                       |
| :----| :---------------------------------------------------------------------------------------------------------------------------------------------------| :---------------------------------------------------------------------------------------| :------------------------------------------------------------------------| :-------------------------------------------------------------------| :------------------------------------------------------|
| git  |  Free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. | [git](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/home/.config/git) | [Site](https://git-scm.com/doc)                                          | N/A                                                                 | [Git](https://git-scm.com/)                            |

## Development Utilities
### Programming & Scripting interpreters/Compilers
| Name       | Description                                                                                                   | Configuration File                                                                    | Documentation                                                     | Fork Homepage | Project Homepage                                  |
| :----------| :-------------------------------------------------------------------------------------------------------------| :-------------------------------------------------------------------------------------| :-----------------------------------------------------------------| :-------------| :-------------------------------------------------|
| zsh        | A Unix shell that can be used as an interactive login shell and as a command interpreter for shell scripting. | [zsh](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/tree/main/src/home/.config/zsh) | [Site](https://zsh.sourceforge.io/Doc/Release/zsh_toc.html)       | N/A           | [Zsh](https://www.zsh.org/)                       |
| dash       | A modern POSIX-compliant implementation of Bourne shell.                                                      |                                                                                       | [Man Page](https://www.man7.org/linux/man-pages/man1/dash.1.html) | N/A           | [Dash](http://gondor.apana.org.au/~herbert/dash/) |
| sbcl       | A high performance Common Lisp compiler.                                                                      |                                                                                       | [Site](https://www.sbcl.org/manual/index.html)                    | N/A           | [SBCL](https://www.sbcl.org/)                     |
| gcc        |
| rust       |
| python3    |
| nodejs     |

### Programming & Scripting Build Systems
| Name            | Description                                                                                                   | Configuration File                                                                    | Documentation                                                     | Fork Homepage | Project Homepage                                  |
| :---------------| :-------------------------------------------------------------------------------------------------------------| :-------------------------------------------------------------------------------------| :-----------------------------------------------------------------| :-------------| :-------------------------------------------------|
| make            |
| cargo           |

### Programming & Scripting Package Managers
| Name            | Description                                                                                                   | Configuration File                                                                    | Documentation                                                     | Fork Homepage | Project Homepage                                  |
| :---------------| :-------------------------------------------------------------------------------------------------------------| :-------------------------------------------------------------------------------------| :-----------------------------------------------------------------| :-------------| :-------------------------------------------------|
| quicklisp       |
| python3-pip     |
| npm             |

### Programming & Scripting Linters
| Name            | Description                                                                                                   | Configuration File                                                                    | Documentation                                                     | Fork Homepage | Project Homepage                                  |
| :---------------| :-------------------------------------------------------------------------------------------------------------| :-------------------------------------------------------------------------------------| :-----------------------------------------------------------------| :-------------| :-------------------------------------------------|| pyright         |
| lua-check       |
| shellcheck      |
| vale            |

### Uncategorized Programming & Scripting Utilities
| Name            | Description                                                                                                   | Configuration File                                                                    | Documentation                                                     | Fork Homepage | Project Homepage                                  |
| :---------------| :-------------------------------------------------------------------------------------------------------------| :-------------------------------------------------------------------------------------| :-----------------------------------------------------------------| :-------------| :-------------------------------------------------|
| binutils        |
| glibc           |
| tree-sitter-cli |

## Privacy and Security Utilities
| Name      | Description                                                                                                                                                        | Configuration File                                                                                               | Documentation                  | Fork Homepage | Project Homepage                      |
| :---------| :------------------------------------------------------------------------------------------------------------------------------------------------------------------| :----------------------------------------------------------------------------------------------------------------| :------------------------------| :-------------| :-------------------------------------|
| keepassxc | Securely store passwords using industry standard encryption, quickly auto-type them into desktop applications, and use our browser extension to log into websites. | [keepassxc.init](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/blob/master/src/configs/keepassx/keepassxc.ini) | [Site](https://keepassxc.org/) | N/A           | [Keepassx](https://www.keepassx.org/) |



## Emulation Applications
| Name    | Description | Configuration File | Documentation | Fork Homepage | Project Homepage |
| :-------| :-----------| :------------------| :-------------| :-------------| :----------------|
| dosbox  |
| scummvm |
| wine    |
| xemu    |
| epsxe   |
| pcsx2   |
| rpcs3   |
| dolphin |

## Communication Applications
| Name        | Description                                                                     | Configuration File                                                                          | Documentation                            | Fork Homepage | Project Homepage            |
| :-----------| :-------------------------------------------------------------------------------| :-------------------------------------------------------------------------------------------| :----------------------------------------| :-------------| :---------------------------|
| profanity   |
| discord     |
| gamejolt    |
| mutt        |
| firefox     |
| nyxt        |
| tor-browser |

## Miscellaneous Utilities
| Name     | Description                                                | Configuration File                                                                          | Documentation                                                 | Fork Homepage | Project Homepage                                               |
| :--------| :----------------------------------------------------------| :-------------------------------------------------------------------------------------------| :-------------------------------------------------------------| :-------------| :--------------------------------------------------------------|
| tmux     | A terminal multiplexer.                                    | [tmux](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/tree/master/src/home/.config/tmux)   | [Site](https://github.com/tmux/tmux/wiki)                     | N/A           | [tmux](https://github.com/tmux/tmux)                           |
| sfeed    | Sfeed is a RSS and Atom parser (and some format programs). | [sfeed](https://gitlab.com/FOSSilized_Daemon/dotfiles/-/tree/master/src/home/.config/sfeed) | [Site](https://codemadness.org/sfeed-simple-feed-parser.html) | N/A           | [sfeed](https://codemadness.org/sfeed-simple-feed-parser.html) |
| firejail |
| openrgb  |
