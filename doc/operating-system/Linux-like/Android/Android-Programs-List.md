# Introduction
In this document you will find a concrete list of all the programs I use along with helpful links and resources.

## Core Operating System
| Name    | Description    | Configuration File    | Documentation    | Fork Homepage    | Project Homepage    |
| :-------| :--------------| :---------------------| :----------------| :----------------| :-------------------|

## Communication Applications
| Name        | Description                                         | Configuration File    | Documentation                                                | Fork Homepage    | Project Homepage                          |
| :-----------| :---------------------------------------------------| :---------------------| :------------------------------------------------------------| :----------------| :-----------------------------------------|
| bromite     | Chromium fork with ad blocking and enhanced privacy | N/A                   | [Site](https://github.com/bromite/bromite/wiki)              | N/A              | [Bromite](https://www.bromite.org/)       |
| tor-browser |


## Miscellaneous Utilities
| Name          | Description                                                                                             | Configuration File    | Documentation                                                | Fork Homepage    | Project Homepage                          |
| :-------------| :-------------------------------------------------------------------------------------------------------| :---------------------| :------------------------------------------------------------| :----------------| :-----------------------------------------|
| dir           | A simple, beautiful, free and open source file manager.                                                 | N/A                   | N/A                                                          | N/A              | [Dir](https://veniosg.github.io/Dir/)     |
| kiss launcher | Launcher for Android requiring nearly no memory to run.                                                 | N/A                   | N/A                                                          | N/A              | [KISSLAUNCHER](https://kisslauncher.com/) |
| aurora store  | An open-source alternative Google Play Store frontend client with privacy and modern design in mind.    | N/A                   | [Site](https://auroraoss.com/guides/wiki-home/#aurora-store) | N/A              | [AuroraOSS](https://auroraoss.com/)       |
| f-droid       | An installable catalogue of FOSS (Free and Open Source Software) applications for the Android platform. | N/A                   | [Site](https://f-droid.org/en/docs/)                         | N/A              | [F-Droid](https://f-droid.org/)           |
