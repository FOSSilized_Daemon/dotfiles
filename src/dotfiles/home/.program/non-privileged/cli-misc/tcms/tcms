#!/usr/bin/env sh
set -euf

#### tcms is a simple POSIX compliant shell-script used to create, and or attach to, a tmux(1) session. When tcms is
#### executed it will do the following:
####    1. Set the value of $session_name to either the value of TMUX_SESSION_NAME or 'main-session'.
####    2. If a tmux(1) session with the name TMUX_SESSION_NAME does not exist, then create it.
####    3. If the arguments "-t" or "--terminal" are passed, then spawn a new TERMINAL and attach to the session.
####       Otherwise, just attach to the session in the current tty/terminal.

#####################
### Handle Checks ###
#####################
check_for_environment_variables() {
	if [ -z "${TMUX_SESSION_NAME}" ]; then
		session_name='main-session'
	else
		session_name="${TMUX_SESSION_NAME}"
	fi
}

######################
### Handle Session ###
######################
create_session() {
	## If $session_name does not exist, then create it and configure all needed windows.
	## Otherwise, attach to it.
	if ! tmux has-session -t "${session_name}"; then
		## Create a new tmux(1) session named $session_name.
		tmux new-session -d -s "${session_name}"

		## Rename the first window to 'sysadmin'.
		tmux rename-window -t "${session_name}":1 'sysadmin'

		## Create a new window named 'programming', and change into the development parent directory.
		tmux new-window -d -t "${session_name}":2 -n 'programming'
		tmux send-keys -t "${session_name}":2.1 'cd ~/document/development && clear && pwd' C-m

		## Create a new window named 'social-media'.
		tmux new-window -d -t "${session_name}":3 -n 'social-media'
	fi
}

attach_to_session() {
		## Attach to $session_name.
		if [ "${1}" -eq 1 ]; then
			"${TERMINAL}" -- tmux attach-session -t "${session_name}"
		else
			tmux attach-session -t "${session_name}"
		fi
}

##########################
### Main Functionality ###
##########################
main() {
	use_terminal=0

	while [ "${#}" -gt 0 ]; do
		case "${1}" in
			"-t"|"--terminal")
				use_terminal=1
				break;;
			*)
				use_terminal=0
				break;;
		esac
		shift
	done

	check_for_environment_variables
	create_session
	attach_to_session "${use_terminal}"
}

main "${@}"
