-- The kommentary.lua file contains all lua related to configuring settings for the kommentary plugin.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for the kommentary plugin.

local kommentary = require('kommentary.config')

kommentary.configure_language('default', {
	-- Prefer single-line comments.
	prefer_single_line_comments = true,

	-- Use consistant indentation.
	use_consistent_indentation = true,

	-- Ignore whitespace when commenting.
	ignore_whitespace = true,
})
