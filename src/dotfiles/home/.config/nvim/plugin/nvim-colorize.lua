-- The nvim-colorize.lua file contains all lua related to configuring settings for the nvim-colorize plugin.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for the nvim-colorize plugin.

local colorizer = require 'colorizer'

colorizer.setup({
	DEFAULT_OPTIONS = {
		-- Highlight RGB hex codes.
		RGB = true;

		-- Highlight RRGGBB hex codes.
		RRGGBB = true;

		-- Highlight color names (example: Blue, Red, or Yellow).
		names = true;

		-- Highlight #RRGGBBAA hex codes.
		RRGGBBAA = true;

		-- Highlight CSS rgb() and rgba() functions.
		rgb_fn = true;

		-- Highlight CSS hsl() and hsla() functions.
		hsl_fn = true;

		-- Enable all CSS features and functions.
		css    = true;
		css_fn = true;

		-- Highlight the background of the text.
		mode = 'background';
	}
})
