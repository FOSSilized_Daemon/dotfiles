-- The nvim-autopairs.lua file contains all lua related to configuring settings for the nvim-autopairs plugin.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for the nvim-autopairs plugin.

autopairs = require('nvim-autopairs')

autopairs.setup({
	-- Disable automatic pairing when recording or executing a macro.
	disable_in_macro = true,

	-- Enable automatic pairing when inserting after visual block mode.
	disable_in_visualblock = false,

	-- Characters to ignore.
	ignored_next_char = string.gsub([[ [%w%%%'%[%"%.] ]],"%s+", ""),

	-- Move the cursor to the right of the inserting pair.
	enable_moveright = true,

	-- Add bracket pairs after quotes.
	enable_afterquote = true,

	-- Check brackets in the same line.
	enable_check_bracket_line = true,

	-- Disable checking for treesitter.
	check_ts = false,

	-- Enable mapping the backspace key.
	map_bs = true,

	-- Disable mapping control + h to delete a pair.
	map_c_h = false,

	-- Disable mapping control + w to delete a pair.
	map_c_w = false,
})
