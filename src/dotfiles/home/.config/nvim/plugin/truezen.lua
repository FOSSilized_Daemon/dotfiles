-- The truezen.lua file contains all lua related to configuring settings for the truezen plugin.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for the truezen plugin.

local true_zen = require("true-zen")

true_zen.setup({
	ui = {
		bottom = {
			-- Do not show the statusline.
			laststatus = 0,

			-- Do not show the line and column number of the cursor.
			ruler = false,

			-- Do not show the current mode.
			showmode = false,

			-- Do not show partial command in the last line of the screen.
			showcmd = false,

			-- Only show one screen line for the command-line.
			cmdheight = 1,
		},

		top = {
			-- Do not show the tabline.
			showtabline = 0,
		},

		left = {
			-- Do not show line numbers.
			number = false,

			-- Do not show relative line numbers.
			relativenumber = false,

			-- Do not draw the signcolumn.
			signcolumn = "no",
		},
	},

	modes = {
		ataraxis = {
			-- Left padding to use.
			left_padding = 32,

			-- Right padding to use.
			right_padding = 32,

			-- Top padding to use.
			top_padding = 1,

			-- Bottom padding to use.
			bottom_padding = 1,

			-- Ideal writing area width.
			ideal_writing_area_width = {0},

			-- Set left_padding and right_padding instead of
			-- using the configured options.
			auto_padding = true,

			-- Keep default fold fillchars.
			keep_default_fold_fillchars = true,

			-- Do not use a custom highlight group for
			-- the background.
			custom_bg = { "none", "" },

			-- Allow TrueZen to interact with the background.
			bg_configuration = true,

			-- Instead of toggling TrueZen, for some reason,
			-- just quit when commands like "q" are used.
			quit = nil,

			-- Avoid restoring TrueZen's layout after
			-- exiting a floating window
			ignore_floating_windows = true,

			-- List of highlight groups to change.
			affected_higroups = {
				NonText = true,
				FoldColumn = true,
				ColorColumn = true,
				VertSplit = true,
				StatusLine = true,
				StatusLineNC = true,
				SignColumn = true,
			},
		},
	},

	integrations = {
		-- Do not integrate with Git Gutter.
		vim_gitgutter = false,

		-- Do not integrate with Galaxyline.
		galaxyline = false,

		-- Do integrate with tmux(1).
		tmux = true,

		-- Do not integrate with Git Signs.
		gitsigns = false,

		-- Do not integrate with Bufferline.
		nvim_bufferline = false,

		-- Do not integrate with Limelight.
		limelight = false,

		-- Do integrate with Twilight.
		twilight = true,

		-- Do not integrate with Airline.
		vim_airline = false,

		-- Do not integrate with Powerline.
		vim_powerline = false,

		-- Do not integrate with Signify.
		vim_signify = false,

		-- Do not integrate with Express Line.
		express_line = false,

		-- Do not integrate with LuaLine.
		lualine = false,

		-- Do not integrate with LightLine.
		lightline = false,

		-- Do not integrate with Feline.
		feline = false
	},

	misc = {
		-- Enable On/Off commands.
		on_off_commands = true,

		-- DIsable commands for UI elements.
		ui_elements_commands = false,

		-- Allow changing the cursor based
		-- on the current mode.
		cursor_by_mode = true,
	}
})
