-- The nvim-cmp.lua file contains all lua related to configuring settings for the nvim-cmp plugin
-- and all of its' related plugins. When this file is loaded, the below lua will do the following:
--    1. Configure settings for the nvim-cmp plugin.
--    2. Configure settings for the cmp-dictionary plugin.

----------------
--- Nvim CMP ---
----------------
-- local cmp = require('cmp')

-- local lsp_icons = {
-- 	Class         = "ﴯ",
-- 	Color         = "",
-- 	Constant      = "",
-- 	Constructor   = "⌘",
-- 	Enum          = "",
-- 	EnumMember    = "",
-- 	Event         = "",
-- 	Field         = "ﰠ",
-- 	File          = "",
-- 	Folder        = "",
-- 	Function      = "",
-- 	Interface     = "",
-- 	Keyword       = "廓",
-- 	Method        = "",
-- 	Module        = "",
-- 	Operator      = "",
-- 	Property      = "ﰠ",
-- 	Reference     = "",
-- 	Snippet       = "",
-- 	Struct        = "פּ",
-- 	Text          = "",
-- 	TypeParameter = "",
-- 	Unit          = "塞",
-- 	Value         = "",
-- 	Variable      = "",
-- }

-- cmp.setup {
-- 	mapping = cmp.mapping.preset.insert({
-- 		-- Use standard vim(1) mappings for completion.
-- 		["<C-n>"]     = cmp.mapping.select_next_item,
-- 		["<C-p>"]     = cmp.mapping.select_prev_item,
-- 		["<C-d>"]     = cmp.mapping.scroll_docs(-4),
-- 		["<C-u>"]     = cmp.mapping.scroll_docs(4),
-- 		["<C-e>"]     = cmp.mapping.abort(),
-- 		["<c-space>"] = cmp.mapping.complete({ select = true }),

-- 		-- Use 'TAB' and 'Shift TAB' to cycle through the completion list.
-- 		["<Tab>"] = function(fallback)
-- 			if cmp.visible() then
-- 				cmp.select_next_item()
-- 			else
-- 				fallback()
-- 			end
-- 		end,

-- 		["<S-Tab>"] = function(fallback)
-- 			if cmp.visible() then
-- 				cmp.select_prev_item()
-- 			else
-- 				fallback()
-- 			end
-- 		end,
-- 	}),

-- 	sources = {
-- 		-- Provide completion from the current buffer, dictionary files,
-- 		-- file system paths, and language server protocol.
-- 		{ name = "buffer",     keyword_length = 1 },
-- 		{ name = "dictionary", keyword_length = 1 },
-- 		{ name = "spell",      keyword_length = 1 },
-- 		{ name = "path",       keyword_length = 1 },
-- 		{ name = "nvim_lsp",   keyword_length = 1 },
-- 	},

-- 	formatting = {
-- 		-- Reformat the completion menu to show an icon for the type of completion
-- 		-- item and then the actual completion item.
-- 		fields = { "kind", "abbr" },

-- 		format = function(_, vim_item)
-- 			vim_item.menu = vim_item.kind
-- 			vim_item.kind = lsp_icons[vim_item.kind]
-- 			return vim_item
-- 		end,
-- 	},

-- 	completion = {
-- 		-- Use a rounded border.
-- 		border = "rounded",

-- 		-- Fix accented letters breaking completion.
-- 		keyword_pattern = [[\k\+]],
-- 	},

-- 	window = {
-- 		-- Wrap the completion menu window in a border.
-- 		completion = cmp.config.window.bordered(),

-- 		-- Wrap the completion menu's documentation window in a border.
-- 		documentation = cmp.config.window.bordered(),
-- 	},

-- 	experimental = {
-- 		-- Utilize the new menu.
-- 		entries = "native",

-- 		-- Show ghost text for completion options.
-- 		ghost_text = true,
-- 	},
-- }

----------------------
--- CMP Dictionary ---
----------------------
-- local env            = vim.env
-- local fn             = vim.fn
-- local cmp_dictionary = require("cmp_dictionary")

-- -- Load all files from 'HOME/.usr/share/dict' and '/usr/share/dict/' to be used as dictionary files.
-- local dictionary_directories = env.HOME .. '/.usr/share/dict/' .. ',' .. '/usr/share/dict/'
-- local dictionary_files       = fn.globpath(dictionary_directories, '*', 0, 1)

-- require("cmp_dictionary").setup({
-- 	dic = {
-- 		["*"] = dictionary_files,
-- 	},

-- 	-- Only show candidates with an exact prefix match.
-- 	exact = 2,

-- 	-- Do not ignore the case of the first character.
-- 	first_case_insensitive = false,

-- 	-- Do not ctivate document using external command.
-- 	document = false,

-- 	-- Use wn(1) for the above document tool.
-- 	document_command = "wn %s -over",

-- 	-- Perform the initialization in a separate thread.
-- 	async = true,

-- 	-- Cache at least 10 dictionaries.
-- 	capacity = 5,

-- 	-- Do not output debug messages.
-- 	debug = false,
-- })
