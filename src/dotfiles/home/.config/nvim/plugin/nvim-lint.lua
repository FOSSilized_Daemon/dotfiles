-- The nvim-lint.lua file contains all lua related to configuring settings for the nvim-lint plugin.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for the nvim-lint plugin.

lint = require('lint')

lint.linters_by_ft = {
	-- Set a list of available linters.
	lua      = { 'luacheck' },
	shell    = { 'shellcheck' },
}

lint.linters.shellcheck = {
	-- Ensure shellcheck(1) is running
	-- in POSIX mode.
	args = { '-s', 'sh' },
}
