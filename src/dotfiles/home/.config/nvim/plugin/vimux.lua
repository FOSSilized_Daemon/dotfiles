-- The vimux.lua file contains all lua related to configuring settings for the vimux plugin.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for the vimux plugin.

local plugin = vim.g

-- Set height of the tmux(1) split.
plugin.VimuxHeight = '25'

-- Use a vertical split for tmux(1).
plugin.VimuxOrientation = 'v'

-- Use the nearest existing tmux()1) pane or window
-- instead of opening a new one.
plugin.VimuxUseNearest = 1

-- Set the string to use when prompting to run
-- a given command.
plugin.VimuxPromptString = 'Run? '

-- Use panes for the Vimux window.
plugin.VimuxRunnerType = 'pane'
