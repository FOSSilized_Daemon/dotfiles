-- The key-binding.lua file contains all lua related to key-bindings. When this file is loaded the below
-- lua will do the following:
--    1. Load all needed modules from "XDG_CONFIG_HOME/nvim/lua/module/key-binding/".

-- Load all needed modules.
require('module/key-binding/general')
require('module/key-binding/insert')
require('module/key-binding/normal')
require('module/key-binding/universal')
require('module/key-binding/visual')
