-- The ui.lua file contains all lua related to user interface settings. When this file is loaded the below
-- lua will do the following:
--    1. Load all needed modules from "XDG_CONFIG_HOME/nvim/lua/module/ui/".
--    2. Configure user interface settings.

local opt = vim.opt

-- Load all needed modules.
require('module/ui/char')
require('module/ui/column')
require('module/ui/number')
require('module/ui/statusline')
require('module/ui/syntax')

-- Do not show the tabline.
opt.showtabline = 0
