-- The lsp.lua file contains all lua related to setting up the language server protocol settings. When this
-- file is loaded the below lua will do the following:
--    1. Set language server protocol settings.

vim.lsp.handlers['textDocument/publishDiagnostics'] = function(_, _, params, client_id, _)
	local uri           = params.uri
	local buffer_number = vim.uri_to_bufnr(uri)
	local diagnostics   = params.diagnostics

	local config = {
		-- Underline the reference object.
		underline = true,

		-- Do not display virtual_text.
		virtual_text = false,

		-- Enable signs.
		signs = true,

		-- Update diagnostics on insert.
		update_in_insert = true,
	}

	if not buffer_number then
		return
	end

	-- Display the LSP server name along with its' message.
	for i,v in ipairs(diagnostics) do
		diagnostics[i].message = string.format('%s: %s', v.source, v.message)
	end

	vim.lsp.diagnostic.save(diagnostics, buffer_number, client_id)

	if not vim.api.nvim_buf_is_loaded(buffer_number) then
		return
	end

	vim.lsp.diagnostic.display(diagnostics, buffer_number, client_id, config)
end
