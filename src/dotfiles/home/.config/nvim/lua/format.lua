-- The format.lua file contains all lua related to format settings. When this file is loaded the below
-- lua will do the following:
--    1. Load all needed modules from "XDG_CONFIG_HOME/nvim/lua/module/format/".

-- Load all needed modules.
require('module/format/file')
require('module/format/tab')
