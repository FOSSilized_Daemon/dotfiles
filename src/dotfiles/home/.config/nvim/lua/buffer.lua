-- The buffer.lua file contains all lua related to configuring buffer settings. When this file is loaded the below
-- lua will do the following:
--    1. Load all needed modules from "XDG_CONFIG_HOME/nvim/lua/module/buffer/".

-- Load all needed modules.
require('module/buffer/hidden')
