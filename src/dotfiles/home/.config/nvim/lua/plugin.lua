-- The plugin.lua file contains all lua related to plugins. When this file is loaded the below
-- lua will do the following:
--    1. Load all needed modules from "XDG_CONFIG_HOME/nvim/lua/module/plugin/".

-- Load all needed modules.
require('module/plugin/disable')
require('module/plugin/load')
