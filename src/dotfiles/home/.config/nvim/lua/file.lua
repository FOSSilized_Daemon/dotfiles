-- The file.lua file contains all lua related to configuring file and directory locations.
-- When this file is loaded the below lua will do the following:
--    1. Load all needed modules from "XDG_CONFIG_HOME/nvim/lua/module/file/".

-- Load all needed modules.
require('module/file/backup')
require('module/file/swap')
require('module/file/undo')
