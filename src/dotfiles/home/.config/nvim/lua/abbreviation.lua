-- The abbreviation.lua file contains all lua related to configuring abbreviation settings.
-- When this file is loaded the below lua will do the following:
--    1. Configure all abbreviation settings.

local cmd           = vim.cmd
local abbreviations = {
	teh = 'the',
	abt = 'about'
}

local function create_abbreviation(input, replace)
	abbreviation_command = 'abbr ' .. input .. ' ' .. replace
	cmd(abbreviation_command)
end

for input,replace in pairs(abbreviations) do
	create_abbreviation(input, replace)
end
