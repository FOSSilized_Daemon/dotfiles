-- The miscellaneous.lua file contains all lua related to setting miscellaneous settings. When this
-- file is loaded the below lua will do the following:
--    1. Set all miscellaneous shell settings.

local opt = vim.opt

-- Open new splits below and to the right of the current window.
opt.splitbelow = true
opt.splitright = true

-- Keep ten lines visible above and below the cursor at all times.
opt.scrolloff = 10

-- When side scrolling a line that has gone off of the page scroll the line five character at a time.
opt.sidescroll = 5

-- Always keep a minimum of five columns to the left and right of the cursor if 'nowrap' is enabled.
opt.sidescrolloff = 5

-- Ignore case when searching.
opt.ignorecase = true

-- If a query starts with a capital letter, then match based on case.
opt.smartcase = true

-- Perform incremental searches.
opt.incsearch = true

-- Highlight search matches.
opt.hlsearch = true

-- Automatically read any file that has been changed outside of vim.
opt.autoread = true

-- Save up to 10,000 lines of command and menu history.
opt.history = 10000

-- Do not show the current mode.
opt.showmode = false

-- Show commands on the last part of the screen.
opt.showcmd = true

-- Enable line wrapping.
opt.wrap = true

-- Set the fold method to 'syntax'.
opt.foldmethod = 'syntax'

-- Automatically indent newlines.
opt.autoindent  = true
opt.smartindent = true

-- Do not redraw the screen when running a macro.
opt.lazyredraw = true
opt.ttyfast    = true

-- Enhance command-line completion.
opt.wildmenu = true

-- Show 5 items in the pop-up menu.
opt.pumheight = 5

-- Update the cursor hold time.
opt.updatetime = 250

-- Add more matchpairs.
opt.matchpairs:append('<:>')

-- Copy and paste the selection to and from the system clipboard.
opt.clipboard:prepend('unnamedplus')
