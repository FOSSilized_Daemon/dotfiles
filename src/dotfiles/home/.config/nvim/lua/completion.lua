-- The completion.lua file contains all lua related to configuring completion settings.
-- When this file is loaded the below lua will do the following:
--    1. Configure all completion settings.

local opt = vim.opt
local env = vim.env
local fn  = vim.fn

-- Add the current spell checking dictionaries to the completion options when spelling check in enabled.
opt.complete:append({ 'kspell' })

-- Load all files from 'HOME/.usr/share/dict' and '/usr/share/dict/' to be used as dictionary files.
local dictionary_directories = env.HOME .. '/.usr/share/dict/' .. ',' .. '/usr/share/dict/'
local dictionary_files       = fn.globpath(dictionary_directories, '*', 0, 1)

for _,file in pairs(dictionary_files) do
	opt.dictionary:append(file)
end

-- Load all files from 'HOME/.usr/share/thesaurus/' and '/usr/share/thesaurus/' to be used as thesaurus files.
local thesaurus_directories = env.HOME .. '/.usr/share/thesaurus/' .. '/usr/share/thesaurus/'
local thesaurus_files       = fn.globpath(thesaurus_directories, '*', 0, 1)

for _,file in pairs(thesaurus_files) do
	opt.thesaurus:append(file)
end

-- Show the pop-up menu for completion and force the user to select a completion.
opt.completeopt = 'menuone,noselect'

-- Do not give completion message prompts.
opt.shortmess:append('c')

-- Add all files and directories to path.
opt.path:append({ '**' })
