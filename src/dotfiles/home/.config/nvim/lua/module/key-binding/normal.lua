-- The normal.lua file contains all lua related to normal key-binding settings. When this file is loaded the below
-- lua will do the following:
--    1. Configure normal key-binding settings.

local fzf_lua    = require('fzf-lua')
local fn         = vim.fn
local opt_local  = vim.opt_local
local cmd        = vim.cmd
local keymap     = vim.api.nvim_set_keymap
local keymap_set = vim.keymap.set

-----------
--- Fzf ---
-----------
function list_files_in_cwd()
	fzf_lua.fzf_exec('find . -type f', {
	colorize_output = function(files)
		return fzf_lua.utils.ansi.codes.cyan(files)
	end
	})
end

function list_non_hidden_files_in_cwd()
	fzf_lua.fzf_exec("find . -not -path '*/.*' -type f -exec basename {} \\;", {
	colorize_output = function(files)
		return vim.loop.cwd() .. '/' .. fzf_lua.utils.ansi.codes.cyan(files)
	end
	})
end

function list_hidden_files_in_cwd()
	fzf_lua.fzf_exec("find . -path '*/.*' -type f -exec basename {} \\;", {
	colorize_output = function(files)
		return vim.loop.cwd() .. '/' .. fzf_lua.utils.ansi.codes.cyan(files)
	end
	})
end

function list_backup_files()
	fzf_lua.files({ cwd = '~/.usr/share/nvim/backup-files/' })
end

function list_files_in_git_repository()
	fzf_lua.git_files()
end

function list_buffers()
	fzf_lua.buffers()
end

-------------------
--- Compiler(s) ---
-------------------
function compile_buffer()
	-- Compile the current file based on its' file type.
	if opt_local.filetype:get() == 'python' then
		cmd([[ VimuxRunCommand('clear; python3 ' . bufname('%')) ]])
	elseif opt_local.filetype:get() == 'sh' then
		cmd([[ VimuxRunCommand('clear; sh ' . bufname('%')) ]])
	elseif opt_local.filetype:get() == 'c' or opt_local.filetype:get() == 'cpp' then
		cmd([[ VimuxRunCommand('clear; make') ]])
	elseif opt_local.filetype:get() == 'nroff' then
		cmd([[ VimuxRunCommand('clear; man ./' . bufname('%')) ]])
	elseif opt_local.filetype:get() == 'lisp' then
		cmd([[ VimuxRunCommand('clear; sbcl --script ' . bufname('%')) ]])
	else
		cmd([[ VimuxRunCommand('clear') ]])
	end
end

function prompt_to_compile_buffer()
	-- Prompt to compile the current file based on its' file type.
	if opt_local.filetype:get() == 'python' then
		cmd([[ VimuxPromptCommand('clear; python3 ' . bufname('%')) ]])
	elseif opt_local.filetype:get() == 'sh' then
		cmd([[ VimuxPromptCommand('clear; sh ' . bufname('%')) ]])
	elseif opt_local.filetype:get() == 'c' then
		cmd([[ VimuxPromptCommand('clear; make') ]])
	else
		cmd([[ VimuxPromptCommand('clear') ]])
	end
end

-- Write changes to disk.
keymap('n', 'Zz', ':update<CR>', { noremap = true, silent = true })

-- Toggle line wrapping.
keymap('n', '<Leader>l', ':set wrap!<CR>', { noremap = true, silent = true })

-- List non-hidden files within the current working directory.
keymap_set('n', '<Leader>f', function() list_non_hidden_files_in_cwd() end, { noremap = true })

-- List hidden files within the current working directory.
keymap_set('n', '<Leader>F', function() list_hidden_files_in_cwd() end, { noremap = true })

-- List all files within the current working directory.
keymap_set('n', '<Leader>d', function() list_files_in_cwd() end, { noremap = true })

-- Open a backup file in a split.
keymap_set('n', '<Leader>B', function() list_backup_files() end, { noremap = true })

-- List all files within the current git repository.
keymap_set('n', '<Leader>g', function() list_files_in_git_repository() end, { noremap = true })

-- List all nvim(1) buffers.
keymap_set('n', '<Leader>b', function() list_buffers() end, { noremap = true })

-- Compile the current file.
keymap('n', '<Leader>c', ':lua compile_buffer()<CR>',           { noremap = true, silent = true })
keymap('n', '<Leader>C', ':lua prompt_to_compile_buffer()<CR>', { noremap = true })

-- Navigate splits.
keymap('n', '<C-j>', '<C-W><C-J>', { noremap = true, silent = true })
keymap('n', '<C-k>', '<C-W><C-K>', { noremap = true, silent = true })
keymap('n', '<C-l>', '<C-W><C-L>', { noremap = true, silent = true })
keymap('n', '<C-h>', '<C-W><C-H>', { noremap = true, silent = true })

-- Resize splits.
keymap('n', '<M-j>', ':resize -2<CR>',          { noremap = true, silent = true})
keymap('n', '<M-k>', ':resize +2<CR>',          { noremap = true, silent = true})
keymap('n', '<M-h>', ':vertical resize -2<CR>', { noremap = true, silent = true})
keymap('n', '<M-l>', ':vertical resize +2<CR>', { noremap = true, silent = true})
