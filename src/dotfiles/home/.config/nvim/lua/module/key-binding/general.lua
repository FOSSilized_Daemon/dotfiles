-- The general.lua file contains all lua related to general key-binding settings. When this file is loaded the below
-- lua will do the following:
--    1. Configure general key-binding settings.

local opt    = vim.opt
local plugin = vim.g

-- Enable mouse support for all modes.
opt.mouse = 'a'

-- Make sure backspace always deletes characters.
vim.backspace = 'indent,eol,start'

-- Set a reasonable time sequence for key mappings.
vim.timeoutlen  = 1000
vim.ttimeoutlen = 0

-- Set ',' as the leader key.
plugin.mapleader = ','
