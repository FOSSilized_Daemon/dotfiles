-- The universal.lua file contains all lua related to univseral key-binding settings.
-- When this file is loaded the below lua will do the following:
--    1. Configure univseral key-binding settings.

local keymap = vim.api.nvim_set_keymap

-- Toggle spell checking in the current buffer.
keymap('', '<F2>', ':setlocal spell! spelllang<CR>', { noremap = true, silent = true })

-- Toggle writer mode.
keymap('', '<F3>', ':TZAtaraxis<CR>', { noremap = true, silent = true })

-- Source 'HOME/.config/nvim/init.lua'.
keymap('', '<F5>', ':source ~/.config/nvim/init.lua<CR>', { noremap = true })
