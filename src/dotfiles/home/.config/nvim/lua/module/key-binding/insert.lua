-- The insert.lua file contains all lua related to insert key-binding settings. When this file is loaded the below
-- lua will do the following:
--    1. Configure insert key-binding settings.

-- Variable declarations.
local keymap = vim.api.nvim_set_keymap

-- Use emacs(1) bindings in insert mode.
keymap('i', '<C-A>',      '<Home>',       { noremap = true, silent = true })
keymap('i', '<C-E>',      '<End>',        { noremap = true, silent = true })
keymap('i', '<C-B>',      '<Left>',       { noremap = true, silent = true })
keymap('i', '<C-F>',      '<Right>',      { noremap = true, silent = true })
keymap('i', 'â',          '<C-Left>',     { noremap = true, silent = true })
keymap('i', 'æ',          '<C-Right>',    { noremap = true, silent = true })
keymap('i', '<C-K>',      '<Esc>lDa',     { noremap = true, silent = true })
keymap('i', '<C-U>',      '<Esc>d0xi',    { noremap = true, silent = true })
keymap('i', '<C-Y>',      '<Esc>Pa',      { noremap = true, silent = true })
keymap('i', '<C-X><C-S>', '<Esc>:w<CR>a', { noremap = true, silent = true })
