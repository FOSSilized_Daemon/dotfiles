-- The visual.lua file contains all lua related to visual key-binding settings.
-- When this file is loaded the below lua will do the following:
--    1. Expose functions for visual key-bindings.
--    2. Configure visual key-binding settings.

local fn     = vim.fn
local api    = vim.api
local cmd    = vim.cmd
local keymap = vim.keymap.set

current_selection = function()
	local mode = api.nvim_get_mode().mode
	local from = fn.getpos("v")
	local to   = fn.getcurpos()

	if from[2] > to[2] then
		from, to = to, from
	end

	if mode == "V" or mode == "CTRL-V" then
		return api.nvim_buf_get_lines(0, from[2] - 1, to[2], false)
	end

	-- If on the same line but from right to left.
	if from[2] == to[2] and from[3] > to[3] then
		from, to = to, from
	end

	local num_lines = to[2] - from[2] + 1
	local lines = api.nvim_buf_get_lines(0, from[2] - 1, to[2], false)
	lines[1] = string.sub(lines[1], from[3], -1)

	if num_lines == 1 then
		lines[num_lines] = string.sub(lines[num_lines], 1, to[3] - from[3] + 1)
	else
		lines[num_lines] = string.sub(lines[num_lines], 1, to[3])
	end

	return table.concat(lines, "\n")
end

function plumb(string)
	if string:find('^http') or string:find('^www') ~= nil then
		cmd([[ !"${BROWSER}" -new-tab ]] .. string)
	elseif string:find('^>') ~= nil then
		cmd([[ ! ]] .. string:sub(2))
	else
		cmd([[ !"${BROWSER}" -new-tab ]] .. string)
	end
end

-- Plumb the currently highlighted selection.
keymap('v', '<Leader>o', function() plumb(current_selection()) end, { noremap = true, silent = true })

-- Move visually selected lines up or down in visual mode.
keymap('v', 'K', ':m .-2<CR>==',      { noremap = true, silent = true })
keymap('v', 'J', ':m .+1<CR>==',      { noremap = true, silent = true })
keymap('v', 'K', ':m \'<-2<CR>gv=gv', { noremap = true, silent = true })
keymap('v', 'J', ':m \'>+1<CR>gv=gv', { noremap = true, silent = true })

-- Tab over visually selected lines.
keymap('v', '<', '<gv', { noremap = true, silent = true })
keymap('v', '>', '>gv', { noremap = true, silent = true })
