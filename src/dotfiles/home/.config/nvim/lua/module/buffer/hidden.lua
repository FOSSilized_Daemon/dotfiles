-- The hidden.lua file contains all lua related to hidden buffer settings. When this file is loaded the below
-- lua will do the following:
--    1. Configure settings for hidden buffers.

local opt = vim.opt

-- Unload buffers when they are abandoned.
opt.hidden = false
