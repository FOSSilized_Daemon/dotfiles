-- The tab.lua file contains all lua related to tab format setting. When this file is loaded the below
-- lua will do the following:
--    1. Configure tab format settings.

local opt = vim.opt

-- Set tab spacing to four.
opt.shiftwidth  = 4
opt.softtabstop = 4
opt.tabstop     = 4

-- Do not expand tabs to spaces.
opt.expandtab = false
