-- The file.lua file contains all lua related to file format setting. When this file is loaded the below
-- lua will do the following:
--    1. Configure file format settings.

local opt = vim.opt

-- Support all needed file formats.
opt.fileformats = 'unix,dos,mac'
