-- The miscellaneous.lua file contains all lua related to miscellaneous automatic commands.
-- When this file is loaded the below lua will do the following:
--    1. Expose functions for automatic miscellanous.lua commands.

local cmd = vim.cmd

function save_cursor_position()
	-- Save the last cursor position.
	cmd([[ if @% !~# '\.git[\/\\]COMMIT_EDITMSG$' && line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif ]])
end
