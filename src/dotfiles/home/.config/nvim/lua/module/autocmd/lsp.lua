-- The lsp.lua file contains all lua related to file automatic commands. When this file is loaded the below
-- lua will do the following:
--    1. Expose functions for automatic lsp commands.

local cmd    = vim.cmd
local keymap = vim.api.nvim_buf_set_keymap
local fn     = vim.fn

function enable_lsp()
	-- Variable declarations.
	local lspconfig   = require 'lspconfig'
	local lsp_servers = {
		'angularls',
		'clangd',
		'pyright',
		'tsserver',
	}

	-- Show diagnostics messages when hovering over the referenced object.
	cmd([[ autocmd CursorHold,CursorHoldI * lua vim.diagnostic.open_float({focusable = false, show_header = false, border = 'single'}) ]])

	local on_attach = function(client, buffer_number)
		local keymap_opts = { noremap = true, silent = true }

		-- Set keybindings for better LSP navigation.
		keymap(buffer_number, 'n', 'gd',    '<Cmd>lua vim.lsp.buf.definition()<CR>',       keymap_opts)
		keymap(buffer_number, 'n', 'gD',    '<Cmd>lua vim.lsp.buf.declaration()<CR>',      keymap_opts)
		keymap(buffer_number, 'n', 'gr',    '<Cmd>lua vim.lsp.buf.references()<CR>',       keymap_opts)
		keymap(buffer_number, 'n', 'gi',    '<Cmd>lua vim.lsp.buf.implementation()<CR>',   keymap_opts)
		keymap(buffer_number, 'n', 'K',     '<Cmd>lua vim.lsp.buf.hover()<CR>',            keymap_opts)
		keymap(buffer_number, 'n', '<C-s>', '<Cmd>lua vim.lsp.buf.signature_help()<CR>',   keymap_opts)
		keymap(buffer_number, 'n', '<C-n>', '<Cmd>lua vim.lsp.diagnostic.goto_next()<CR>', keymap_opts)
		keymap(buffer_number, 'n', '<C-p>', '<Cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', keymap_opts)
	end

	-- Setup lsp_servers and attach them to the current buffer (with proper configuration options).
	for _,server in ipairs(lsp_servers) do
		lspconfig[server].setup {
			on_attach = on_attach,
			flags = {
				debounce_text_changes = 150,
			}
		}
	end
end
