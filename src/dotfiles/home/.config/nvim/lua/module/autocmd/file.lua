-- The file.lua file contains all lua related to file automatic commands. When this file is loaded the below
-- lua will do the following:
--    1. Expose functions for automatic file commands.

local fn = vim.fn

function remove_unneeded_files()
	-- Remove the 'HOME/.config/nvim/.netrwhist' file if it exists.
	local netrwhist_file = fn.expand('~/.config/nvim/.netrwhist')

	if fn.filereadable(netrwhist_file) == 0 then
		fn.delete(netrwhist_file)
	end
end
