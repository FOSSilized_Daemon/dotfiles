-- The load.lua file contains all lua related to loading plugins. When this file is loaded the below
-- lua will do the following:
--    1. Configure settings for loading plugins.

local env = vim.env
local opt = vim.opt
local cmd = vim.cmd

-- Add the 'XDG_CONFIG_HOME/.config/nvim/' directory to the plugin path.
local config_directory = env.XDG_CONFIG_HOME
opt.packpath:append(config_directory .. '/nvim/pack')

-- Load all plugins found in the plugin path.
cmd([[ packloadall ]])

-- Load filetype plugins.
cmd([[ filetype plugin on ]])
cmd([[ filetype indent on ]])

-- Enable file type detection.
cmd([[ filetype on ]])
