-- The disable.lua file contains all lua related to disabling plugins. When this file is loaded the below
-- lua will do the following:
--    1. Disable unwanted and unneeded plugins.

local plugin = vim.g

-- Disable unneeded built-in plugins.
local built_in_plugins = {
	'netrw',
	'netrwPlugin',
	'netrwSettings',
	'netrwFileHandlers',
	'gzip',
	'zip',
	'zipPlugin',
	'tar',
	'tarPlugin',
	'getscript',
	'getscriptPlugin',
	'vimball',
	'vimballPlugin',
	'2html_plugin',
	'logipat',
	'rrhelper',
	'spellfile_plugin',
	'matchit',
}

for _,built_in in pairs(built_in_plugins) do
	plugin['loaded_' .. built_in] = 1
end
