-- The number.lua file contains all lua related to visual number settings.
-- When this file is loaded the below lua will do the following:
--    1. Configure settings for visual number settings.

local opt = vim.opt

-- Show relative line numbers.
opt.number         = true
opt.relativenumber = true
