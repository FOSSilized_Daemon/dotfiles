-- The char.lua file contains all lua related to visual char settings.
-- When this file is loaded the below lua will do the following:
--    1. Configure settings for visual char settings.

local opt = vim.opt

-- Set the listchars.
opt.listchars = 'nbsp:¬,tab:»·,trail:·,extends:>'

-- Enable showing list characters.
opt.list = true

-- Display a space after end of buffer instead of a '~'.
opt.fillchars = 'eob: '
