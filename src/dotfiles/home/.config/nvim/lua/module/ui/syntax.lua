-- The syntax.lua file contains all lua related to syntax highlighting settings.
-- When this file is loaded the below lua will do the following:
--    1. Configure settings for syntax highlighting.

local opt = vim.opt
local cmd = vim.cmd

-- Enable syntax highlighting.
cmd([[ syntax on ]])

-- Enable true colors.
opt.termguicolors = true

-- Set the colorscheme.
cmd([[ colorscheme acid ]])
