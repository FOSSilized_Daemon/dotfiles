-- The column.lua file contains all lua related to visual column settings.
-- When this file is loaded the below lua will do the following:
--    1. Configure settings for visual column settings.

local opt = vim.opt

-- Highlight the current line and column.
opt.cursorline   = true
opt.cursorcolumn = true

-- Highlight the 120th line for reference.
opt.colorcolumn = '120'
