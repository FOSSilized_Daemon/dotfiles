-- The statusline.lua file contains all lua related to statusline settings.
-- When this file is loaded the below lua will do the following:
--    1. Configure settings for statusline settings.

local opt = vim.opt
local env = vim.env

-- Do not show the column number and cursor position.
opt.ruler = false

-- Only show one statusline.
opt.laststatus = 3

-- Set the statusline.
_G.statusline  = dofile(env.XDG_CONFIG_HOME .. '/nvim/pack/start/acid-statusline/statusline.lua')
opt.statusline = [[%!luaeval("statusline.status_line()")]]
