-- The undo.lua file contains all lua related to undo files.
-- When this file is loaded the below lua will do the following:
--    1. Configure settings for undo files.

local opt = vim.opt
local env = vim.env
local fn  = vim.fn

-- Enable undo files.
opt.undofile = true

-- Store undo files in 'HOME/.var/cache/nvim/undo-files/'.
local undo_files_directory = env.HOME .. '/.var/cache/nvim/undo-files/'

if fn.isdirectory(undo_files_directory) == 0 then
	fn.mkdir(undo_files_directory, 'p')
end

opt.undodir = undo_files_directory
