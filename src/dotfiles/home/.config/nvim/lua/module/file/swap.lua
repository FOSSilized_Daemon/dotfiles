-- The swap.lua file contains all lua related to swap files.
-- When this file is loaded the below lua will do the following:
--    1. Configure settings for swap files.

local opt = vim.opt
local env = vim.env
local fn  = vim.fn

-- Enable swap files.
opt.swapfile = true

-- Store swap files in 'HOME/.var/cache/nvim/swap-files'.
local swap_files_directory = env.HOME .. '/.var/cache/nvim/swap-files/'

if fn.isdirectory(swap_files_directory) == 0 then
	fn.mkdir(swap_files_directory, 'p')
end

opt.directory = swap_files_directory
