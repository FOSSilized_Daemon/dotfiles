-- The backup.lua file contains all lua related to backup files.
-- When this file is loaded the below lua will do the following:
--    1. Configure settings for backup files.

local opt = vim.opt
local env = vim.env
local fn  = vim.fn

-- Enable backup files.
opt.backup      = true
opt.writebackup = true

-- Append the '.bac' extension to backup files.
opt.backupext = '.bac'

-- Store backup files in 'HOME/.usr/share/nvim/backup-files/'.
local backup_files_directory = env.HOME .. '/.usr/share/nvim/backup-files/'

if fn.isdirectory(backup_files_directory) == 0 then
	fn.mkdir(backup_files_directory, 'p')
end

opt.backupdir = backup_files_directory
