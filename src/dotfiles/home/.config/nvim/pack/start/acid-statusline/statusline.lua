-- Variable declarations.
local api      = vim.api
local fn       = vim.fn
local bo       = vim.bo
local opt      = vim.opt
local Metadata = {}

Metadata.modes = setmetatable({
	['n']  = {'Normal',    'N'};
	['no'] = {'N·Pending', 'N·P'};
	['v']  = {'Visual',    'V' };
	['V']  = {'V·Line',    'V·L' };
	[''] = {'V·Block',   'V·B'};
	['s']  = {'Select',    'S'};
	['S']  = {'S·Line',    'S·L'};
	[''] = {'S·Block',   'S·B'};
	['i']  = {'Insert',    'I'};
	['ic'] = {'Insert',    'I'};
	['R']  = {'Replace',   'R'};
	['Rv'] = {'V·Replace', 'V·R'};
	['c']  = {'Command',   'C'};
	['cv'] = {'Vim·Ex ',   'V·E'};
	['ce'] = {'Ex ',       'E'};
	['r']  = {'Prompt ',   'P'};
	['rm'] = {'More ',     'M'};
	['r?'] = {'Confirm ',  'C'};
	['!']  = {'Shell ',    'S'};
	['t']  = {'Terminal ', 'T'};
}, {
	__index = function()
		-- Handle edge cases where the current mode is not available in the modes table.
		return {'Unknown', 'U'}
	end
})

Metadata.get_current_mode = function()
	-- Get vims' current mode.
	local current_mode = api.nvim_get_mode().mode
	return string.format('[%s]', Metadata.modes[current_mode][2]):upper()
end

Metadata.get_current_file_name = function()
	-- Get the name of the current file.
	return '%f'
end

Metadata.check_if_file_is_readonly = function()
	-- Check if the current file is readonly. If it is, then return the readonly symbol.
	if bo.readonly then
		return ' '
	else
		return ''
	end
end

Metadata.check_if_file_is_modified = function()
	-- Check if the file has been modified and if so return the modified symbol.
	if bo.modified then
		return ' ✘'
	else
		return ''
	end
end

Metadata.get_current_git_branch = function()
	-- Get the current git(1) branch.
	local current_branch = fn.system([[ git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n' ]])

	if current_branch ~= nil and current_branch:len() > 0 then
		return current_branch .. ' '
	else
		return ''
	end
end

Metadata.get_current_file_type = function()
	-- Get the file type of the current file.
	local file_type = opt.filetype:get()
	return string.format('%s', file_type):lower()
end

Metadata.get_current_file_format_and_encoding = function()
	return '[%{&fileformat} (%{&fileencoding?&fileencoding:&encoding})]'
end

Metadata.get_current_file_size = function()
	-- Get the size of the current file.
	local bytes = fn.getfsize(fn.expand('%:p'))
	local kilibytes
	local metabytes
	local gigibytes

	if bytes >= 1024 then
		kilibytes = bytes / 1024
	end

	if kilibytes and kilibytes >= 1024 then
		metabytes = kilibytes / 1024
	end

	if metabytes and metabytes >= 1024 then
		gigibytes = metabytes / 1024
	end

	if gigibytes then
		return gigibytes .. 'GB'
	elseif metabytes then
		return metabytes .. 'MB'
	elseif kilibytes then
		return kilibytes .. 'KB'
	else
		return bytes .. 'B'
	end
end

Metadata.get_current_percentage_of_file = function()
	-- Get the current percentage of the file.
	return '%p%%'
end

Metadata.status_line = function()
	-- Variable declarations.
	local statusline = ''

	statusline = statusline .. '%#CurrentMode#'
	statusline = statusline .. Metadata.get_current_mode()
	statusline = statusline .. '%#CurrentFileName#'
	statusline = statusline .. ' ' .. Metadata.get_current_file_name()
	statusline = statusline .. '%#CurrentFileReadOnly#'
	statusline = statusline .. Metadata.check_if_file_is_readonly()
	statusline = statusline .. '%#CurrentFileModified#'
	statusline = statusline .. Metadata.check_if_file_is_modified()
	statusline = statusline .. '%#CurrentGitBranch#'
	statusline = statusline .. ' ' .. Metadata.get_current_git_branch()

	statusline = statusline .. '%='
	statusline = statusline .. '%#CurrentFileType#'
	statusline = statusline .. ' ' .. Metadata.get_current_file_type()
	statusline = statusline .. '%#CurrentFileFormatEncoding#'
	statusline = statusline .. ' ' .. Metadata.get_current_file_format_and_encoding()
	statusline = statusline .. '%#CurrentFileSize#'
	statusline = statusline .. ' ' .. Metadata.get_current_file_size()
	statusline = statusline .. '%#CurrentFilePercentage#'
	statusline = statusline .. ' ' .. Metadata.get_current_percentage_of_file()

	return statusline
end

return Metadata
