-- The acid.lua file contains all lua related to implementing the acid theme for nvim(1). This file contains support
-- for both 256 and true colour codes. The below lua implements the following:
--    1. Support for all standard nvim(1) color categories.
--    2. Support for some common plugin color categories.

local cmd    = vim.api.nvim_command
local fn     = vim.fn
local plugin = vim.g
local acid   = {}

local colors = {
	black_256       = 0;
	blueviolet_256  = 57;
	chartreuse3_256 = 70;
	chartreuse4_256 = 76;
	cyan1_256       = 51;
	darkred_256     = 88;
	darkviolet_256  = 92;
	deep_pink1_256  = 199;
	deep_pink3_256  = 162;
	deep_pink4_256  = 89;
	gold3_256       = 142;
	greenyellow_256 = 154;
	grey11_256      = 234;
	grey42_256      = 242;
	magenta1_256    = 201;
	magenta3_256    = 127;
	purple_256      = 93;
	purple4_256     = 54;
	red1_256        = 196;
	yellow3_256     = 184;
	yellow4_256     = 100;

	black_hex       = "#000000";
	blueviolet_hex  = "#5f00ff";
	chartreuse3_hex = "#5faf00";
	chartreuse4_hex = "#5fd700";
	cyan1_hex       = "#00ffff";
	darkred_hex     = "#870000";
	darkviolet_hex  = "#8700d7";
	deep_pink1_hex  = "#ff00af";
	deep_pink3_hex  = "#d70087";
	deep_pink4_hex  = "#87005f";
	gold3_hex       = "#afaf00";
	greenyellow_hex = "#afff00";
	grey11_hex      = "#1c1c1c";
	grey42_hex      = "#6c6c6c";
	magenta1_hex    = "#ff00ff";
	magenta3_hex    = "#af00af";
	purple_hex      = "#8700ff";
	purple4_hex     = "#5f0087";
	red1_hex        = "#ff0000";
	yellow3_hex     = "#d7d700";
	yellow4_hex     = "#878700";
	none            = "NONE";
}

local symbols = {
	error       = "✘";
	warning     = "⚠";
	information = "ⓘ";
	hint        = "↪";
}

function acid.highlight(group, color)
	-- Apply syntax highlighting rules.
	local ctermfg = color.ctermfg and 'ctermfg=' .. color.ctermfg or 'ctermfg=NONE'
	local ctermbg = color.ctermbg and 'ctermbg=' .. color.ctermbg or 'ctermbg=NONE'
	local cterm   = color.cterm and 'cterm=' .. color.cterm or 'cterm=NONE'
	local ctermsp = color.ctermsp and 'ctermsp=' .. color.ctermsp or ''
	local guifg   = color.guifg and 'guifg=' .. color.guifg or 'guifg=NONE'
	local guibg   = color.guibg and 'guibg=' .. color.guibg or 'guibg=NONE'
	local gui     = color.gui and 'gui=' .. color.gui or 'gui=NONE'
	cmd('highlight ' .. group .. ' ' .. ctermfg .. ' ' .. ctermbg .. ' ' .. cterm .. ' ' .. ctermsp .. ' ' .. guifg .. ' ' .. guibg .. ' ' .. gui)
end

function acid.sign(group, symbol)
	-- Apply sign rules.
	fn.sign_define(group, { text = symbol.symbol, texthl = symbol.color })
end

function acid.load_syntax()
	-- Set syntax highlighting colors.
	local syntax = {
		-- General text.
		Normal      = { ctermfg=colors.cyan1_256,ctermbg=colors.black_256,guifg=colors.cyan1_hex,guibg=colors.black_hex };
		NormalNC    = { ctermfg=colors.deep_pink4_256,guifg=colors.deep_pink4_hex };
		String      = { ctermfg=colors.chartreuse4_256,cterm='italic',guifg=colors.chartreuse4_hex,gui='italic' };
		Underlined  = { ctermfg=colors.deep_pink4_256,cterm='italic,underline',guifg=colors.deep_pink4_hex,gui='italic,underline' };
		Substitute  = { ctermfg=colors.chartreuse3_256,cterm='italic',guifg=colors.chartreuse3_hex,gui='italic' };
		SpecialKey  = { ctermfg=colors.gold3_256,cterm='italic',guifg=colors.gold3_hex,gui='italic' };

		-- Numbers.
		Number = { ctermfg=colors.magenta1,guifg=colors.magenta1_hex };
		Float  = { ctermfg=colors.magenta3_256,guifg=colors.magenta1_hex };

		-- Whitespace.
		Whitespace  = { ctermfg=colors.chartreuse4_256,cterm='italic',guifg=colors.chartreuse4_hex,gui='italic' };

		-- Spell checking.
		SpellBad   = { ctermfg=colors.red1_256,cterm='undercurl',guifg=colors.red1_hex,gui='undercurl' };
		SpellCap   = { ctermfg=colors.red1_256,cterm='undercurl',guifg=colors.red1_hex,gui='undercurl' };
		SpellLocal = { ctermfg=colors.red1_256,cterm='undercurl',guifg=colors.red1_hex,gui='undercurl' };
		SpellRare  = { ctermfg=colors.red1_256,cterm='undercurl',guifg=colors.red1_hex,gui='undercurl' };

		-- Searching.
		Search    = { ctermfg=colors.deep_pink4_256,cterm='bold,underline',guifg=colors.deep_pink4_hex,gui='bold,underline' };
		IncSearch = { ctermfg=colors.magenta3_256,cterm='bold',guifg=colors.magenta1_hex,gui='bold' };

		-- Comments.
		Comment        = { ctermfg=colors.purple4_256,cterm='italic',guifg=colors.purple4_hex,gui='italic' };
		SpecialComment = { ctermfg=colors.darkviolet_256,cterm='italic',guifg=colors.darkred_hex,gui='italic' };

		-- Programming.
		Todo         = { ctermfg=colors.gold3_256,cterm='bold',guifg=colors.gold3_hex,gui='bold' };
		Debug        = { ctermfg=colors.darkred_256,cterm='bold,italic',guifg=colors.darkred_hex,gui='bold,italic' };
		Ignore       = { ctermfg=colors.none,ctermbg=colors.none };
		Typedef      = { ctermfg=colors.deep_pink3_256,cterm='italic',guifg=colors.deep_pink3_hex,gui='italic' };
		Include      = { ctermfg=colors.deep_pink4_256,cterm='italic',guifg=colors.deep_pink4_hex,gui='italic' };
		Identifier   = { ctermfg=colors.magenta3,cterm='italic',guifg=colors.magenta1_hex,gui='italic' };
		Conditional  = { ctermfg=colors.gold3_256,cterm='bold',guifg=colors.gold3_hex,gui='bold' };
		StorageClass = { ctermfg=colors.deep_pink3_256,cterm='bold',guifg=colors.deep_pink3_hex,gui='bold' };
		Special      = { ctermfg=colors.gold3_256,cterm='italic',guifg=colors.gold3_hex,gui='italic' };
		Label        = { ctermfg=colors.yellow4_256,cterm='italic',guifg=colors.yellow4_hex,gui='italic' };
		Delimiter    = { ctermfg=colors.deep_pink4_256,cterm='italic',guifg=colors.deep_pink4_hex,gui='italic' };
		Statement    = { ctermfg=colors.gold3_256,cterm='italic',guifg=colors.gold3_hex,gui='italic' };
		Character    = { ctermfg=colors.magenta1_256,cterm='italic',guifg=colors.magenta1_hex,gui='italic' };
		Boolean      = { ctermfg=colors.deep_pink4_256,cterm='bold',guifg=colors.deep_pink4_hex,gui='bold' };
		Define       = { ctermfg=colors.deep_pink4_256,cterm='bold',guifg=colors.deep_pink4_hex,gui='bold' };
		Function     = { ctermfg=colors.gold3_256,cterm='bold',guifg=colors.gold3_hex,gui='bold' };
		PreProc      = { ctermfg=colors.deep_pink4_256,cterm='italic',guifg=colors.deep_pink4_hex,gui='italic' };
		Exception    = { ctermfg=colors.yellow4_256,cterm='italic',guifg=colors.yellow4_hex,gui='italic' };
		Keyword      = { ctermfg=colors.yellow4_256,cterm='bold',guifg=colors.yellow4_hex,gui='bold' };
		Type         = { ctermfg=colors.deep_pink3_256,cterm='bold',guifg=colors.deep_pink3_hex,gui='bold' };
		Constant     = { ctermfg=colors.gold3_256,cterm='italic',guifg=colors.gold3_hex,gui='italic' };
		Tag          = { ctermfg=colors.gold3_256,cterm='italic',guifg=colors.gold3_hex,gui='italic' };
		Repeat       = { ctermfg=colors.gold3_256,cterm='bold',guifg=colors.gold3_hex,gui='bold' };
		Structure    = { ctermfg=colors.deep_pink4_256,cterm='bold',guifg=colors.deep_pink4_hex,gui='bold' };
		Macro        = { ctermfg=colors.deep_pink4_256,cterm='bold',guifg=colors.deep_pink4_hex,gui='bold' };
		Operator     = { ctermfg=colors.gold3_256,cterm='bold,italic',guifg=colors.gold3_hex,gui='bold,italic' };
		PreCondit    = { ctermfg=colors.deep_pink4_256,cterm='italic',guifg=colors.deep_pink4_hex,gui='italic' };

		-- LSP.
		LspDiagnosticsDefaultError           = { ctermfg=colors.red1_256,cterm='bold',guifg=colors.red1_hex,gui='bold' };
		LspDiagnosticsDefaultWarning         = { ctermfg=colors.yellow3_256,cterm='bold',guifg=colors.yellow3_hex,gui='bold' };
		LspDiagnosticsDefaultInformation     = { ctermfg=colors.chartreuse4_256,cterm='italic',guifg=colors.chartreuse4_hex,gui='italic' };
		LspDiagnosticsDefaultHint            = { ctermfg=colors.blueviolet_256,cterm='italic',guifg=colors.blueviolet_hex,gui='italic' };
		LspDiagnosticsFloatingError          = { ctermfg=colors.red1_256,cterm='bold',guifg=colors.red1_hex,gui='bold' };
		LspDiagnosticsFloatingWarning        = { ctermfg=colors.yellow3_256,cterm='bold',guifg=colors.yellow3_hex,gui='bold' };
		LspDiagnosticsFloatingInformation    = { ctermfg=colors.chartreuse4_256,cterm='italic',guifg=colors.chartreuse4_hex,gui='italic' };
		LspDiagnosticsFloatingHint           = { ctermfg=colors.blueviolet_256,cterm='italic',guifg=colors.blueviolet_hex,gui='italic' };
		LspDiagnosticsDefaultTextHint        = { ctermfg=colors.blueviolet_256,cterm='italic',guifg=colors.blueviolet_hex,gui='italic' };
		LspDiagnosticsVirtualTextError       = { ctermfg=colors.red1_256,cterm='bold',guifg=colors.red1_hex,gui='bold' };
		LspDiagnosticsVirtualTextWarning     = { ctermfg=colors.yellow3_256,cterm='bold',guifg=colors.yellow3_hex,gui='bold' };
		LspDiagnosticsVirtualTextInformation = { ctermfg=colors.chartreuse4_256,cterm='italic',guifg=colors.chartreuse4_hex,gui='italic' };
		LspDiagnosticsVirtualTextHint        = { ctermfg=colors.blueviolet_256,cterm='italic',guifg=colors.blueviolet_hex,gui='italic' };
		LspDiagnosticsUnderlineError         = { ctermfg=colors.red1_256,cterm='undercurl,bold',guifg=colors.red1_hex,gui='undercurl,bold' };
		LspDiagnosticsUnderlineWarning       = { ctermfg=colors.yellow3_256,cterm='undercurl,bold',guifg=colors.yellow3_hex,gui='undercurl,bold' };
		LspDiagnosticsUnderlineInformation   = { ctermfg=colors.chartreuse4_256,cterm='undercurl,italic',guifg=colors.chartreuse4_hex,gui='undercurl,italic' };
		LspDiagnosticsUnderlineHint          = { ctermfg=colors.blueviolet_256,cterm='undercurl,italic',guifg=colors.blueviolet_hex,gui='undercurl,italic' };

		-- Cursor.
		Cursor       = { ctermfg=colors.cyan1_256,cterm='bold',guifg=colors.cyan1_hex,gui='bold' };
		lCursor      = { ctermfg=colors.cyan1_256,cterm='bold',guifg=colors.cyan1_hex,gui='bold' };
		CursorIM     = { ctermfg=colors.cyan1_256,cterm='bold',guifg=colors.cyan1_hex,gui='bold' };
		iCursor      = { ctermfg=colors.black_256,cterm='bold',guifg=colors.black_hex,gui='bold' };
		TermCursor   = { ctermbg=colors.deep_pink1_256,guibg=colors.deep_pink1_hex };
		TermCursorNC = { ctermbg=colors.purple4_256,guibg=colors.purple4_hex };

		-- Fold.
		Folded     = { ctermfg=colors.purple4_256,cterm='italic',guifg=colors.purple4_hex,gui='italic' };
		FoldColumn = { ctermfg=colors.purple4_256,cterm='italic',guifg=colors.purple4_hex,gui='italic' };

		-- Line Numbers.
		LineNr       = { ctermfg=colors.gold3_256,guifg=colors.gold3_hex };
		CursorLineNr = { ctermfg=colors.greenyellow_256,cterm='bold',guifg=colors.greenyellow_hex,gui='bold' };
		ColorColumn  = { ctermbg=colors.grey11_256,guibg=colors.grey11_hex };

		-- Cursor Line.
		CursorColumn = { ctermbg=colors.grey11_256,guibg=colors.grey11_hex };
		CursorLine   = { ctermbg=colors.grey11_256,guibg=colors.grey11_hex };

		-- Error.
		Error      = { ctermfg=colors.red1_256,cterm='bold',guifg=colors.red1_hex,gui='bold' };
		ErrorMsg   = { ctermfg=colors.red1_256,cterm='bold',guifg=colors.red1_hex,gui='bold' };
		WarningMsg = { ctermfg=colors.red1_256,cterm='bold',guifg=colors.red1_hex,gui='bold' };

		-- Highlighting.
		VISUAL    = { ctermfg=colors.deep_pink1_256,cterm='bold',guifg=colors.deep_pink1_hex,gui='bold' };
		VisualNOS = { ctermfg=colors.deep_pink1_256,cterm='underline',guifg=colors.deep_pink1_hex,gui='underline' };

		-- Statusline.
		StatusLine   = { ctermfg=colors.purple_256,cterm='bold',guifg=colors.purple_hex,gui='bold' };
		StatusLineNC = { ctermfg=colors.purple_256,cterm='bold',guifg=colors.purple_2_hex,gui='bold' };

		-- Pop-up Menu.
		Pmenu      = { ctermfg=colors.deep_pink1_256,ctermbg=colors.black_256,cterm='bold',guifg=colors.deep_pink1_hex,guibg=colors.black_hex,gui='bold' };
		PmenuSel   = { ctermfg=colors.cyan1_256,ctermbg=colors.black_256,cterm='italic',guifg=colors.cyan1_hex,guibg=colors.black_hex,gui='italic' };
		PmenuSbar  = { ctermfg=colors.black_256,ctermbg=colors.black_256,guifg=colors.black_hex,guibg=colors.black_hex };
		PmenuThumb = { ctermfg=colors.black_256,ctermbg=colors.black_256,guifg=colors.black_hex,guibg=colors.black_hex };

		-- Diff.
		DiffAdd    = { ctermfg=colors.chartreuse4_256,cterm='italic',guifg=colors.chartreuse4_hex,gui='italic' };
		DiffChange = { ctermfg=colors.chartreuse3_256,cterm='italic',guifg=colors.chartreuse3_hex,gui='italic' };
		DiffDelete = { ctermfg=colors.red1_256,cterm='italic',guifg=colors.red1_hex,gui='italic' };
		DiffText   = { ctermfg=colors.chartreuse3_256,cterm='italic',guifg=colors.chartreuse3_hex,gui='italic' };

		-- Floating window.
		NormalFloat        = { cterm='italic' };
		FloatBorder        = { ctermfg=colors.cyan1_256,cterm='bold',guifg=colors.cyan1_hex,gui='bold' };
		FloatShadow        = { ctermfg=colors.none,ctermbg=colors.none };
		FloatShadowThrough = { ctermfg=colors.none,ctermbg=colors.none };

		-- Tabline.
		TabLine     = { ctermfg=colors.magenta1_256,cterm='italic',guifg=colors.magenta1_hex,gui='italic' };
		TabLineSel  = { ctermfg=colors.cyan1_256,cterm='bold,italic',guifg=colors.cyan1_hex,gui='bold,italic' };
		TabLineFill = { ctermbg=colors.none,guibg=colors.none };

		-- Miscellaneous.
		MatchParen   = { ctermfg=colors.purple4_256,cterm='underline',guifg=colors.purple4_hex,gui='underline' };
		Directory    = { ctermfg=colors.darkviolet_256,cterm='italic',guifg=colors.darkred_hex,gui='italic' };
		EndOfBuffer  = { ctermfg=colors.none,guifg=colors.none };
		VertSplit    = { ctermfg=colors.deep_pink4_256,ctermbg=colors.black_256,guifg=colors.deep_pink4_hex,guibg=colors.black_hex };
		SignColumn   = { ctermfg=colors.purple4_256,cterm='bold',guifg=colors.purple4_hex,gui='bold' };
		ModeMsg      = { ctermfg=colors.purple_256,cterm='italic',guifg=colors.purple_hex,gui='italic' };
		MsgArea      = { ctermfg=colors.cyan1_256,cterm='italic',guifg=colors.cyan1_hex,gui='italic' };
		MsgSeparator = { ctermfg=colors.purple4_256,guifg=colors.purple4_hex };
		MoreMsg      = { ctermfg=colors.cyan1_256,cterm='italic',guifg=colors.cyan1_hex,gui='italic' };
		NoneText     = { ctermfg=colors.gold3_256,cterm='bold',guifg=colors.gold3_hex,gui='bold' };
		Question     = { ctermfg=colors.cyan1_256,cterm='italic',guifg=colors.cyan1_hex,gui='italic' };
		QuickFixLine = { ctermfg=colors.chartreuse3_256,cterm='italic',guifg=colors.chartreuse3_hex,gui='italic' };
		Title        = { ctermfg=colors.darkviolet_256,cterm='italic',guifg=colors.darkred_hex,gui='italic' };
		WildMenu     = { ctermfg=colors.cyan1_256,ctermbg=colors.black_256,cterm='italic',guifg=colors.cyan1_hex,guibg=colors.black_hex,gui='italic' };
	}

	return syntax
end

function acid.load_plugin_syntax()
	-- Set syntax highlighting colors for plugins.
	local plugin_syntax = {
		-- Statusline
		CurrentMode               = { ctermfg=colors.magenta3_256,cterm='bold',guifg=colors.magenta3_hex,gui='bold' };
		CurrentFileName           = { ctermfg=colors.magenta3_256,cterm='italic',guifg=colors.magenta3_hex,gui='italic' };
		CurrentFileReadOnly       = { ctermfg=colors.darkviolet_256,cterm='italic',guifg=colors.darkviolet_hex,gui='italic' };
		CurrentFileModified       = { ctermfg=colors.darkviolet_256,cterm='italic',guifg=colors.darkviolet_hex,gui='italic' };
		CurrentGitBranch          = { ctermfg=colors.darkviolet_256,cterm='italic',guifg=colors.darkviolet_hex,gui='italic' };
		CurrentFileType           = { ctermfg=colors.magenta3_256,cterm='italic',guifg=colors.magenta3_hex,gui='italic' };
		CurrentFileFormatEncoding = { ctermfg=colors.darkviolet_256,cterm='italic',guifg=colors.darkviolet_hex,gui='italic' };
		CurrentFileSize           = { ctermfg=colors.magenta3_256,cterm='italic',guifg=colors.magenta3_hex,gui='italic' };
		CurrentFilePercentage     = { ctermfg=colors.magenta3_256,cterm='italic',guifg=colors.magenta3_hex,gui='italic' };

		-- Nvim Cmp
		CmpItemAbbr             = { ctermfg=colors.deep_pink1_256,ctermbg=colors.black_256,cterm='bold',guifg=colors.deep_pink1_hex,guibg=colors.black_hex,gui='bold' };
		CmpItemAbbrDeprecated   = { ctermfg=colors.red1_256,cterm='bold',guifg=colors.red1_hex,gui='bold' };
		CmpItemAbbrMatch        = { ctermfg=colors.cyan1_256,ctermbg=colors.black_256,cterm='bold',guifg=colors.cyan1_hex,guibg=colors.black_hex,gui='bold' };
		CmpItemAbbrMatchFuzzy   = { ctermfg=colors.darkviolet_256,ctermbg=colors.black_256,cterm='bold',guifg=colors.darkviolet_hex,guibg=colors.black_hex,gui='bold' };
		CmpItemKind             = { ctermfg=colors.chartreuse4_256,cterm='italic',guifg=colors.chartreuse4_hex,gui='italic' };
		CmpItemMenu             = { ctermfg=colors.cyan1_256,ctermbg=colors.black_256,cterm='bold',guifg=colors.cyan1_hex,guibg=colors.black_hex,gui='bold' };

		-- Twilight
		TwilightDim = { ctermfg=colors.grey42_256,cterm='italic',guifg=colors.grey42_hex,gui='italic' };
	}

	return plugin_syntax
end

function acid.load_signs()
	-- Set sign text and colors.
	local signs = {
		-- LSP.
		LspDiagnosticsSignError       = { symbol=symbols.error,color="LspDiagnosticsDefaultError" };
		LspDiagnosticsSignWarning     = { symbol=symbols.warning,color="LspDiagnosticsDefaultWarning" };
		LspDiagnosticsSignInformation = { symbol=symbols.information,color="LspDiagnosticsDefaultInformation" };
		LspDiagnosticsSignHint        = { symbol=symbols.hint,color="LspDiagnosticsDefaultHint" };
	}

	return signs
end

local async_load_plugin
async_load_plugin = vim.loop.new_async(vim.schedule_wrap(function ()
	-- Sync plugin syntax highlighting rules.
	local syntax = acid.load_plugin_syntax()

	for group,color in pairs(syntax) do
		acid.highlight(group,color)
	end

	async_load_plugin:close()
end))

function acid.colorscheme()
	-- Clear the current highlight rules.
	cmd([[ highlight clear ]])

	-- If syntax highlight is enabled, then reset it.
	if (fn.exists('syntax_on')) then
		cmd([[ syntax reset ]])
	end

	-- Set the colorscheme name to 'colors'.
	plugin.colors_name = 'acid'

	-- Load syntax highlighting rules.
	local syntax = acid.load_syntax()

	-- Apply syntax highlight rules.
	for group,color in pairs(syntax) do
		acid.highlight(group,color)
	end

	async_load_plugin:send()

	-- Load sign rules.
	local sign = acid.load_signs()

	-- Apply sign rules.
	for group,symbol in pairs(sign) do
		acid.sign(group,symbol)
	end
end

-- Export colorscheme.
acid.colorscheme()
return acid
