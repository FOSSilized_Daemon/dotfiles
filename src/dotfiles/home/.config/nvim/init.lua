-- The init.lua file traditionally contains all lua related to settings, automation, and the like.
-- However, in an effort to keep this codebase as clean and easy to navigate as possible each section of this file
-- has been broken up into modules which are only loaded by this file. Therefore, all this lua does is:
--    1. Load all files within the "XDG_CONFIG_HOME/nvim/lua/" directory.

-- Load all needed modules.
require('completion')
require('abbreviation')
require('file')
require('format')
require('buffer')
require('autocmd')
require('plugin')
require('key-binding')
require('lsp')
require('miscellaneous')
require('ui')
