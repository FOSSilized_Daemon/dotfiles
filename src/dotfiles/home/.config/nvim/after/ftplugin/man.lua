-- The man.lua file contains all lua related to configuring settings for man viewing.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for man viewing.

local opt_local = vim.opt_local
local cmd       = vim.cmd

-- Disable the statusline.
opt_local.laststatus = 0

-- Do not output any backspaces, printing only the last character written to each column position.
cmd([[ silent keepj keepp %s/\v(.)\b\ze\1?//ge ]])

-- Remove any ANSI escape sequences.
cmd([[ silent keepj keepp %s/\v\e\[%(%(\d;)?\d{1,2})?[mK]//ge ]])
