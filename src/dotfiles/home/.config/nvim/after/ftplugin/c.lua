-- The c.lua file contains all lua related to configuring settings for C programming. When this file is loaded, the
-- below lua will do the following:
--    1. Configure settings for C programming.

local opt_local = vim.opt_local

-- Set iskeyword so that snakecase variables as well as non-dynamic structures can be automatically completed.
opt_local.iskeyword:append({ '_', '.' })
