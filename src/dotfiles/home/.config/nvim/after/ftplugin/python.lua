-- The python.lua file contains all lua related to configuring settings for python programming.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for python programming.

local opt_local = vim.opt_local

-- Do not expand tabs to spaces.
opt_local.expandtab = false

-- Set iskeyword so that snakecase variable and functions as well as classes can be automatically completed.
opt_local.iskeyword:append({ '_', '.', '@' })
