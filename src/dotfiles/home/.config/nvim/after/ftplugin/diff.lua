-- The diff.lua file contains all lua related to configuring settings for diff viewing.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for diff viewing.

local opt_local = vim.opt_local

-- Disable the statusline.
opt_local.laststatus = 0
