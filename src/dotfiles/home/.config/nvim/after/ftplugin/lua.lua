-- The lua.lua file contains all lua related to configuring settings for lua programming.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for lua programming.

local opt_local = vim.opt_local

-- Set iskeyword so that snakecase variables can be automatically completed.
opt_local.iskeyword:append({ '_', '.', ':' })
