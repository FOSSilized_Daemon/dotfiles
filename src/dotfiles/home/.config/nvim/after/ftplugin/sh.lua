-- The sh.lua file contains all lua related to configuring settings for shell scripting.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for shell scripting.

local opt_local = vim.opt_local

-- Set iskeyword so that snakecase variables and functions can be automatically completed.
opt_local.iskeyword:append({ '_' })
