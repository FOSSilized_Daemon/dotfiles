-- The css.lua file contains all lua related to configuring settings for writing css.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for writing css.

local opt_local = vim.opt_local

-- Set tab spacing to three.
opt_local.shiftwidth  = 3
opt_local.softtabstop = 3
opt_local.tabstop     = 3

-- Expand tab characters to spaces.
opt_local.expandtab = true
