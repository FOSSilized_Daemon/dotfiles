-- The scheme.lua file contains all lua related to configuring settings for scheme programming.
-- When this file is loaded, the below lua will do the following:
--    1. Configuring settings for scheme programming.

local opt_local = vim.opt_local

-- Set tab spacing to two.
opt_local.shiftwidth  = 2
opt_local.softtabstop = 2
opt_local.tabstop     = 2

-- Expand tab characters to spaces.
opt_local.expandtab = true

-- Set iskeyword so that scheme variables, functions, etc. can be automatically completed.
opt_local.iskeyword:append({ '-' })
