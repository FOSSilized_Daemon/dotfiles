-- The markdown.lua file contains all lua related to configuring settings for writing markdown.
-- When this file is loaded, the below lua will do the following:
--    1. Configure settings for writing markdown.

local opt_local = vim.opt_local
local keymap    = vim.api.nvim_set_keymap
-- local cmd       = vim.cmd

-- Set the fold method to 'manual'.
opt_local.foldmethod = 'manual'

-- Do not automatically indent newlines.
opt_local.autoindent  = false
opt_local.smartindent = false

-- Set the maximum width of text that is being inserted to 80.
opt_local.textwidth = 80

-- Enable line wrapping.
opt_local.wrap = true

-- Enable spell checking for United States English.
opt_local.spelllang = 'en_us'
opt_local.spell     = true

-- Do not show listchars.
opt_local.list = false

-- Remap 'j' and 'k' to move line by line.
keymap('n', 'j', 'gj', { noremap = true, silent = true })
keymap('n', 'k', 'gk', { noremap = true, silent = true })

-- Enter into writer mode.
-- cmd([[ TZAtaraxis ]])
