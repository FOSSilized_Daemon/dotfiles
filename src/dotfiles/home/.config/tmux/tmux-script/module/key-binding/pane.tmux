#### The pane.tmux file contains all tmux script related to pane key-bindings. When this file is sourced
#### the below tmux script will do the following:
####    1. Set pane key-bindings.

## Split the current window horizontally with the current working directory.
bind - split-window -v -c "#{pane_current_path}"

## Split the current window vertically with the current working directory.
bind _ split-window -h -c "#{pane_current_path}"

## Navigate panes.
bind -r h select-pane -L
bind -r j select-pane -D
bind -r k select-pane -U
bind -r l select-pane -R

## Resize panes.
bind -r H     resize-pane -L 1
bind -r J     resize-pane -D 1
bind -r K     resize-pane -U 1
bind -r L     resize-pane -R 1
bind -r Up    resize-pane -U 1
bind -r Down  resize-pane -D 1
bind -r Left  resize-pane -L 1
bind -r Right resize-pane -R 1

## Move the current pane.
bind > swap-pane -D
bind < swap-pane -U
