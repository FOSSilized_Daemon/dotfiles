#### The session.tmux file contains all tmux script related to session key-bindings. When this file is sourced
#### the below tmux script will do the following:
####    1. Set session key-bindings.

## Create a new session.
bind C-c new-session

## Select an existing session.
bind C-f command-prompt -p find-session 'switch-client -t %%'
