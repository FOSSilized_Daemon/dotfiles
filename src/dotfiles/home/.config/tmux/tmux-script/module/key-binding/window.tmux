#### The window.tmux file contains all tmux script related to window key-bindings. When this file is sourced
#### the below tmux script will do the following:
####    1. Set window key-bindings.

## Create a new window with the current working directory.
unbind C
bind C new-window -c "#{pane_current_path}"

## Move the current window to master.
bind Enter move-window -t 0

## Navigate windows.
unbind n
unbind p
bind Tab last-window
bind -r C-h previous-window
bind -r C-l next-window
