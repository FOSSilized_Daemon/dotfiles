#### The miscellaneous.tmux file contains all tmux script related to miscellaneous key-bindings.
#### When this file is sourced the below tmux script will do the following:
####    1. Set miscellaneous key-bindings.

## Source "HOME/.tmux.conf".
bind F5 source-file "${HOME}"/.tmux.conf \; display 'sourced ~/.tmux.conf'
