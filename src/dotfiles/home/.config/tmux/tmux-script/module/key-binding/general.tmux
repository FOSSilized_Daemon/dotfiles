#### The general.tmux file contains all tmux script related to general key-bindings. When this file is sourced
#### the below tmux script will do the following:
####    1. Set general key-bindings.

## Enable mouse support.
setw -g mouse on

## Fix escape delays.
set-option -sg escape-time 10

## Allow key bindings to be repeated.
set-option -g repeat-time 500

## Set 'c-a' as the prefix.
set -g prefix2 C-a
bind C-a send-prefix -2

## Set 'c-n' as the prefix for nested sessions.
bind-key -n C-n send-prefix
