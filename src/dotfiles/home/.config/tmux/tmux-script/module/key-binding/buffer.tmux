#### The buffer.tmux file contains all tmux script related to buffer key-bindings. When this file is sourced
#### the below tmux script will do the following:
####    1. Set buffer key-bindings.

## List buffers.
unbind b
bind b list-buffers

## List existing buffers and allow the user to select one.
unbind P
bind P choose-buffer
