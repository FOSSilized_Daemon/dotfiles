#### The copy.tmux file contains all tmux script related to choice key-bindings.
#### When this file is sourced the below tmux script will do the following:
####    1. Set copy key-bindings based on "XDG_SESSION_TYPE".

## Enter copy mode.
bind v copy-mode -H

## Paste the contents of the tmux buffer.
bind p paste-buffer

## Enter buffer pasting mode.
bind-key b switch-client -T buffers

if-shell -b 'echo "${XDG_SESSION_TYPE}" | grep -q "x11"' {
	bind -Tcopy-mode-vi v send -X begin-selection
	bind -Tcopy-mode-vi C-v send -X rectangle-toggle
	bind -Tcopy-mode-vi y send -X copy-pipe-and-cancel "xclip -selection clipboard -i"
	bind-key P run "xclip -out -selection clipboard | tmux load-buffer -; tmux paste-buffer"
	bind -Tcopy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "xclip -in -selection clipboard"
	bind -Tcopy-mode-vi Escape send -X cancel
	bind -Tcopy-mode-vi H send -X start-of-line
	bind -Tcopy-mode-vi L send -X end-of-line
	}

if-shell -b 'echo "${XDG_SESSION_TYPE}" | grep -q "wayland"' {
	bind -Tcopy-mode-vi v send -X begin-selection
	bind -Tcopy-mode-vi C-v send -X rectangle-toggle
	bind -Tcopy-mode-vi y send -X copy-pipe-and-cancel "wl-copy"
	bind-key P run "wl-paste | tmux load-buffer -; tmux paste-buffer"
	bind -Tcopy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "wl-copy"
	bind -Tcopy-mode-vi Escape send -X cancel
	bind -Tcopy-mode-vi H send -X start-of-line
	bind -Tcopy-mode-vi L send -X end-of-line
}
