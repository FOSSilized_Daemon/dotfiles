#### The plugin.tmux file contains all tmux script related to configuraing and loading plugins.
#### When this file is sourced the below tmux script will do the following:
####    1. Configure plugins.
####    2. Load plugins.

######################
### Tmux Resurrect ###
######################
## Store the tmux environment file in 'XDG_CACHE_HOME/tmux/plugin/tmux-resurrect'.
set -g @resurrect-dir '~/.var/cache/tmux/plugin/tmux-resurrect'

## Restore all user defined processes.
set -g @ressurect-processes ':all:'

## Save pane contents.
set -g @resurrect-capture-pane-contents 'on'

## Capture the entire pane contents.
set -g @resurrect-pane-contents-area 'full'

## Load the tmux-resurrect plugin.
run-shell "bash ${XDG_CONFIG_HOME}/tmux/plugin/tmux-resurrect/resurrect.tmux"

#####################
### Tmux Continum ###
#####################
## Enable automatic session restoring.
set -g @continuum-restore 'on'

## Update the saved session every five minuets.
set -g @continuum-save-interval '5'

## Load the tmux-continuum plugin.
run-shell "bash ${XDG_CONFIG_HOME}/tmux/plugin/tmux-continuum/continuum.tmux"
