#### The key-binding.tmux file contains all tmux script related to key-bindings. When this file is sourced
#### the below tmux script will do the following:
####    1. Source all modules in the "XDG_CONFIG_HOME/tmux/tmux-script/module/key-binding/" directory.
source "${XDG_CONFIG_HOME}"/tmux/tmux-script/module/key-binding/buffer.tmux
source "${XDG_CONFIG_HOME}"/tmux/tmux-script/module/key-binding/copy.tmux
source "${XDG_CONFIG_HOME}"/tmux/tmux-script/module/key-binding/general.tmux
source "${XDG_CONFIG_HOME}"/tmux/tmux-script/module/key-binding/miscellaneous.tmux
source "${XDG_CONFIG_HOME}"/tmux/tmux-script/module/key-binding/pane.tmux
source "${XDG_CONFIG_HOME}"/tmux/tmux-script/module/key-binding/session.tmux
source "${XDG_CONFIG_HOME}"/tmux/tmux-script/module/key-binding/window.tmux
