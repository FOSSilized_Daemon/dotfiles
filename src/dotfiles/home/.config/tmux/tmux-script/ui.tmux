#### The ui.tmux file contains all tmux script related to configuring the user interface.
#### When this file is sourced the below tmux script will do the following:
####    1. Configure the user interface.

## Support 256 colors.
set -gs default-terminal "tmux-256color"

## Enable undercurl support.
set -as terminal-overrides ',*:Smulx=\E[4::%p1%dm'

## Show the status bar at the bottom.
set-option -g status-position bottom

## Show the window names on the centre of the status bar.
set-option -g status-justify centre

## Update the status bar every second.
set-option -g status-interval 1

## Set status character length.
set-option -g status-left-length 50
set-option -g status-right-length 60

## Set the colors for pane splits..
set -g pane-border-style 'fg=colour199 bold'
set -g pane-active-border-style 'fg=colour51 bold'

## Set the foreground color of the status bar.
set-option -g status-style 'fg=colour51,bold'

## Show the current tmux(1) session name on the left side of
## the status bar.
set-option -g status-left '[#[fg=colour51]#{session_name}] '

## Show the current window as 'int:name(position)*'
setw -g window-status-current-format '#[fg=colour51]#I:#[fg=colour51]#W#{?pane_at_top,(⇱),(⇲)}#F'

## Show the last selected window as 'int:name-'
setw -g window-status-format '#[fg=colour199,bold]#I:#[fg=colour199]#W#F'

## Set the right side of the status bar/
set-option -g status-right '#("${TMUX_STATUS_DIRECTORY}"/network-status)#[fg=colour51]\
							 | #("${TMUX_STATUS_DIRECTORY}"/battery-status)\
							 | #[fg=colour93]#("${TMUX_STATUS_DIRECTORY}"/date-status)#[fg=colour51]\
							 | #[fg=colour93]#("${TMUX_STATUS_DIRECTORY}"/time-status)'
