#### The miscellanous.tmux file contains all tmux script related to miscellanous settings. When this file is sourced
#### the below tmux script will do the following:
####    1. Set miscellanous settings.

## Run a non-login shell when opening tmux.
set-option -g default-command "${SHELL}"

## Start the index for windows and panes at 1.
set -g base-index 1
set -g pane-base-index 1

## Renumber windows when one is destroyed.
set-option -g renumber-windows on

## Do not automatically rename windows.
set-option -g automatic-rename off

## Set the history size.
set -g history-limit 10000

## Enable focus events.
set -g focus-events on
