startpage

social media
-- general
	- reddit [https://old.reddit.com/]
	- twitter [https://twitter.com/]
	- tiktok [https://www.tiktok.com/]
	- instagram [https://www.instagram.com/]
	- snapchat [https://www.snapchat.com/]
	- tinder [https://tinder.com/]
	- matrix [https://matrix.org/]
	- discord [https://discord.com/]
	- skype [https://wwww.skype.com/en/]
	- zoom [https://zoom.us/]
-- e-mail
	- gmail [https://www.google.com/gmail/about/]
	- protonmail [https://protonmail.com/]

media
-- general
	- invidious [https://vid.puffyan.us/feed/popular]
	- hulu [https://www.hulu.com/welcome]
	- netflix [https://www.netflix.com/]
	- hbo max [https://www.hbomax.com/]
-- streams
	- the vaush pit [https://www.vaush.gg/]
	- denims [https://www.denims.tv/]
	- conure.cc [https//www.conure.cc/]
	- twitch [https://www.twitch.tv/]

development
-- git repositories
	- fossilized daemon (gitlab) [https://gitlab.com/FOSSilized_Daemon]
	- fossilizeddaemon (github) [https://github.com/FOSSilizedDaemon]
-- discourse
	- neovim discourse [https://neovim.discourse.group/]
-- practice
	- codecademy [https://www.codecademy.com/]
	- linux journey [https://linuxjourney.com/]
-- handbooks
	- linux from scratch [https://www.linuxfromscratch.org/]
-- wikis
	- gentoo wiki [https://wiki.gentoo.org/wiki/Main_Page]
	- arch wiki [https://wiki.archlinux.org/]

video games
-- general
	- steam [https://store.steampowered.com/]
	- epic games [https://www.epicgames.com/store/en-US]
-- drm free
	- gog [https://www.gog.com/]
	- itch.io [https://itch.io/]
	- indiegala [https://www.indiegala.com/]
