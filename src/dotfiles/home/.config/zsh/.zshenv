#### The .zshenv file contains all shell script related to setting environment variables. When zsh(1) executes this
#### shell script it will:
####    1. Set the value of XDG_CONFIG_HOME and then ZDOTDIR so that every module can use said environment variables.
####    2. Load all modules that set environment variables.

create_directory() {
	if ! [ -d "${*}" ]; then
		mkdir -p "${*}"
	fi
}

## Use the "HOME/.config/" directory to store configuration files.
configuration_files_directory="${HOME}"/.config
create_directory "${configuration_files_directory}"
export XDG_CONFIG_HOME="${configuration_files_directory}"

## Use the "XDG_CONFIG_HOME/zsh/" directory to store zsh(1) configuration
## files.
zsh_configuration_directory="${XDG_CONFIG_HOME}"/zsh
create_directory "${zsh_configuration_directory}"
export ZDOTDIR="${zsh_configuration_directory}"

## Load all needed modules.
for module in "${ZDOTDIR}"/zsh-script/module/environment-variable/*
do
	. "${module}"
done
