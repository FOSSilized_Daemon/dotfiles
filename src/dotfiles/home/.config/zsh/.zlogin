#### The .zlogin file contains all shell script related to setting up the environment of a login shell.
#### If zsh(1) is running as a login shell the below script will:
####    1. Prompt the user for whether or not a graphical session should be launched.
####    2. Start all user services.

start_environment() {
	## Prompt the user asking whether or not a graphical session should be started. If the user inputs either 'y',
	## 'Y', 'yes', YES, or 'Yes', then run start(1) if it is installed. Otherwise, attempt to run MULTIPLEXOR if it is
	## installed. If neither of the above is possible, then do nothing.
	start_gui=0

	printf "Start graphical user interface? (y/n/N): "
	read -r user_answer

	case "${user_answer}" in
		y|Y|yes|YES|Yes)
			start_gui=1;;
		*)
			return;;
	esac

	if [ "${start_gui}" -eq 1 ]; then
		if command -v startx > /dev/null 2>&1; then
			startx
		fi
	else
		if command -v "${MULTIPLEXOR}" > /dev/null 2>&1; then
			"${MULTIPLEXOR}"
		fi
	fi
}

start_user_services() {
	## Start mpd(1) in the background.
	if command -v mpd > /dev/null 2>&1; then
		mpd &
	fi
}

main() {
	start_environment
	start_user_services
}

main
