#### The plugin.zsh file contains all shell script related to loading and configuring plugins. When this
#### file is sourced the below shell script will do the following:
####    1. Load all plugin configurations from "ZDOTDIR/zsh-script/module/plugin".

. "${ZDOTDIR}"/zsh-script/module/plugin/zsh-history-substring-search/zsh-history-substring-search.zsh
. "${ZDOTDIR}"/zsh-script/module/plugin/zsh-autosuggestions/zsh-autosuggestions.zsh
. "${ZDOTDIR}"/zsh-script/module/plugin/fast-syntax-highlighting/fast-syntax-highlighting.zsh
. "${ZDOTDIR}"/zsh-script/module/plugin/zsh-autopair/zsh-autopair.zsh
. "${ZDOTDIR}"/zsh-script/module/plugin/zsh-manydots-magic/zsh-manydots-magic.zsh
. "${ZDOTDIR}"/zsh-script/module/plugin/fzf-zsh/fzf-zsh.zsh
