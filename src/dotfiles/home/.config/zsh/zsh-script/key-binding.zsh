#### The key-binding.zsh file contains all shell script related to key-bindings. When this file is sourced
#### the below shell script will do the following:
####    1. Load all needed zsh(1) stock modules.
####    2. Set zsh(1) key-binding settings.

## Load needed functions.
autoload -Uz edit-command-line

## Wait 10 milliseconds for additional characters in
## key sequences.
export KEYTIMEOUT=1

## Enable Emacs navigation when in 'insert' mode and
## Vi navigation when in 'normal' mode.
bindkey -e
bindkey -e '\e' vi-cmd-mode

## Ensure backspace always deletes the last character.
bindkey "^?" backward-delete-char

## Ensure delete always deletes the next character.
bindkey '\e[3~' delete-char

## Tab complete words.
bindkey '^i' complete-word

## Accept the current suggestion.
bindkey '^k' autosuggest-accept

## Edit the current line in $EDITOR.
bindkey '^x^e' edit-command-line

## Add edit-command-line to the line editor.
zle -N edit-command-line

## Search for the current string in history.
bindkey -M emacs '^p' history-substring-search-up
bindkey -M emacs '^n' history-substring-search-down
bindkey -M vicmd '/'  history-substring-search-up
bindkey -M vicmd '?'  history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down

## Use Vi navigation in the completion menu.
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
