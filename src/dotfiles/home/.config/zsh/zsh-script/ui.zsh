#### The ui.zsh file contains all shell script related to user interface. When this file is sourced
#### the below shell script will do the following:
####    1. Load all modules from "ZDOTDIR/zsh-script/module/ui/".

## Load all needed modules.
for module in "${ZDOTDIR}"/zsh-script/module/ui/*
do
	if [ -d "${module}" ]; then
		for module in "${module}"/*
		do
			. "${module}"
		done
	else
		. "${module}"
	fi
done
