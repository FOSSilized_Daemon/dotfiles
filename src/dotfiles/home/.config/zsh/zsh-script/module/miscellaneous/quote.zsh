#### The qoute.zsh file contains all shell script related to printing qoutes. When this file is sourced
#### the below shell script will do the following:
####    1. Expose functions for printing qoutes.

print_quote() {
	## Print a quote colorized as pink.
	current_quote=$(sort -uR "${ZSH_QUOTES_FILE}" | head -n 1)
	print -Pr "%F{199}${current_quote}%f"
}
