#### The insult.zsh file contains all shell script related to printing insults. When this file is sourced
#### the below shell script will do the following:
####    1. Expose functions for printing insults.

print_insult() {
	## Print an insult colorized as red.
	current_insult=$(sort -uR "${ZSH_INSULTS_FILE}" | head -n 1)
	current_insult="%F{196}${current_insult}%f"
	print -Pr "${current_insult}"
}
