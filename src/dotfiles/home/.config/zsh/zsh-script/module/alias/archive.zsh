#### The archive.zsh file contains all shell script related to archive functions used for aliases. When this file is
#### sourced the below shell script will do the following:
####    1. Expose wrapper functions for common archiving tasks.

create_cpio_archive() {
	for target_file in "${@}"
	do
		if [ -d "${target_file}" ]; then
			find -L -O3 "${target_file}" | cpio -ovVL --device-independent --absolute-filenames > "${target_file}".cpio
		else
			printf "%s" "${target_file}" | cpio -ovVL --device-independent --absolute-filenames > "${target_file}".cpio
		fi
	done
}

extract_cpio_archive() {
	for target_file in "${@}"
	do
		cpio -idvV < "${target_file}".cpio
	done
}

create_tar_archive() {
	for target_file in "${@}"
	do
		tar cWahPlRfv "${target_file}".tar --sort=name --format=posix --acls --selinux --xattrs --hard-dereference --exclude-caches-all --exclude-vcs --exclude-vcs-ignores --checkpoint=5 --utc "${target_file}"
	done
}

extract_tar_archive() {
	for target_file in "${@}"
	do
		tar xkpsfv "${target_file}" --acls --selinux --xattrs --atime-preserve=replace --delay-directory-restore --utc
	done
}

list_tar_archive() {
	for target_file in "${@}"
	do
		tar tf "${target_file}" --acls --selinux --xattrs --utc
	done
}

create_gzip_archive() {
	for target_file in "${@}"
	do
		if [ -d "${target_file}" ]; then
			create_tar_archive "${target_file}"
			gzip -ckNv < "${target_file}".tar > "${target_file}".tar.gz
			rm -f "${target_file}".tar
		else
			gzip -ckNv < "${target_file}" > "${target_file}".gz
		fi
	done
}

extract_gzip_archive() {
	for target_file in "${@}"
	do
		gzip -cdkNv < "${target_file}" > $(printf "%s" "${target_file}" | sed 's/\.gz$//')
		rm -f "${target_file}"
	done
}


create_zip_archive() {
	temporary_directory=$(mktemp -d /tmp/zip.XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX)

	for target_file in "${@}"
	do
		zip -vr6b "${temporary_directory}" "${target_file}".zip "${target_file}"
	done

	rm -rf "${temporary_directory}"
}

extract_zip_archive() {
	for target_file in "${@}"
	do
		unzip -VX "${target_file}"
	done
}

list_zip_archive() {
	for target_file in "${@}"
	do
		unzip -lVXv "${target_file}"
	done
}

create_xz_archive() {
	for target_file in "${@}"
	do
		if [ -d "${target_file}" ]; then
			create_tar_archive "${target_file}"
			xz -zk6v "${target_file}".tar --format=auto --check=sha256
			rm -f "${target_file}".tar
		else
			xz -zk6v "${target_file}" --format=auto --check=sha256
		fi
	done
}

extract_zip_archive() {
	for target_file in "${@}"
	do
		xz -vX "${target_file}"
	done
}

list_xz_archive() {
	for target_file in "${@}"
	do
		xz -lv "${target_file}"
	done
}
