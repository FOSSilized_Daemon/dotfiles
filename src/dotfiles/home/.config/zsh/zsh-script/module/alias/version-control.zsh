#### The version-control.zsh file contains all shell script related to version control functions used for aliases.
#### When this file is sourced the below shell script will do the following:
####    1. Expose wrapper functions for common version control tasks.

git_pull() {
		if [ -z "${1}" ]; then
				git pull --verbose
		else
				git pull "${@}" --verbose
		fi
}

git_push() {
	git push --verbose
}

git_add() {
	if [ -z "${1}" ]; then
			git add . --verbose
	else
			git add "${@}" --verbose
	fi
}

git_remove() {
	git rm -rf "${@}"
}

git_reset() {
	current_branch=$(git branch | grep "^\*" | sed 's/*//;s/ //')

	if [ -n "${1}" ]; then
			git checkout "${current_branch}" "${@}"
	else
			git reset --hard
	fi
}

git_checkout() {
	if [ -z "${1}" ]; then
			git checkout master
	else
			git checkout "${@}"
	fi
}

git_branch() {
	if [ -z "${1}" ]; then
			git branch --verbose
	elif [ "${1}" = "-d" ] || [ "${1}" = "-D" ]; then
			shift 1
			git branch -d "${@}" --verbose
	elif [ "${1}" = "-m" ] || [ "${1}" = "-M" ]; then
			shift 1
			git branch -m "${@}" --verbose
	else
			git checkout -b "${@}"
	fi
}

git_clone() {
	if [ -z "${2}" ]; then
			git clone "${@}" --verbose
	elif [ "${1}" = "branch" ]; then
			shift 1
			git clone --branch "${@}"
	elif [ "${1}" = "submodules" ]; then
			shift 1
			git clone --recurse-submodules -j8 "${@}"
	fi
}

git_ls() {
	if [ -z "${1}" ]; then
			git ls-files --stage
	else
			git ls-files --stage | grep "${@}"
	fi
}

git_submodule() {
	if [ -z "${1}" ]; then
			git submodule status --recursive | awk '{print $2}'
	elif [ "${1}" = "add" ]; then
			shift 1
			git submodule add "${@}"
	elif [ "${1}" = "update" ]; then
			git submodule update --remote
	elif [ "${*}" = "update -i" ]; then
			git submodule update --init --recursive
	elif [ "${1}" = "summary" ]; then
			if [ -z "${2}" ]; then
					git submodule summary
			else
					shift 1
					git submodule summary "${@}"
			fi
	fi
}

git_diff() {
	if [ -z "${2}" ]; then
			git diff --color HEAD "${1}"
	else
			git diff --color "${@}"
	fi
}

git_ignore() {
	git_ignore_file=$(printf "%s/.gitignore" $(git rev-parse --show-toplevel))

	if [ "${1}" = "ignore" ]; then
		shift 1

		for file in "${@}"
		do
			target_file=$(printf "%s%s" $(git rev-parse --show-prefix) ${file})
			if grep -Fxq "${target_file}" "${git_ignore_file}"; then
				printf "\"%s\" is already ignored...\n" "${target_file}" >&2
			else
				printf "%s\n" "${target_file}" >> "${git_ignore_file}"
				printf "Ignoring \"%s\"...\n" "${target_file}"
			fi
		done

	elif [ "${1}" = "unignore" ]; then
		shift 1

		for file in "${@}"
		do
			target_file=$(printf "%s%s" $(git rev-parse --show-prefix) ${file})
			if ! grep -Fxq "${target_file}" "${git_ignore_file}"; then
				printf "\"%s\" is not ignored...\n" "${target_file}" >&2
			else
				ed "${git_ignore_file}" <<- EOF
				,g/${target_file}/d
				w
				q
				EOF
			fi
		done

	elif [ "${1}" = "ls" ]; then
		printf "Ignored files:\n"
		grep -v '^#' "${git_ignore_file}"
	fi
}
