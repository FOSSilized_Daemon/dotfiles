#### The directory.zsh file contains all shell script related to directory functions used for aliases.
#### When this file is sourced the below shell script will do the following:
####    1. Expose wrapper functions for common bookmark directory tasks.

bookmark_directory() {
	## Save the name of the bookmarked parent directory. Then shift so that any sub-directories past on the
	## command-line can be used.
	bookmark_directory="${1}"
	shift 1

	if [ -z "${*}" ]; then
		## If the bookmarked parent directory does not exist, then create it and cd(1) into it.
		if ! [ -d "${bookmark_directory}" ]; then
			mkdir -p "${bookmark_directory}"
		fi
		cd "${bookmark_directory}" || return 1
	else
		## If the passed sub directory does not exist, then create it and cd(1) into it.
		if ! [ -d "${bookmark_directory}"/"${*}" ]; then
			mkdir -p "${bookmark_directory}"/"${*}"
		fi
		cd "${bookmark_directory}"/"${*}" || return 1
	fi
}

cybermedia_parent() {
	bookmark_directory "${HOME}"/cybermedia "${@}"
}

music_parent() {
	bookmark_directory "${HOME}"/cybermedia/audio/music "${@}"
}

screenshot_parent() {
	bookmark_directory "${HOME}"/cybermedia/image/screenshot "${@}"
}

wallpaper_parent() {
	bookmark_directory "${HOME}"/cybermedia/image/wallpaper "${@}"
}

video_parent() {
	bookmark_directory "${HOME}"/cybermedia/video "${@}"
}

television_show_parent() {
	bookmark_directory "${HOME}"/cybermedia/video/television-show "${@}"
}

movie_parent() {
	bookmark_directory "${HOME}"/cybermedia/video/movie "${@}"
}

document_parent() {
	bookmark_directory "${HOME}"/document "${@}"
}

development_parent() {
	bookmark_directory "${HOME}"/document/development "${@}"
}

core_project_parent() {
	bookmark_directory "${HOME}"/document/development/core-project "${@}"
}

core_project_documentation_parent() {
	bookmark_directory "${HOME}"/document/development/core-project/documentation "${@}"
}

core_project_library_parent() {
	bookmark_directory "${HOME}"/document/development/core-project/library "${@}"
}

core_project_program_parent() {
	bookmark_directory "${HOME}"/document/development/core-project/program "${@}"
}

development_miscellaneous_parent() {
	bookmark_directory "${HOME}"/document/development/miscellaneous "${@}"
}

media_parent() {
	bookmark_directory "${HOME}"/media "${@}"
}

download_parent() {
	bookmark_directory "${HOME}"/transfer/download "${@}"
}

upload_parent() {
	bookmark_directory "${HOME}"/transfer/upload "${@}"
}

config_parent() {
	bookmark_directory "${HOME}"/.config "${@}"
}

library_parent() {
	bookmark_directory "${HOME}"/.library "${@}"
}

program_parent() {
	bookmark_directory "${HOME}"/.program "${@}"
}

usr_parent() {
	bookmark_directory "${HOME}"/.usr "${@}"
}

var_parent() {
	bookmark_directory "${HOME}"/.var "${@}"
}
