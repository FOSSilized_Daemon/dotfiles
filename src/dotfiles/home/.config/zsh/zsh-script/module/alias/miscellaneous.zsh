#### The miscellaneous.zsh file contains all shell script related to miscellaneous functions used for aliases.
#### When this file is sourced the below shell script will do the following:
####    1. Expose wrapper functions for common miscellaneous tasks.

view_rss() {
	if [ "${1}" = "new" ]; then
		sfeed_plain "${XDG_CACHE_HOME}"/sfeed/feeds/* | grep '^N' | eval "${PAGER}"
	else
		sfeed_plain "${XDG_CACHE_HOME}"/sfeed/feeds/* | eval "${PAGER}"
	fi
}

_cheat() {
	if [ -z "${1}" ]; then
			curl -fLm 7 "https://cheat.sh"
	else
			curl -fLm 7 "https://cheat.sh/${@}"
	fi
}

_failias() {
	if [ -z "${1}" ]; then
			alias
	else
			alias "${@}"
	fi
}
