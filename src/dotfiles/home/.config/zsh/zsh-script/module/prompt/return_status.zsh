#### The return_status.zsh file contains all shell script related to getting the return status of the previously
#### executed command. When this file is sourced the below shell script will do the following:
####    1. Expose functions for getting the return status of the previously executed command.

check_last_return_status() {
	## If the last command returned a non-zero exit status,
	## then save said status with pink coloring. Otherwise,
	## save said status with red coloring.
	if [ "${return_status}" -eq 0 ]; then
		return_status_prompt="%F{199}${return_status}%f"
	else
		return_status_prompt="%F{196}${return_status}%f"
	fi

	## Print the value of $return_status to standard out.
	printf "%s" "${return_status_prompt}"
}
