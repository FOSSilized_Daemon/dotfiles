#### The time_status.zsh file contains all shell script related to getting the time elapsed from the previously
#### executed command. When this file is sourced the below shell script will do the following:
####    1. Load all needed zsh(1) stock modules.
####    2. Expose functions for getting the time elasped from the previously executed command.
####    3. Set all needed functions into the preexec hook.

## Load needed functions.
autoload -U add-zsh-hook

## Variable declarations.
typeset -F SECONDS

function record-start-time() {
	ZSH_START_TIME=${ZSH_START_TIME:-$SECONDS}
}

function report-start-time() {
	if [ "${ZSH_START_TIME}" ]; then
		DELTA=$(($SECONDS - $ZSH_START_TIME))
		DAYS=$((~~($DELTA / 86400)))
		HOURS=$((~~(($DELTA - $DAYS * 86400) / 3600)))
		MINUTES=$((~~(($DELTA - $DAYS * 86400 - $HOURS * 3600) / 60)))
		SECS=$(($DELTA - $DAYS * 86400 - $HOURS * 3600 - $MINUTES * 60))
		ELAPSED=''

		test "$DAYS" != '0' && ELAPSED="${DAYS}d"
		test "$HOURS" != '0' && ELAPSED="${ELAPSED}${HOURS}h"
		test "$MINUTES" != '0' && ELAPSED="${ELAPSED}${MINUTES}m"

		if [ "$ELAPSED" = '' ]; then
			SECS="$(print -f "%.2f" $SECS)s"
		elif [ "$DAYS" != '0' ]; then
			SECS=''
		else
			SECS="$((~~$SECS))s"
		fi

		ELAPSED="${ELAPSED}${SECS}"
		ITALIC_ON=$'\e[3m'
		ITALIC_OFF=$'\e[23m'
		export RPROMPT="%F{128}%{$ITALIC_ON%}${ELAPSED}%{$ITALIC_OFF%}%f"
		unset ZSH_START_TIME
	fi
}

## Add record-start-time() to the preexec() hook.
add-zsh-hook -Uz preexec record-start-time
