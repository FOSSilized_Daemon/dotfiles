#### The vcs_status.zsh file contains all shell script related to getting the current version
#### control system information. When this file is sourced the below shell script will do the following:
####    1. Load all needed zsh(1) stock modules.
####    2. Expose functions for getting the current version control system information.

## Load needed functions.
autoload -Uz vcs_info

zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' stagedstr "%F{128}●%f"
zstyle ':vcs_info:*' unstagedstr "%F{196}●%f"
zstyle ':vcs_info:*' use-simple true
zstyle ':vcs_info:git+set-message:*' hooks git-untracked
zstyle ':vcs_info:git*:*' formats '%F{199}-%f%F{128}[%f%F{199}%b%f%m%c%u%F{128}]%f'
zstyle ':vcs_info:git*:*' actionformats '%F{199}-%f%F{128}[%f%F{199}%b|%a%m%c%u%F{128}]%f'

function +vi-git-untracked() {
	emulate -L zsh

	if [[ -n $(git ls-files --exclude-standard --others 2> /dev/null) ]]; then
		hook_com[unstaged]+="%F{184}●%f"
	fi
}
