#### The environment_status.zsh file contains all shell script related to getting the currently
#### sourced environment. When this file is sourced the below shell script will do the following:
####    1. Expose functions for getting the currently sourced enviroment.

get_current_environment() {
	## Get the current Python virtual environment.
	if [ -n "${VIRTUAL_ENV}" ] && [ -n "${DIRENV_DIR}" ]; then
		current_python_env=$(basename "${VIRTUAL_ENV}")
	fi

	if [ -n "${current_python_env}" ]; then
		current_environment="%F{199}-%f%F{128}(%f%F{199}${current_python_env}%f%F{128})%f"
	fi

	## Print the value of $current_environment to standard out.
	printf "%s" "${current_environment}"
}
