#### The language.zsh file contains all shell script related to exporting environment variables for language settings.
#### When this file is sourced the below shell script will do the following:
####    1. Export environment variables that set language and encoding settings.

## Use United States english and UTF-8 for language and encoding settings.
export LANG=en_US.UTF-8
