#### The file.zsh file contains all shell script related to exporting environment variables used for files.
#### When this file is sourced the below shell script will do the following:
####    1. Check for the existence of a specified file or directory and create it if it does not exist.
####    2. Export environment variables that point to files and directories.

create_directory() {
	if ! [ -d "${*}" ]; then
		mkdir -p "${*}"
	fi
}

#################################
### XDG Files and Directories ###
#################################
## Use the "HOME/.usr/share/" directory to store shareable data files.
data_files_directory="${HOME}"/.usr/share
create_directory "${data_files_directory}"
export XDG_DATA_HOME="${data_files_directory}"

## Use the "HOME/.var/cache/" directory to store cache files.
cache_files_directory="${HOME}"/.var/cache
create_directory "${cache_files_directory}"
export XDG_CACHE_HOME="${cache_files_directory}"

## Use the "HOME/.var/state/" to store state files.
state_files_directory="${HOME}"/.var/state
create_directory "${state_files_directory}"
export XDG_STATE_HOME="${state_files_directory}"

## Use the "HOME/.var/log/" to store log files.
log_files_directory="${HOME}"/.var/log
create_directory "${log_files_directory}"
export XDG_LOG_HOME="${log_files_directory}"

## Use the "HOME/.var/run/" to store UNIX sockets files.
socket_files_directory="${HOME}"/.var/run
create_directory "${socket_files_directory}"
export XDG_RUN_HOME="${socket_files_directory}"

## Use the "HOME/.library/" to store library files.
library_files_directory="${HOME}"/.library
create_directory "${library_files_directory}"
export XDG_LIBRARY_HOME="${library_files_directory}"

## Use the "HOME/.config/" to store configuration files.
configuration_files_directory="${HOME}"/.config
create_directory "${configuration_files_directory}"
export XDG_CONFIG_HOME="${configuration_files_directory}"

## Use the "HOME/transfer/download/" to store downloaded files.
download_files_directory="${HOME}"/transfer/download
create_directory "${download_files_directory}"
export XDG_DOWNLOAD_HOME="${download_files_directory}"
export XDG_DOWNLOAD_DIR="${XDG_DOWNLOAD_HOME}"

########################################
### UNIX Shell Files and Directories ###
########################################
## Use the "XDG_CONFIG_HOME/zsh/" directory to store zsh(1) configuration
## files.
zsh_configuration_directory="${XDG_CONFIG_HOME}"/zsh
create_directory "${zsh_configuration_directory}"
export ZDOTDIR="${zsh_configuration_directory}"

## Use the "XDG_CACHE_HOME/zsh/history-files/" directory to store
## zsh(1) history directory.
zsh_history_directory="${XDG_CACHE_HOME}"/zsh/history-files
create_directory "${zsh_history_directory}"
export ZHISTDIR="${zsh_history_directory}"

## Use the "XDG_DATA_HOME/dict/quotes" file for ZSH_QUOTES_FILE.
export ZSH_QUOTES_FILE="${XDG_DATA_HOME}"/dict/quotes

## Use the "XDG_DATA_HOME/dict/insults" file for ZSH_INSULTS_FILE.
export ZSH_INSULTS_FILE="${XDG_DATA_HOME}"/dict/insults

#########################################
### Multiplexor Files and Directories ###
#########################################
## Use "XDG_LIBRARY_HOME/generic/shell-script/tmux-status/" for TMUX_STATUS_DIRECTORY.
export TMUX_STATUS_DIRECTORY="${XDG_LIBRARY_HOME}"/generic/shell-script/tmux-status

####################################
### Editor Files and Directories ###
####################################
## Use the 'XDG_LOG_HOME/nvim/nvim.log' file for the nvim(1) log file.
nvim_log_file_directory="${XDG_LOG_HOME}"/nvim
create_directory "${nvim_log_file_directory}"
export NVIM_LOG_FILE="${nvim_log_file_directory}"/nvim.log

## Use the 'XDG_CONFIG_HOME' directory for lem(1) configuration files.
lem_configuration_directory="${XDG_CONFIG_HOME}"/lem
create_directory "${lem_configuration_directory}"
export LEM_HOME="${lem_configuration_directory}"
