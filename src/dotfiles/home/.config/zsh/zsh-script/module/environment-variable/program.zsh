#### The program.zsh file contains all shell script related to exporting environment variables used for programs.
#### When this file is sourced the below shell script will do the following:
####    1. Export environment variables that set generic program types.
####    2. Export environment variables that set program settings.

## Use zsh(1) for shell.
export SHELL=zsh

## Use nvim(1) for visual editor.
export VISUAL=nvim

## Use nvim(1) for line editor.
export EDITOR=nvim

## Use nvim(1) for pager.
export PAGER="nvim -R -c 'set ft=man nomod nolist nowrap' +0 -"

## Use nvim(1) for git(1) pager.
export GIT_PAGER="${PAGER}"

## Use st(1) for terminal emulator.
export TERMINAL=st

## Set the desktop envrionment.
export DESKTOP_SESSION=stumpwm
export XDG_SESSION_DESKTOP="${DESKTOP_SESSION}"
export XDG_CURRENT_DESKTOP="${DESKTOP_SESSION}"
export SESSION="${DESKTOP_SESSION}"

## Use tmux(1) for multiplexor.
export MULTIPLEXOR=tcms

## Use 'main-session' for tcms.
export TMUX_SESSION_NAME='main-session'

## Use firefox(1) for web browser.
export BROWSER=firefox

## Use tor-browser(1) for secure web browser.
export SECURE_BROWSER=tor-browser

## Use slock(1) for lock screen.
export LOCK_SCREEN=slock

## Use dmenu(1) for PROMPT_MENU.
export PROMPT_MENU="dmenu -p"

## Use zathura(1) for DOCUMENT_VIEWER.
export DOCUMENT_VIEWER="zathura"

## Set the default options to pass to tar(1).
export TAR_OPTIONS="--acls --selinux --xattrs --utc"

## Set the default backup suffix that tar(1) should use when creating
## a backup of the files being arachived.
export DEFAULT_BACKUP_SUFFIX=".bac"
export SIMPLE_BACKUP_SUFFIX=".bac"

## Set the default archive name for tar(1) to use when creating archives.
export TAPE="archive.tar"

## Set the directories containing installed manually pages.
export MANPATH=$(find "${HOME}"/.usr/share/man -type d -printf "%p:")"${MANPATH}"

## Set the pager for man(1) to use to display manual pages.
export MAN_PAGER="${PAGER}"
export MANPAGER="${PAGER}"

## Set the maximum width of characters to eighty for exa(1).
export COLUMNS=80

## Enable exa(1)'s strict mode.
export EXA_STRICT=1

## Set exa(1) to not use a grid for output if there will be less than
## four rows.
export EXA_GRID_ROWS=4

## Set exa(1) to print one space between an icon and its file name.
export EXA_ICON_SPACING=1

## Set the exa(1) colors for colorized output.
export EXA_COLORS="no=0;32:fi=4;35:di=1;34:ln=4;33:pi=1;33:do=4;41:so=1;32:ex=1;31:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=30;43"
export LS_COLORS="${EXA_COLORS}"
