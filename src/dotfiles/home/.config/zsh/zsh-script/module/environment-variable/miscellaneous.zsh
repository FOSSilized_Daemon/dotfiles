#### The miscellaneous.zsh file contains all shell script related to exporting environment variables used for
#### miscellaneous things. When this file is sourced the below shell script will do the following:
####    1. Export environment variables that for miscellaneous things.

## Set the terminal type.
export TERM=xterm-256color

## Set the directories containing installed programs.
export PATH="${HOME}/.program/non-privileged:${HOME}/.program/privileged:/${HOME}/.usr/share/bin:${HOME}/.roswell/bin:${HOME}/.local/bin:${PATH}"
