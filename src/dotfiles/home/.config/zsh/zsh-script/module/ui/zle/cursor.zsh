#### The cursor.zsh file contains all shell script related to setting the cursor type.
#### When this file is sourced the below shell script will do the following:
####    1. Load all needed zsh(1) stock modules.
####    2. Expose functions for setting the cursor type.
####    3. Set all needed functions into the precmd hook.

## Load needed functions.
autoload -Uz add-zsh-hook

## If the current mode is set to 'normal (command)' mode, then
## use a block cursor. If the current mode is set to 'insert'
## mode or nothing, then use an i-beam cursor.
zle-keymap-select() {
	if [[ "${KEYMAP}" == vicmd ]] || [[ "${KEYMAP}" == visual ]] ||\
       [[ "${KEYMAP}" == isearch ]] || [[ "${1}" = 'block' ]]; then
		printf '\e[1 q'
	elif [[ "${KEYMAP}" == main ]] || [[ "${KEYMAP}" == .safe ]] || [[ "${KEYMAP}" == emacs ]] ||\
         [[ "${KEYMAP}" == viins ]] || [[ "${1}" = 'beam' ]] || [[ "${1}" = '' ]]; then
		printf '\e[5 q'
	fi
}

## Print an i-beam cursor when initalizing the line editor.
zle-line-init() {
	printf '\e[5 q'
}

## Add zle-keymap-select() and zle-line-init() to the line editor.
zle -N zle-keymap-select
zle -N zle-line-init

## Add zle-line-init() to the precmd() hook.
add-zsh-hook precmd zle-line-init
