## Load the zsh-autopair plugin.
. "${ZDOTDIR}"/plugin/zsh-autopair/zsh-autopair.plugin.zsh

## Close autopairs if they are surrounded by whitespace.
AUTOPAIR_BETWEEN_WHITESPACE=1
