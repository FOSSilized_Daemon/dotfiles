## Load the fast-syntax-highlighting plugin.
. "${ZDOTDIR}"/plugin/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh

## Set the fast-syntax-highlighting theme.
fast-theme "${ZDOTDIR}"/zsh-script/module/plugin/fast-syntax-highlighting/themes/acid.ini > /dev/null 2>&1
