## Load the fzf-zsh plugins.
. "${ZDOTDIR}"/plugin/fzf-zsh/key-bindings.zsh > /dev/null 2>&1
. "${ZDOTDIR}"/plugin/fzf-zsh/completion.zsh > /dev/null 2>&1

## Paste the slected file path(2) into the command-line.
bindkey -r '^T'
zle     -N            fzf-file-widget
bindkey -M emacs '^t' fzf-file-widget
bindkey -M vicmd '^t' fzf-file-widget
bindkey -M viins '^t' fzf-file-widget

## Use cd(1) with fzf(1).
bindkey -r '\ec'
zle     -N             fzf-cd-widget
bindkey -M emacs '^x' fzf-cd-widget
bindkey -M vicmd '^x' fzf-cd-widget
bindkey -M viins '^x' fzf-cd-widget

## Paste the select command from history into the command-line.
bindkey -r '^R'
zle     -N            fzf-history-widget
bindkey -M emacs '^r' fzf-history-widget
bindkey -M vicmd '^r' fzf-history-widget
bindkey -M viins '^r' fzf-history-widget

## Paste the selected completion into the command-line.
bindkey -r '^I'
zle     -N   fzf-completion
bindkey '^i' fzf-completion
