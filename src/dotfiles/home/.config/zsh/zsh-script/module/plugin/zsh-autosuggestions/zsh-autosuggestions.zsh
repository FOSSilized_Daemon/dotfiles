## Load the zsh-autosuggestions plugin.
. "${ZDOTDIR}"/plugin/zsh-autosuggestions/zsh-autosuggestions.zsh > /dev/null 2>&1

## Set active suggestion groups.
ZSH_AUTOSUGGEST_STRATEGY=(match_prev_cmd history completion)

## Highlight suggestions as light grey.
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=242,italic"
