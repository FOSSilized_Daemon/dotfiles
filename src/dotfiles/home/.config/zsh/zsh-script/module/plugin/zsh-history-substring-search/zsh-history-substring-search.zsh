## Load the zsh-history-substring-search plugin.
. "${ZDOTDIR}"/plugin/zsh-history-substring-search/zsh-history-substring-search.plugin.zsh > /dev/null 2>&1

## Search resulsts colors.
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND="fg=198,italic"
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND="fg=196,bold,bg="

## Ensure all resulsts are unique.
HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE=1
