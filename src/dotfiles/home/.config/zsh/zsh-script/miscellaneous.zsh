#### The miscellaneous.zsh file contains all shell script related to setting miscellaneous shell settings. When this
#### file is sourced the below shell script will do the following:
####    1. Load all miscellaneous modules from "ZDOTDIR/zsh-script/module/miscellaneous/".
####    2. Set all miscellaneous shell settings.

## Load all needed modules.
for module in "${ZDOTDIR}"/zsh-script/module/miscellaneous/*
do
	. "${module}"
done

## Set the default permissions for files and directories.
umask 0077

## Change into any directory provided without a command.
setopt AUTO_CD

## Push old directories onto the directory stack.
setopt AUTO_PUSHD

## If an argument to cd(1) is not a directory, then try to expand it preceded by a '~'.
setopt CDABLE_VARS

## Resolve symbolic links to their true values when changing directory,
setopt CHASE_LINKS

## Do not push multiple copies of the same directory onto the directory stack.
setopt PUSHD_IGNORE_DUPS

## Do not print the directory stack after running pushd(1) or popd(1).
setopt PUSHD_SILENT

## Allow '>' redirection to truncate existing files.
setopt CLOBBER

## Try to correct the spelling of commands.
setopt CORRECT

## Allow commenyts even in interactive shells.
setopt INTERACTIVE_COMMENTS

## Note the location of each command the first time it is executed.
setopt HASH_CMDS

## Ensure that files being hashed are actually executables.
setopt HASH_EXECUTABLES_ONLY

## Do not prompt for user input when running 'rm *'.
setopt RM_STAR_SILENT

## Send the HUP signal to all running jobs when the shell exits.
setopt HUP

## Add direnv(1) to zsh(1) hooks.
eval "$(direnv hook zsh)"

## Print an insult if zsh(1) returned an exit
## status of '127' (i.e. command-not-found).
command_not_found_handler() {
	print_insult
}

## Print a quote when initializing a shell.
print_quote
