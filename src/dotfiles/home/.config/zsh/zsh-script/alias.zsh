#### The alias.zsh file contains all shell script related to setting shell aliases. When this file is sourced
#### the below shell script will do the following:
####    1. Load all alias modules from "ZDOTDIR/zsh-script/module/alias/".
####    2. Set all aliases to their respective command.

## Load all needed modules.
for module in "${ZDOTDIR}"/zsh-script/module/alias/*
do
	. "${module}"
done

## Archive Utilities.
alias cpic='create_cpio_archive'
alias cpid='extract_cpio_archive'
alias gzc='create_gzip_archive'
alias gzd='extract_gzip_archive'
alias gzl='gzip -l'
alias trc='create_tar_archive'
alias trd='extract_tar_archive'
alias trl='list_tar_archive'
alias zpc='create_zip_archive'
alias uzd='extract_zip_archive'
alias uzl='list_zip_archive'
alias xzc='create_xz_archive'
alias xzd='extract_zip_archive'
alias xzl='list_xz_archive'

## Disk Usage Utilities.
alias df='df -hHP'
alias du='du -aHh'

## Editor Utilities.
alias le='ed -p ">"'
alias ve='nvim'
alias he='nvim -b'

## Encoding and Decoding Utilities.
alias e32='base32'
alias d32='base32 -d'
alias e64='base64'
alias d64='base64 -d'

## File Formatting Utilities.
alias dos2unix='dos2unix -ko'
alias unix2dos='unix2dos -ko'

## File Management Utilities.
alias cp='cp -HpRv'
alias ln='ln -sv'
alias mkdir='mkdir -pv'
alias mkfifo='mkfifo -v'
alias mv='mv -v'
alias rm='rm -Rv'
alias srm='srm -drv'
alias smem='smem -v'
alias sfill='sfill -z'
alias sswap='sswap -v'

## File Searching Utilities.
alias l='exa -F --group-directories-first --colour=always --icons'
alias ls='exa -F --group-directories-first --colour=always --icons'
alias la='exa -aF --group-directories-first --colour=always --icons'
alias ll='exa -alF --extended --group-directories-first --colour=always --icons'
alias lr='exa -alTF --extended --group-directories-first --colour=always --icons'
alias lsg='exa -F --group-directories-first --colour=always --icons --git-ignore'
alias lag='exa -aF --group-directories-first --colour=always --icons --git-ignore'
alias llg='exa -alF --extended --group-directories-first --colour=always --icons --git-ignore'
alias lrg='exa -alTF --extended --group-directories-first --colour=always --icons --git-ignore'

## Text Formatting Utilities.
alias expand='expand -t 4'

## Version Control Utilities.
alias gpl='git_pull'
alias gp='git_push'
alias ga='git_add'
alias gr='git_remove'
alias grst='git_reset'
alias gco='git_checkout'
alias gb='git_branch'
alias gc='git commit -m'
alias gcl='git_clone'
alias gls='git_ls'
alias gs='git status --short'
alias gml='git_submodule'
alias gd='git_diff'
alias gi='git_ignore'

## Bookmarked Directories.
alias cm='cybermedia_parent'
alias msc='music_parent'
alias sct='screenshot_parent'
alias wlp='wallpaper_parent'
alias vdo='video_parent'
alias tv='television_show_parent'
alias mvi='movie_parent'
alias dx='document_parent'
alias dev='development_parent'
alias cpj='core_project_parent'
alias cpjd='core_project_documentation_parent'
alias cpjl='core_project_library_parent'
alias cpjp='core_project_program_parent'
alias dvm='development_miscellaneous_parent'
alias md='media_parent'
alias dl='download_parent'
alias ul='upload_parent'
alias cfg='config_parent'
alias lib='library_parent'
alias prg='program_parent'
alias usr='usr_parent'
alias var='var_parent'

## Miscellaneous.
alias urss='sfeed_update'
alias vrss='view_rss'
alias failias='_failias'
alias cheat='_cheat'
