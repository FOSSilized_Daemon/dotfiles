#### The prompt.zsh file contains all shell script related to prompts. When this file is sourced the
#### below shell script will do the following:
####    1. Load all needed zsh(1) stock modules.
####    2. Load all needed modules from "ZDOTDIR/zsh-script/module/prompt/".
####    3. Configure and export all prompts.
####    4. Set all needed functions into the precmd hook.

## Load needed functions.
autoload -Uz add-zsh-hook
autoload -Uz colors && colors

## Load all needed modules.
for module in "${ZDOTDIR}"/zsh-script/module/prompt/*
do
	. "${module}"
done

## Allow substitutions within prompt(s).
setopt PROMPTSUBST

## Export the PS1 prompt.
export PS1='%F{128}[%f$(check_last_return_status)%F{128}]%f${vcs_info_msg_0_}$(get_current_environment) %F{199}>%f%F{190}>%f%F{51}>%f%F{184}%(1j.*.)%f '

## Export the PS2 prompt.
export PS2='%F{190}>%f '

## Export the PS3 prompt.
export PS3='%F{190}+%f '

## Export the SPROMPT.
export SPROMPT="Correct $fg[red]%R$reset_color to $fg[cyan]%r$reset_color? (y/n/a/e/N) "

precmd() {
	## Capture the return status of the last command.
	return_status="${?}"

	## Load the vcs_info of the current directory.
	vcs_info

	## Report the start time of the last command.
	report-start-time
}
