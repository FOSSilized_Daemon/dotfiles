#### The hook.zsh file contains all shell script related to hooks. When this file sourced the
#### below shell script will do the following:
####    1. Load all needed zsh(1) stock modules.
####    2. Add hook functions to their corresponding hooks.

## Load all needed modules.
autoload -U add-zsh-hook

list_current_directory() {
	## After every cd(1) list the contents of the current
	## working directory.
	ls
}

## Add list_current_directory to the chpwd hook.
add-zsh-hook -Uz chpwd list_current_directory
