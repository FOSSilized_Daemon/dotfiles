(in-package :stumpwm)

;; The acid.lisp file contains all of the
;; Common Lisp related to configuring theme
;; settings. When this file is loaded the
;; below Common Lisp will do the following
;;    1. Apply the acid theme to stumpwm(1)

(defvar black        "#000000")
(defvar blueviolet   "#5f00ff")
(defvar chartreuse3  "#5faf00")
(defvar chartreuse4  "#5fd700")
(defvar cyan1        "#00ffff")
(defvar darkred      "#870000")
(defvar darkviolet   "#8700d7")
(defvar deep-pink1   "#ff00af")
(defvar deep-pink3   "#d70087")
(defvar deep-pink4   "#87005f")
(defvar gold3        "#afaf00")
(defvar greenyellow  "#afff00")
(defvar grey11       "#1c1c1c")
(defvar grey42       "#6c6c6c")
(defvar magenta1     "#ff00ff")
(defvar magenta3     "#af00af")
(defvar purple       "#8700ff")
(defvar purple4      "#5f0087")
(defvar red1         "#ff0000")
(defvar yellow3      "#d7d700")
(defvar yellow4      "#878700")
(defvar tungsten1    "#f6b008")
(defvar tungsten2    "#e5a200")
(defvar tungsten3    "#bd8704")


;; Window colors.
(set-fg-color      cyan1)
(set-bg-color      black)
(set-border-color  cyan1)
(set-focus-color   black)
(set-unfocus-color black)

;; Mode-line colors.
(setf *mode-line-foreground-color* cyan1
      *mode-line-background-color* black
      *mode-line-border-color*     black)

;; Ensure mode-line changes its' theme.
(toggle-mode-line (current-screen) (current-head))
(toggle-mode-line (current-screen) (current-head))
