(in-package :stumpwm)

;; The acid.lisp file contains all of the
;; Common Lisp related to configuring theme
;; settings. When this file is loaded the
;; below Common Lisp will do the following
;;    1. Apply the acid theme to stumpwm(1)

(defvar black        "#000000")
(defvar tungsten1    "#f6b008")
(defvar tungsten2    "#e5a200")
(defvar tungsten3    "#bd8704")


;; Window colors.
(set-fg-color      tungsten1)
(set-bg-color      black)
(set-border-color  tungsten2)
(set-focus-color   black)
(set-unfocus-color black)

;; Mode-line colors.
(setf *mode-line-foreground-color* tungsten1
      *mode-line-background-color* black
      *mode-line-border-color*     black)

;; Ensure mode-line changes its' theme.
(toggle-mode-line (current-screen) (current-head))
(toggle-mode-line (current-screen) (current-head))
