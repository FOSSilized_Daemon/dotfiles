(in-package :stumpwm)

;; The init.lisp file contains all of the Common Lisp
;; related to loading all Common Lisp used to configure
;; stumpwm(1). When this file is loaded the below
;; Common Lisp will do the following.
;;    1. Load quicklisp if it is not already loaded.
;;    2. Load the "uiop" library.
;;    3. Define a macro to load modules from "XDG_LIBRARY_HOME/generic/common-lisp/stumpwm/common-lisp".
;;    4. Load all needed files from INSERT-DIRECTORY.

#-quicklisp
(let ((quicklisp-init (merge-pathnames ".library/generic/common-lisp/quicklisp/setup.lisp" (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload "uiop")

(defmacro load-module (&rest module-list)
  (dolist (module module-list)
    (load (merge-pathnames (concatenate 'string ".library/generic/common-lisp/stumpwm/common-lisp/" module) (uiop:getenv "XDG_LIBRARY_HOME")))))

(load-module "file.lisp"
             "group.lisp"
             "key-binding.lisp"
             "user-interface.lisp")
