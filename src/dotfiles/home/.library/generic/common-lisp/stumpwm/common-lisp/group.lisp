(in-package :stumpwm)

;; The group.lisp file contains all of the
;; Common Lisp related to configuring workspaces.
;; When this file is loaded the below Common Lisp
;; will do the following.
;;    1. Create named groups.
;;    2. Define group rules for applications.
;;    3. Define settings for each layout used.

(defun reorder-groups (&optional (screen (current-screen)))
  "Reorder groups starting from one."
  (let ((iterator 1))
    (loop for current-group in (screen-groups screen)
          when (>= (group-number current-group) 0)
          do (setf (group-number current-group) iterator)
          (incf iterator))))

(when *initializing*
  (gnewbg-dynamic "term")
  (gnewbg-dynamic "www")
  (gnewbg-dynamic "multimedia")
  (gnewbg-dynamic "comms")
  (gnewbg-dynamic "media")
  (kill-group (find-group (current-screen) "Default")
              (find-group (current-screen) "term"))
  (reorder-groups)
  (switch-to-group (find-group (current-screen) "term")))

(define-frame-preference "www"
  (0 nil t :class "nyxt")
  (0 nil t :class "Nyxt")
  (0 nil t :class "Firefox")
  (0 nil t :class "firefox")
  (0 nil t :class "Navigator")
  (0 nil t :class "Chromium")
  (0 nil t :class "chromium")
  (0 nil t :class "Chrome")
  (0 nil t :class "chrome"))

(define-frame-preference "multimedia"
  (0 nil t :class "Blender")
  (0 nil t :class "blender")
  (0 nil t :class "Gimp")
  (0 nil t :class "gimp")
  (0 nil t :class "Inkscpae")
  (0 nil t :class "inkscpae"))

(define-frame-preference "comms"
  (0 nil t :class "Discord")
  (0 nil t :class "discord"))

(define-frame-preference "media"
  (0 nil t :class "Mpv")
  (0 nil t :class "mpv")
  (0 nil t :class "Vlc")
  (0 nil t :class "vlc")
  (0 nil t :class "Steam")
  (0 nil t :class "steam")
  (0 nil t :class "Lutris")
  (0 nil t :class "lutris"))

;; Split dynamic windows by 50%.
; (when-let ((current-group (find-if #'dynamic-group-p
;                                    (screen-groups (current-screen)))))
;           (setf (dynamic-group-default-split-ratio current-group :all) 0.5))
