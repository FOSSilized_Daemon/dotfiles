(in-package :stumpwm)

;; The user-interface.lisp file contains all of the
;; Common Lisp related to configuring user interface
;; settings. When this file is loaded the below
;; Common Lisp will do the following.
;;    1. Configure general user interface settings.

;; Do not send a startup message.
(setf *startup-message* nil)

;; Show a message for three seconds.
(setq *timeout-wait* 3)

;; Do not save duplicate commands
;; in the input window history.
(setq *input-history-ignore-duplicates* t)

;; Show the message window at the bottom-right
;; of the screen.
(setq *message-window-gravity* :bottom-right)

;; Set the message window border width.
(set-msg-border-width 2)

;; Add some padding to the message window
;; to improve readability.
(setq *message-window-padding*   5
      *message-window-y-padding* 5)

;; Show the input window at the bottom-right
;; of the screen.
(setq *input-window-gravity* :bottom-right)

;; Add some padding to the input window
;; to improve readability.
(setq *input-window-padding*   5
      *input-window-y-padding* 5)

;; Do not show any information about the
;; current window.
(setq *window-format* nil)

;; Set the window border style.
(setq *window-border-style* :none)

;; Set the normal window border width.
(setq *normal-border-width* 0)

;; Set the float window border width.
(setq *float-window-border* 1)

;; Set the float window title height.
(setq *float-window-title-height* 0)

;; Set the maximized window border wifth.
(setq *maxsize-border-width* 0)

;; Set the transient window border width.
(setq *transient-border-width* 0)

;; Disable size hints.
(setq *ignore-wm-inc-hints* t)

;; Show the mode-line at the top of the
;; screen.
(setq *mode-line-position* :bottom)

;; Add some padding to the mode-line
;; to improve readability.
(setq *mode-line-pad-x* 1
      *mode-line-pad-y* 1)

;; Set the mode-line border width.
(setq *mode-line-border-width* 0)

;; Update the mode-line every second.
(setq *mode-line-timeout* 1)

;; Enable polybar(1).
(when *initializing*
  (run-shell-command "start-polybar"))

;; Add hooks to update polybar(1).
(defun polybar-update-groups ()
  (run-shell-command "polybar-msg action stumpwm-groups 1"))

(add-hook *new-window-hook*     (lambda (win)       (polybar-update-groups)))
(add-hook *destroy-window-hook* (lambda (win)       (polybar-update-groups)))
(add-hook *focus-window-hook*   (lambda (win lastw) (polybar-update-groups)))
(add-hook *focus-group-hook*    (lambda (grp lastg) (polybar-update-groups)))

;; Load the stumpwm(1) acid theme.
(load (merge-pathnames "themes/tungsten.lisp" *home-dir*))
