(in-package :stumpwm)

;; The key-binding.lisp file contains all of the Common Lisp
;; related to configuring key-binding settings. When this
;; file is loaded the below Common Lisp will do the following.
;;    1. Define macros for creating key-binding commands.
;;    2. Set general key-binding settings.
;;    3. Define key-bindings.

(defmacro define-user-command (function-name command-name &rest arguments-list)
  `(defcommand ,function-name() ()
    (uiop:launch-program (cons ,command-name ',arguments-list))))

;; Register AltGr key.
(setf *altgr-offset* 4)
(register-altgr-as-modifier)

;; Set the rotation focus policy to follow
;; the currently selected window.
(setf *rotation-focus-policy* :follow)

;; Set window focus to follow mouse clicks.
(setf *mouse-focus-policy* :click)

;; Set "MOD4" to the "Windows" key and then set the prefix
;; key to "MOD4".
(run-shell-command "xmodmap -e 'clear mod4'" t)
(run-shell-command "xmodmap -e 'add mod4 = Super_L'" t)
(set-prefix-key (kbd "s-a"))

;; Application Launcher(s).
(define-user-command gal "gal")
(define-key *top-map* (kbd "s-d") "gal")

;; Terminal(s).
(define-user-command terminal             (uiop:getenv "TERMINAL"))
(define-user-command multiplexed-terminal "tcms" "-t")

(defvar *terminal-group*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "t") "terminal")
    (define-key m (kbd "m") "multiplexed-terminal")
    m))

(define-key *root-map* (kbd "t")          '*terminal-group*)
(define-key *top-map*  (kbd "s-Return")   "multiplexed-terminal")
(define-key *top-map*  (kbd "s-S-Return") "terminal")

;; Web Browser(s).
(define-user-command web-browser        (uiop:getenv "BROWSER"))
(define-user-command secure-web-browser (uiop:getenv "SECURE_BROWSER"))

(defvar *browser-group*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "m") "web-browser")
    (define-key m (kbd "s") "secure-web-browser")
    m))

(define-key *root-map* (kbd "b") '*browser-group*)

;; Video Game(s).
(define-user-command lutris     "lutris")
(define-user-command gog-galaxy "gog-galaxy")
(define-user-command itchio     "itchio")
(define-user-command steam      "steam")

(defvar *video-game-group*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "l") "lutris")
    (define-key m (kbd "g") "gog-galaxy")
    (define-key m (kbd "i") "itchio")
    (define-key m (kbd "s") "steam")
    m))

(define-key *root-map* (kbd "g") '*video-game-group*)

;; Help Application(s).
(define-user-command shqu "shqu")

(defvar *help-group*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "g") "shqu")
    m))

(define-key *root-map* (kbd "h") '*help-group*)

;; Media Application(s).
(define-user-command mpc-prev   "mpc" "-q" "prev")
(define-user-command mpc-toggle "mpc" "-q" "toggle")
(define-user-command mpc-next   "mpc" "-q" "next")
(define-user-command youtube    (uiop:getenv "BROWSER") "-new-tab" "https://www.invidious.snopyta.org/feed/popular")

(defvar *media-group*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "comma")  "mpc-prev")
    (define-key m (kbd "period") "mpc-toggle")
    (define-key m (kbd "minus")  "mpc-next")
    (define-key m (kbd "y")      "youtube")
    m))

(define-key *root-map* (kbd "m")        '*media-group*)
(define-key *top-map*  (kbd "s-comma")  "mpc-prev")
(define-key *top-map*  (kbd "s-period") "mpc-toggle")
(define-key *top-map*  (kbd "s-minus")  "mpc-next")

;; Communication Application(s).
(define-user-command discord    "discord")
(define-user-command protonmail (uiop:getenv "BROWSER") "-new-tab" "https://www.proton.me/mail")

(defvar *communication-group*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "d") "discord")
    (define-key m (kbd "E") "protonmail")
    m))

(define-key *root-map* (kbd "c") '*communication-group*)

;; Editing Application(s).
(define-user-command blender         "blender")
(define-user-command davinci-resolve "davinciresolve")
(define-user-command audacity        "audacity")
(define-user-command gimp            "gimp")

(defvar *editing-group*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "v")   "blender")
    (define-key m (kbd "V")   "davinci-resolve")
    (define-key m (kbd "a")   "audacity")
    (define-key m (kbd "i")   "gimp")
    m))

(define-key *root-map* (kbd "e") '*editing-group*)

;; System Application(s).
(define-user-command volume-down            "pactl" "set-sink-volume" "@DEFAULT_SINK@" "-5%")
(define-user-command volume-up              "pactl" "set-sink-volume" "@DEFAULT_SINK@" "+5%")
(define-user-command volume-toggle-mute     "pactl" "set-sink-mute"   "@DEFAULT_SINK@" "toggle")
(define-user-command brightness-down        "brightnessctl" "set" "-5%")
(define-user-command brightness-up          "brightnessctl" "set" "+5%")
(define-user-command close-notification     "dunstctl" "close")
(define-user-command close-all-notification "dunstctl" "closeall")
(define-user-command cycle-notification     "dunstctl" "history-pop")
(define-user-command logout-menu            "gsm")

(defvar *system-group*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "j")     "volume-down")
    (define-key m (kbd "k")     "volume-up")
    (define-key m (kbd "l")     "volume-toggle-mute")
    (define-key m (kbd "J")     "brightness-down")
    (define-key m (kbd "K")     "brightness-up")
    (define-key m (kbd "n")     "close-notification")
    (define-key m (kbd "N")     "close-all-notification")
    (define-key m (kbd "C-n")   "cycle-nofication")
    (define-key m (kbd "Q")     "gsm")
    (define-key m (kbd "r")     "loadrc")
    (define-key m (kbd "R")     "restart-hard")
    m))

(define-key *root-map* (kbd "s")   '*system-group*)
(define-key *top-map*  (kbd "s-Q") "logout-menu")

;; Window Management.
(defvar *window-group*
  (let ((m (make-sparse-keymap)))
    (define-key m (kbd "j")         "prev")
    (define-key m (kbd "k")         "next")
    (define-key m (kbd "J")         "exchange-direction down")
    (define-key m (kbd "K")         "exchange-direction up")
    (define-key m (kbd "S-Return")  "exchange-with-master")
    (define-key m (kbd "1")         "gselect 1")
    (define-key m (kbd "2")         "gselect 2")
    (define-key m (kbd "3")         "gselect 3")
    (define-key m (kbd "4")         "gselect 4")
    (define-key m (kbd "5")         "gselect 5")
    (define-key m (kbd "!")         "gmove term")
    (define-key m (kbd "\"")        "gmove www")
    (define-key m (kbd "s-section") "gmove multimedia")
    (define-key m (kbd "$")         "gmove comms")
    (define-key m (kbd "%")         "gmove media")
    (define-key m (kbd "TAB")       "gother")
    (define-key m (kbd "(")         "sprev")
    (define-key m (kbd ")")         "snext")
    (define-key m (kbd "C-(")       "sother")
    (define-key m (kbd "x")         "kill")
    m))

(define-key *root-map* (kbd "w")         '*window-group*)
(define-key *top-map*  (kbd "s-j")       "prev")
(define-key *top-map*  (kbd "s-k")       "next")
(define-key *top-map*  (kbd "s-J")       "rotate-windows backward")
(define-key *top-map*  (kbd "s-K")       "rotate-windows forward")
(define-key *top-map*  (kbd "s-1")       "gselect 1")
(define-key *top-map*  (kbd "s-2")       "gselect 2")
(define-key *top-map*  (kbd "s-3")       "gselect 3")
(define-key *top-map*  (kbd "s-4")       "gselect 4")
(define-key *top-map*  (kbd "s-5")       "gselect 5")
(define-key *top-map*  (kbd "s-!")       "gmove term")
(define-key *top-map*  (kbd "s-\"")      "gmove www")
(define-key *top-map*  (kbd "s-section") "gmove multimedia")
(define-key *top-map*  (kbd "s-$")       "gmove comms")
(define-key *top-map*  (kbd "s-%")       "gmove media")
(define-key *top-map*  (kbd "s-TAB")     "gother")
(define-key *top-map*  (kbd "s-(")       "sprev")
(define-key *top-map*  (kbd "s-)")       "snext")
(define-key *top-map*  (kbd "s-C-(")     "sother")
(define-key *root-map* (kbd "x")         "kill")
