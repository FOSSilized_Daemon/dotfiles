(in-package :stumpwm)

;; The file.lisp file contains all of the Common Lisp
;; related to configuring file and directory settings.
;; When this file is loaded the below Common Lisp will
;; do the following.
;;    1. Set file and directory locations.

;; Set stumpwm(1)'s home directory.
(setf *home-dir* (merge-pathnames ".library/generic/common-lisp/stumpwm/" (uiop:getenv "XDG_LIBRARY_HOME")))

;; Set stumpwm(1)'s modules directory.
(setf *contrib-dir* (merge-pathnames "modules/" *home-dir*))
(set-module-dir *contrib-dir*)

;; Set stumpwm(1)'s data directory.
(setf *data-dir* (merge-pathnames "stumpwm/" (uiop:getenv "XDG_DATA_HOME")))

;; Set stumpwm(1)'s undo directory.
(setf *undo-dir* (merge-pathnames "stumpwm/undo/" (uiop:getenv "XDG_CACHE_HOME")))
