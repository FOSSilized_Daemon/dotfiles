(defsystem "repl-config"
  :description "FOSSilized_Daemon's common-lisp repl configuration."
  :long-description "FOSSilized_Daemon's common-lisp repl configuration."
  :author "FOSSilized_Daemon <fossilizeddaemon@protonmail.com>"
  :licence "AGPL"
  :version "0.0.1"
  :serial t
  :depends-on ("linedit" "cl-ppcre")
  :components ((:file "package")
               (:file "common-lisp/library")
               (:file "common-lisp/environment-variable")
               (:file "common-lisp/login")
               (:file "common-lisp/function")))
