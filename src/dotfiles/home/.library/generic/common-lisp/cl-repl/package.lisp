(defpackage :repl-config
  (:use :common-lisp :uiop :linedit :cl-ppcre)
  (:export
    ;; login.lisp
    #:init-environment

    ;; function.lisp
    #:df
    #:du
    #:he
    #:le
    #:ve
    #:e32
    #:d32
    #:e64
    #:d64
    #:dos2unix
    #:unix2dos
    #:cp
    #:ln
    #:mkdir
    #:mkfifo
    #:mv
    #:rm
    #:srm
    #:smem
    #:sfill
    #:sswap
    #:l
    #:ls
    #:la
    #:ll
    #:lr
    #:lsg
    #:lag
    #:llg
    #:lrg
    #:expand
    #:cheat
    #:view-rss))
