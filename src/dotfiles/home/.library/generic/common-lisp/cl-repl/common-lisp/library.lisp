(in-package :repl-config)

;;;; The library.lisp file contains all of the common-lisp related
;;;; to loading external libraries. When this file is loaded the
;;;; below common-lisp will do the following.
;;;;    1. Expose a function to easily load a common-lisp library.
;;;;    2. Load all desired common-lisp libraries.
;;;;    3. Configure libraries.

;; Development Libraries.
(ql:quickload "uiop"     :silent t)
(ql:quickload "cl-ppcre" :silent t)

;; User Interface Libraries.
; (ql:quickload  "linedit" :silent t)

; (linedit:formedit :prompt1 ">>> " :prompt2 "> ")
; (linedit:install-repl :wrap-current t :eof-quits t)
