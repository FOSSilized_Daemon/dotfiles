(in-package :repl-config)

;;;; The environment-variable.lisp file contains all of the common-lisp related
;;;; to setting environment variables. When this file is loaded the below
;;;; common-lisp will do the following.
;;;;    1. Create a macro that will export a given environment-variable
;;;;       with the value of environment-value. Additionally, perform
;;;;       specified actions based on the environment-type.
;;;;    2. Export all environment variables.

(defmacro export-environment-variable (environment-variable environment-value environment-type)
  "Export environment-variable using (uiop:getenv) with the value of environment-value."
  `(progn
     (when (eql 'path ,environment-type)
       (ensure-directories-exist ,environment-value))
    (setf (uiop:getenv ,environment-variable) (namestring ,environment-value))))

(defun export-path-environment-variable (environment-variable &rest directory-paths)
  "Export a path environment variable, such as PATH, using (uiop:getenv) with the value of directory-paths."
  (cond ((when (uiop:getenv environment-variable)
          (let ((sub-directory (format nil "~{~A~^:~}" directory-paths)))
            (setf (uiop:getenv environment-variable) (concatenate 'string sub-directory ":" (uiop:getenv environment-variable))))))
        ((when (not (uiop:getenv environment-variable))
          (let ((sub-directory (format nil "~{~A~^:~}" directory-paths)))
            (setf (uiop:getenv environment-variable) sub-directory))))))

;; XDG Files and Directories.
(export-environment-variable "HOME"              (user-homedir-pathname)                                     'path)
(export-environment-variable "XDG_CONFIG_HOME"   (merge-pathnames ".config/"           (uiop:getenv "HOME")) 'path)
(export-environment-variable "XDG_DATA_HOME"     (merge-pathnames ".usr/share/"        (uiop:getenv "HOME")) 'path)
(export-environment-variable "XDG_CACHE_HOME"    (merge-pathnames ".var/cache/"        (uiop:getenv "HOME")) 'path)
(export-environment-variable "XDG_STATE_HOME"    (merge-pathnames ".var/state/"        (uiop:getenv "HOME")) 'path)
(export-environment-variable "XDG_LOG_HOME"      (merge-pathnames ".var/log/"          (uiop:getenv "HOME")) 'path)
(export-environment-variable "XDG_RUN_HOME"      (merge-pathnames ".var/run/"          (uiop:getenv "HOME")) 'path)
(export-environment-variable "XDG_LIBRARY_HOME"  (merge-pathnames ".library/"          (uiop:getenv "HOME")) 'path)
(export-environment-variable "XDG_PROGRAM_HOME"  (merge-pathnames ".program/"          (uiop:getenv "HOME")) 'path)
(export-environment-variable "XDG_DOWNLOAD_HOME" (merge-pathnames "transfer/download/" (uiop:getenv "HOME")) 'path)
(export-environment-variable "XDG_DOWNLOAD_DIR"  (merge-pathnames "transfer/download/" (uiop:getenv "HOME")) 'path)
(export-environment-variable "XDG_UPLOAD_HOME"   (merge-pathnames "transfer/upload/"   (uiop:getenv "HOME")) 'path)

;; Multiplexor Files and Directories.
(export-environment-variable "TMUX_STATUS_DIRECTORY" (merge-pathnames "generic/shell-script/tmux-status/" (uiop:getenv "XDG_LIBRARY_HOME")) 'path)

;; Web Browser Files and Directories.
(export-environment-variable "NYXT_HOME" (merge-pathnames "generic/common-lisp/nyxt/" (uiop:getenv "XDG_LIBRARY_HOME")) 'path)

;; Editor Files and Directories.
(export-environment-variable "LEM_HOME"      (merge-pathnames "generic/common-lisp/lem/" (uiop:getenv "XDG_LIBRARY_HOME")) 'path)
(export-environment-variable "NVIM_LOG_FILE" (merge-pathnames "nvim/nvim.log"            (uiop:getenv "XDG_DATA_HOME"))    'path)

;; Language and Encoding Values.
(export-environment-variable "LANG" "en_US.UTF-8"    'lang)
(export-environment-variable "TERM" "xterm-256color" 'encoding)

;; Program Values.
(export-environment-variable "SHELL"                 "sh"                                               'program)
(export-environment-variable "VISUAL"                "nvim"                                             'program)
(export-environment-variable "EDITOR"                "ed -p '>'"                                        'program)
(export-environment-variable "HEXADECIMAL_EDITOR"    "nvim -b"                                          'program)
(export-environment-variable "PAGER"                 "nvim -R +Man"                                     'program)
(export-environment-variable "GIT_PAGER"             (uiop:getenv "PAGER")                              'program)
(export-environment-variable "MAN_PAGER"             (uiop:getenv "PAGER")                              'program)
(export-environment-variable "MANPAGER"              (uiop:getenv "PAGER")                              'program)
(export-environment-variable "TERMINAL"              "st"                                               'program)
(export-environment-variable "XDG_SESSION_DESKTOP"   "dwm"                                              'program)
(export-environment-variable "XDG_CURRENT_DESKTOP"   (uiop:getenv "XDG_SESSION_DESKTOP")                'program)
(export-environment-variable "DESKTOP_SESSION"       (uiop:getenv "XDG_SESSION_DESKTOP")                'program)
(export-environment-variable "SESSION"               (uiop:getenv "XDG_SESSION_DESKTOP")                'program)
(export-environment-variable "MULTIPLEXOR"           "tcms"                                             'program)
(export-environment-variable "TMUX_SESSION_NAME"     "main-session"                                     'program)
(export-environment-variable "BROWSER"               "firefox"                                          'program)
(export-environment-variable "SECURE_BROWSER"        "tor-browser"                                      'program)
(export-environment-variable "LOCK_SCREEN"           "slock"                                            'program)
(export-environment-variable "PROMPT_MENU"           "dmenu -p"                                         'program)
(export-environment-variable "DOCUMENT_VIEWER"       "zathura"                                          'program)
(export-environment-variable "TAR_OPTIONS"           "--acls --selinux --xattrs --utc"                  'program)
(export-environment-variable "DEFAULT_BACKUP_SUFFIX" ".bac"                                             'program)
(export-environment-variable "SIMPLE_BACKUP_SUFFIX"  ".bac"                                             'program)
(export-environment-variable "TAPE"                  "archive.bac"                                      'program)
(export-environment-variable "COLUMNS"               "80"                                               'program)
(export-environment-variable "EXA_STRICT"            "1"                                                'program)
(export-environment-variable "EXA_GRID_ROWS"         "4"                                                'program)
(export-environment-variable "EXA_ICON_SPACING"      "4"                                                'program)
(export-environment-variable "EXA_GRID_ROWS"         "4"                                                'program)
(export-environment-variable "EXA_COLORS"            "no=0;32:fi=4;35:di=1;34:ln=4;33:pi=1;33:do=4;41:so=1;32:ex=1;31:bd=34;46:cd=34;43:su=30;41:sg=30;46:tw=30;42:ow=30;43" 'program)
(export-environment-variable "LS_COLORS"             (uiop:getenv "EXA_COLORS")                         'program)

;; Miscellaneous values.
(export-path-environment-variable "PATH" (concatenate 'string (namestring (user-homedir-pathname)) ".program/non-privileged")
                                         (concatenate 'string (namestring (user-homedir-pathname)) ".program/privileged")
                                         (concatenate 'string (namestring (user-homedir-pathname)) ".usr/share/bin")
                                         (concatenate 'string (namestring (user-homedir-pathname)) ".local/bin")
                                         (concatenate 'string (namestring (user-homedir-pathname)) ".roswell/bin"))

(export-path-environment-variable "MANPATH" (concatenate 'string (uiop:getenv "XDG_DATA_HOME") "/man/man1")
                                            (concatenate 'string (uiop:getenv "XDG_DATA_HOME") "/man/man2")
                                            (concatenate 'string (uiop:getenv "XDG_DATA_HOME") "/man/man3")
                                            (concatenate 'string (uiop:getenv "XDG_DATA_HOME") "/man/man4")
                                            (concatenate 'string (uiop:getenv "XDG_DATA_HOME") "/man/man5")
                                            (concatenate 'string (uiop:getenv "XDG_DATA_HOME") "/man/man6")
                                            (concatenate 'string (uiop:getenv "XDG_DATA_HOME") "/man/man7")
                                            (concatenate 'string (uiop:getenv "XDG_DATA_HOME") "/man/man8")
                                            (concatenate 'string (uiop:getenv "XDG_DATA_HOME") "/man/man9"))
