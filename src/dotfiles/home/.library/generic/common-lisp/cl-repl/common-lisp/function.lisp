(in-package :repl-config)

;;;; The function.lisp file contains all of the common-lisp related
;;;; to custom user functions. When this file is loaded the below
;;;; common-lisp will do the following.
;;;;    1. Define macros for more easily creating simple function templates.
;;;;    2. Expose user functions.

(defmacro define-user-function (function-name is-interactive command-name &rest default-arguments)
  `(defun ,function-name (&rest arguments-list)
     (uiop:run-program (cons ,command-name (or arguments-list ',default-arguments))
                       :output ,@(if is-interactive
                                   '(:interactive :input :interactive)
                                   '(*standard-output*)))))

(defmacro define-bookmarked-directory (function-name parent-pathname)
  `(defun ,function-name (&optional (sub-pathname nil))
     (uiop:chdir (ensure-directories-exist (or (merge-pathnames sub-pathname ,parent-pathname) ,parent-pathname)))))

;; Disk Usage Functions.
(define-user-function df nil "df" "-hHP")
(define-user-function du nil "du" "-aHh")

;; Editor Functions.
(define-user-function he t "nvim" "-b")
(define-user-function le t "ed"   "-p" ">")
(define-user-function ve t "nvim")

;; Encoding and Decoding Functions.
(define-user-function e32 nil "base32")
(define-user-function d32 nil "base32" "-d")
(define-user-function e64 nil "base64")
(define-user-function d64 nil "base64" "-d")

;; File Formatting Functions.
(define-user-function dos2unix nil "dos2unix" "-ko")
(define-user-function unix2dos nil "unix2dos" "-ko")

;; File Management Functions.
(define-user-function cp     nil "cp"     "-HpRv")
(define-user-function ln     nil "ln"     "-sv")
(define-user-function mkdir  nil "mkdir"  "-pv")
(define-user-function mkfifo nil "mkfifo" "-v")
(define-user-function mv     nil "mv"     "-v")
(define-user-function rm     nil "rm"     "-Rv")
(define-user-function srm    nil "srm"    "-drv")
(define-user-function smem   nil "smem"   "-v")
(define-user-function sfill  nil "sfill"  "-z")
(define-user-function sswap  nil "sswap"  "-v")

;; File Searching Functions.
(define-user-function l   nil "exa" "-F"    "--group-directories-first"              "--colour=always" "--icons")
(define-user-function ls  nil "exa" "-F"    "--group-directories-first"              "--colour=always" "--icons")
(define-user-function la  nil "exa" "-aF"   "--group-directories-first"              "--colour=always" "--icons")
(define-user-function ll  nil "exa" "-alF"  "--extended" "--group-directories-first" "--colour=always" "--icons")
(define-user-function lr  nil "exa" "-alTF" "--extended" "--group-directories-first" "--colour=always" "--icons")
(define-user-function lsg nil "exa" "-F"    "--group-directories-first"              "--colour=always" "--icons" "--git-ignore")
(define-user-function lag nil "exa" "-aF"   "--group-directories-first"              "--colour=always" "--icons" "--git-ignore")
(define-user-function llg nil "exa" "-alF"  "--extended" "--group-directories-first" "--colour=always" "--icons" "--git-ignore")
(define-user-function lrg nil "exa" "-alTF" "--extended" "--group-directories-first" "--colour=always" "--icons" "--git-ignore")

;; Text Formatting Functions.
(define-user-function expand nil "expand" "-t" "4")

;; Version Control Functions.
(defun git-pull (&rest optional-arguments)
  (uiop:run-program (append (list "git" "pull") (or (mapcar 'string optional-arguments) (list "origin")) (list "--verbose")) :output *standard-output*))

(defun git-push (&rest optional-arguments)
  (uiop:run-program (append (list "git" "push") (or (mapcar 'string optional-arguments)) (list "--verbose")) :output *standard-output*))

(defun git-add (&rest target-files)
  (uiop:run-program (append (list "git" "add") (or (mapcar 'string target-files) (list ".")) (list "--verbose")) :output *standard-output*))

(defun git-remove (&rest target-files)
  (uiop:run-program (append (list "git" "rm" "-rf") (mapcar 'string target-files) (list "--verbose")) :output *standard-output*))

(defun git-checkout (&rest optional-arguments) ; Throws an error in SBCL but in shell.
  (uiop:run-program (append (list "git" "checkout") (or (mapcar 'string optional-arguments) (list "main")) (list "--verbose")) :output *standard-output*))

(defun git-ls () ; Add grep(1) functionality.
  (uiop:run-program (list "git" "ls-files" "--stage") :output *standard-output*))

;; Miscellaneous Functions.
(defun cheat (&optional (target-program nil))
  "Show quick tips and usage information for target-program."
    (uiop:run-program (list "curl" "-fLm" "7" (or (concatenate 'string "https://cheat.sh/" target-program) "https://cheat.sh/")) :output *standard-output*))

(defun view-rss ()
  "View a specific level of the local RSS feed using sfeed_plain(1)."
  (let ((rss-file-path "/tmp/rss.tmp"))
    (with-open-file (target-file rss-file-path :direction :output :if-exists :supersede :if-does-not-exist :create)
      (uiop:run-program (cons "sfeed_plain" (mapcar 'namestring (uiop:directory-files (concatenate 'string (uiop:getenv "XDG_CACHE_HOME") "/sfeed/feeds/")))) :output target-file))
    (uiop:run-program (append (ppcre:split " " (uiop:getenv "PAGER")) (list rss-file-path)) :output :interactive :input :interactive)))

;; Directory Functions.
(define-bookmarked-directory cm   (merge-pathnames "cybermedia/"                                      (uiop:getenv "HOME")))
(define-bookmarked-directory msc  (merge-pathnames "cybermedia/audio/music/"                          (uiop:getenv "HOME")))
(define-bookmarked-directory sct  (merge-pathnames "cybermedia/image/screenshot/"                     (uiop:getenv "HOME")))
(define-bookmarked-directory wlp  (merge-pathnames "cybermedia/image/wallpaper/"                      (uiop:getenv "HOME")))
(define-bookmarked-directory vdo  (merge-pathnames "cybermedia/video/"                                (uiop:getenv "HOME")))
(define-bookmarked-directory tv   (merge-pathnames "cybermedia/video/television-show/"                (uiop:getenv "HOME")))
(define-bookmarked-directory mvi  (merge-pathnames "cybermedia/video/movie/"                          (uiop:getenv "HOME")))
(define-bookmarked-directory dx   (merge-pathnames "document/"                                        (uiop:getenv "HOME")))
(define-bookmarked-directory dev  (merge-pathnames "document/development/"                            (uiop:getenv "HOME")))
(define-bookmarked-directory cpj  (merge-pathnames "document/development/core-project"                (uiop:getenv "HOME")))
(define-bookmarked-directory cpjd (merge-pathnames "document/development/core-project/documentation/" (uiop:getenv "HOME")))
(define-bookmarked-directory cpjl (merge-pathnames "document/development/core-project/library/"       (uiop:getenv "HOME")))
(define-bookmarked-directory cpjp (merge-pathnames "document/development/core-project/program/"       (uiop:getenv "HOME")))
(define-bookmarked-directory dvm  (merge-pathnames "document/development/core-project/miscellaneous/" (uiop:getenv "HOME")))
(define-bookmarked-directory md   (merge-pathnames "media/"                                           (uiop:getenv "HOME")))
(define-bookmarked-directory usr  (merge-pathnames ".usr/"                                            (uiop:getenv "HOME")))
(define-bookmarked-directory var  (merge-pathnames ".var/"                                            (uiop:getenv "HOME")))
(define-bookmarked-directory dl   (uiop:getenv "XDG_DOWNLOAD_HOME"))
(define-bookmarked-directory ul   (uiop:getenv "XDG_UPLOAD_HOME"))
(define-bookmarked-directory cfg  (uiop:getenv "XDG_CONFIG_HOME"))
(define-bookmarked-directory lib  (uiop:getenv "XDG_LIBRARY_HOME"))
(define-bookmarked-directory prg  (uiop:getenv "XDG_PROGRAM_HOME"))
