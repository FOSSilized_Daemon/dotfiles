(in-package :repl-config)

;;;; The login.lisp file contains all of the common-lisp related
;;;; to setting up the environment when the repl is run as a login
;;;; repl. When this file is loaded the below common-lisp will do
;;;; the following.
;;;;    1. Expose functions that help initialization the users environment.
(defun start-environment ()
  "Prompt the user asking whether or not a graphical session should be started. If not, spawn a MULTIPLEXOR session."
  (if (y-or-n-p "start the graphical environment?")
    (uiop:run-program "startx"))
  (uiop:run-program (string (uiop:getenv "MULTIPLEXOR")) :output :interactive :input :interactive))

(defun start-user-services ()
  "Start user-level services and daemons in the background."
  (uiop:launch-program "mpd"))

(defun init-environment ()
  "Initialise the user environment."
  (start-environment)
  (start-user-services))
