;;;; The .replrc file traditionally contains all common-lisp related to settings, activites, aliases and the like.
;;;; However, in an effort to keep this codebase as clean and easy to navigate as possible each section of this file
;;;; has been broken up into modules which are only loaded by this file. Therefore, the below common-lisp does only
;;;; the following.
;;;;    1. If ASDF is installed, then load it into the current repl.
;;;;    2. To keep sbcl(1) from complaining load quicklisp here as well.
;;;;    3. Load the "repl-config" system and then import it to be
;;;;       used within the current session.

#-asdf
(require 'asdf)

#-quicklisp
(let ((quicklisp-init (merge-pathnames ".library/generic/common-lisp/quicklisp/setup.lisp" (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(asdf:load-system :repl-config)
(use-package :repl-config)
